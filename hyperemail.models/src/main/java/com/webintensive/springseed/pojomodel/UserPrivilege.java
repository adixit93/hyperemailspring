package com.webintensive.springseed.pojomodel;

import java.io.Serializable;

/**
 * The persistent class for the user_privileges database table.
 * 
 */
public class UserPrivilege implements Serializable {
	private static final long serialVersionUID = 1L;

	private short id;

	private String description;

	private String name;

	public UserPrivilege() {
	}

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}