package com.webintensive.springseed.pojomodel;

import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the user_login_sessions database table.
 * 
 */

public class UserLoginSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private String usessToken;

	private Date usessLoginDate;

	private String usessSeries;

	// bi-directional many-to-one association to UserAccount

	private UserAccount userAccount;

	public UserLoginSession() {
	}

	public String getUsessToken() {
		return this.usessToken;
	}

	public void setUsessToken(String usessToken) {
		this.usessToken = usessToken;
	}

	public Date getUsessLoginDate() {
		return this.usessLoginDate;
	}

	public void setUsessLoginDate(Date usessLoginDate) {
		this.usessLoginDate = usessLoginDate;
	}

	public String getUsessSeries() {
		return this.usessSeries;
	}

	public void setUsessSeries(String usessSeries) {
		this.usessSeries = usessSeries;
	}

	public UserAccount getUserAccount() {
		return this.userAccount;
	}

	public void setUserAccount(UserAccount userAccount) {
		this.userAccount = userAccount;
	}

}