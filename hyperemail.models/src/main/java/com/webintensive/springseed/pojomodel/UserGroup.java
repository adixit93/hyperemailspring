package com.webintensive.springseed.pojomodel;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * The persistent class for the user_groups database table.
 * 
 */
@JsonSerialize
public class UserGroup implements Serializable {
	private static final long serialVersionUID = 1L;


	private Integer id;

	
	private Byte admin;

	
	private String description;


	private String name;

	private List<UserAccount> userAccounts;

	private List<UserPrivilege> privileges;

	

	public List<UserAccount> getUserAccounts() {
		return this.userAccounts;
	}

	public void setUserAccounts(List<UserAccount> userAccounts) {
		this.userAccounts = userAccounts;
	}

	public UserAccount addUserAccount(UserAccount userAccount) {
		getUserAccounts().add(userAccount);
		userAccount.setUserGroup(this);

		return userAccount;
	}

	public UserAccount removeUserAccount(UserAccount userAccount) {
		getUserAccounts().remove(userAccount);
		userAccount.setUserGroup(null);

		return userAccount;
	}

	public List<UserPrivilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(List<UserPrivilege> privileges) {
		this.privileges = privileges;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Byte getAdmin() {
		return admin;
	}

	public void setAdmin(Byte admin) {
		this.admin = admin;
	}
}