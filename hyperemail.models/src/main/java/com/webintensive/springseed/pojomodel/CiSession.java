package com.webintensive.springseed.pojomodel;

import java.io.Serializable;

/**
 * The persistent class for the ci_sessions database table.
 * 
 */

public class CiSession implements Serializable {
	private static final long serialVersionUID = 1L;

	private String sessionId;

	private String ipAddress;

	private int lastActivity;

	private String userAgent;

	private String userData;

	public CiSession() {
	}

	public String getSessionId() {
		return this.sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getLastActivity() {
		return this.lastActivity;
	}

	public void setLastActivity(int lastActivity) {
		this.lastActivity = lastActivity;
	}

	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getUserData() {
		return this.userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

}