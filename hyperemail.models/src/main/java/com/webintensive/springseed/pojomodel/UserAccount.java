package com.webintensive.springseed.pojomodel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the user_accounts database table.
 * 
 */

public class UserAccount implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private int id;

	
	private String activationToken;


	private byte active;


	private Date dateAdded;


	private Date dateFailLoginBan;


	private Date dateLastLogin;


	private String email;


	private short failLoginAttempts;


	private String failLoginIpAddress;

	
	private Date forgottenPasswordExpire;

	private String forgottenPasswordToken;


	private String ipAddress;

	
	private transient String password;


	private String salt;


	private byte suspend;

	
	private String updateEmail;


	private String updateEmailToken;


	private String username;

	private UserGroup userGroup;

	private List<UserLoginSession> userLoginSessions;

	private List<UserPrivilege> userPrivileges;

	public UserAccount() {		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public byte getActive() {
		return active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getDateFailLoginBan() {
		return dateFailLoginBan;
	}

	public void setDateFailLoginBan(Date dateFailLoginBan) {
		this.dateFailLoginBan = dateFailLoginBan;
	}

	public Date getDateLastLogin() {
		return dateLastLogin;
	}

	public void setDateLastLogin(Date dateLastLogin) {
		this.dateLastLogin = dateLastLogin;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public short getFailLoginAttempts() {
		return failLoginAttempts;
	}

	public void setFailLoginAttempts(short failLoginAttempts) {
		this.failLoginAttempts = failLoginAttempts;
	}

	public String getFailLoginIpAddress() {
		return failLoginIpAddress;
	}

	public void setFailLoginIpAddress(String failLoginIpAddress) {
		this.failLoginIpAddress = failLoginIpAddress;
	}

	public Date getForgottenPasswordExpire() {
		return forgottenPasswordExpire;
	}

	public void setForgottenPasswordExpire(Date forgottenPasswordExpire) {
		this.forgottenPasswordExpire = forgottenPasswordExpire;
	}

	public String getForgottenPasswordToken() {
		return forgottenPasswordToken;
	}

	public void setForgottenPasswordToken(String forgottenPasswordToken) {
		this.forgottenPasswordToken = forgottenPasswordToken;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public byte getSuspend() {
		return suspend;
	}

	public void setSuspend(byte suspend) {
		this.suspend = suspend;
	}

	public String getUpdateEmail() {
		return updateEmail;
	}

	public void setUpdateEmail(String updateEmail) {
		this.updateEmail = updateEmail;
	}

	public String getUpdateEmailToken() {
		return updateEmailToken;
	}

	public void setUpdateEmailToken(String updateEmailToken) {
		this.updateEmailToken = updateEmailToken;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserGroup getUserGroup() {
		return this.userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}

	public List<UserLoginSession> getUserLoginSessions() {
		return this.userLoginSessions;
	}

	public void setUserLoginSessions(List<UserLoginSession> userLoginSessions) {
		this.userLoginSessions = userLoginSessions;
	}

	public UserLoginSession addUserLoginSession(UserLoginSession userLoginSession) {
		getUserLoginSessions().add(userLoginSession);
		userLoginSession.setUserAccount(this);

		return userLoginSession;
	}

	public UserLoginSession removeUserLoginSession(UserLoginSession userLoginSession) {
		getUserLoginSessions().remove(userLoginSession);
		userLoginSession.setUserAccount(null);

		return userLoginSession;
	}

	public List<UserPrivilege> getUserPrivileges() {
		return userPrivileges;
	}

	public void setUserPrivileges(List<UserPrivilege> userPrivileges) {
		this.userPrivileges = userPrivileges;
	}
	
	@JsonIgnore
	public boolean isUserEnabled() {
		return active == 1;
	}
	
	@JsonIgnore
	public boolean isUserBlocked() {
		return suspend == 1;
	}
}