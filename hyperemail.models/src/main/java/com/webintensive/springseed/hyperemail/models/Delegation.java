package com.webintensive.springseed.hyperemail.models;
import java.io.Serializable;
import java.util.List;

public class Delegation implements Serializable {

	private static final long serialVersionUID = -6515328598268619851L;
	private int id;
	private User user;
	private String name;
	private List <User> subOrdinateUsers;
	private List<Folder>folder;
	private List<Filter> filter;
	
	public Delegation() {
		super();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public List<User> getDelegate() {
		return subOrdinateUsers;
	}
	public void setDelegate(List<User> delegate) {
		this.subOrdinateUsers = delegate;
	}
	public List<Folder> getFolder() {
		return folder;
	}
	public void setFolder(List<Folder> folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "Delegation [id=" + id + ", user_id=" + user.getId() + "user_username=" + user.getUsername() + ", user_password=" + user.getPassword() + ", name=" + name + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getSubOrdinateUsers() {
		return subOrdinateUsers;
	}

	public void setSubOrdinateUsers(List<User> subOrdinateUsers) {
		this.subOrdinateUsers = subOrdinateUsers;
	}

	public List<Filter> getFilter() {
		return filter;
	}

	public void setFilter(List<Filter> filter) {
		this.filter = filter;
	}

	
}
