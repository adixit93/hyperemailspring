package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;
import java.util.List;

import com.webintensive.springseed.pojomodel.UserAccount;

public class User extends UserAccount implements Serializable {

	private static final long serialVersionUID = 2269619601894621952L;
	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private List<Delegation> delegate;
	private List<UserEmailAccount> userEmailAccounts;

	public User() {
		super();
	}

	

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getMiddleName() {
		return middleName;
	}



	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public List<Delegation> getDelegate() {
		return delegate;
	}



	public void setDelegate(List<Delegation> delegate) {
		this.delegate = delegate;
	}



	public List<UserEmailAccount> getUserEmailAccounts() {
		return userEmailAccounts;
	}



	public void setUserEmailAccounts(List<UserEmailAccount> userEmailAccounts) {
		this.userEmailAccounts = userEmailAccounts;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName
				+ ", getUsername()=" + getUsername() + "]";
	}
}
