package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.webintensive.springseed.hyperemail.models.Filter;



@JsonDeserialize(as=DomainFilter.class)
public class DomainFilter extends Filter implements Serializable{
	private static final long serialVersionUID = 7665570568244486555L;
	public static final String DTYPE = "3";


	public DomainFilter() {
		setFilterType(FilterType.DOMAIN);
	}
}