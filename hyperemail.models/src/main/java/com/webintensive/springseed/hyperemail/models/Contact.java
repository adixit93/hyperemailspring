package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;

public class Contact implements Serializable {
	private static final long serialVersionUID = -6062725632215869035L;
	private int id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String email;
	private int countMail;
	private UserEmailAccount userEmailAccount;

	public Contact() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getCountMail() {
		return countMail;
	}

	public void setCountMail(int countMail) {
		this.countMail = countMail;
	}

	public UserEmailAccount getAccount() {
		return userEmailAccount;
	}

	public void setAccount(UserEmailAccount account) {
		this.userEmailAccount = account;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", email=" + email + ", countMail=" + countMail + "]";
	}

	
}
