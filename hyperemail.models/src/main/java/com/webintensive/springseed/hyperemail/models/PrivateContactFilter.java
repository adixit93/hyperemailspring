package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.webintensive.springseed.hyperemail.models.Filter;

@JsonDeserialize(as = PrivateContactFilter.class)
public class PrivateContactFilter extends Filter implements Serializable {

	private static final long serialVersionUID = 2892230333476796581L;
	public static final String DTYPE = "1";

	public PrivateContactFilter() {
		setFilterType(FilterType.PRIVATE_CONTACT);

	}

}
