package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;

/**
 * The Class ErrorInfo.
 */
@JsonSerialize(include = Inclusion.NON_EMPTY)
public class ErrorInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The reuqest url. */
	public final String url;

	/** The exception message. */
	public final String errorMessage;

	private Date date = new Date();

	// invalid data
	private Object data;

	/**
	 * Instantiates a new error info.
	 * 
	 * @param url
	 *            the url
	 * @param ex
	 *            the ex
	 */
	public ErrorInfo(String url, Exception e) {
		this.url = url;
		this.errorMessage = e.getLocalizedMessage();
	}

	public ErrorInfo(String url, String message) {
		this.url = url;
		this.errorMessage = message;
	}

	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	// This will return time of server rather than timestamp
	public String getDate() {
		return date.toString();
	}
}