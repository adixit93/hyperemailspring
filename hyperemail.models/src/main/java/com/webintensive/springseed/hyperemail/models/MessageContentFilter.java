package com.webintensive.springseed.hyperemail.models;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.webintensive.springseed.hyperemail.models.Filter;

@JsonDeserialize(as=MessageContentFilter.class)
public class MessageContentFilter extends Filter implements Serializable{

	private static final long serialVersionUID = -2926101988721648954L;
	public static final String DTYPE = "4";

	public MessageContentFilter() {
		setFilterType(FilterType.MESSAGE_CONTENT);

	}
}
