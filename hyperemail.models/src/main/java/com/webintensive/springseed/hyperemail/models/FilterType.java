package com.webintensive.springseed.hyperemail.models;

public enum FilterType {
	PRIVATE_CONTACT,DOMAIN,INDIVIDUAL_EMAIL,MESSAGE_CONTENT

}
