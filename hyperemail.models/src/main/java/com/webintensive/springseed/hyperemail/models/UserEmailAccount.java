
package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;
import java.util.List;

public class UserEmailAccount implements Serializable {
	private static final long serialVersionUID = 4321531901068321312L;
	private String password;
	private String email;
	private String imapDomain;
	private String smtpDomain;
	private int imapPort;
	private int smtpPort;
	private User user;

	private int id;

	public UserEmailAccount() {
		super();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImapDomain() {
		return imapDomain;
	}

	public void setImapDomain(String imapDomain) {
		this.imapDomain = imapDomain;
	}

	public String getSmtpDomain() {
		return smtpDomain;
	}

	public void setSmtpDomain(String smtpDomain) {
		this.smtpDomain = smtpDomain;
	}

	public int getImapPort() {
		return imapPort;
	}

	public void setImapPort(int imapPort) {
		this.imapPort = imapPort;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "UserEmailAccount [password=" + password + ", email=" + email + ", imapDomain=" + imapDomain
				+ ", smtpDomain=" + smtpDomain + ", imapPort=" + imapPort + ", smtpPort=" + smtpPort + ", id=" + id
				+ ", user=" + user.getId() + "]";
	}

}
