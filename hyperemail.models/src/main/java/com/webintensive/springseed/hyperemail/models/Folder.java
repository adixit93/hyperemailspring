package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;
import java.util.List;

public class Folder implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String identifier;
	private UserEmailAccount userEmailAccount;
	private User user;

	private List<FolderPermission> folderPermissions;

	public Folder() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public UserEmailAccount getUserEmailAccount() {
		return userEmailAccount;
	}

	public void setUserEmailAccount(UserEmailAccount userEmailAccount) {
		this.userEmailAccount = userEmailAccount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<FolderPermission> getFolderPermissions() {
		return folderPermissions;
	}

	public void setFolderPermissions(List<FolderPermission> folderPermissions) {
		this.folderPermissions = folderPermissions;
	}

	@Override
	public String toString() {
		return "Folder [id=" + id + ", name=" + name + ", identifier=" + identifier + "]";
	}

	
	

}
