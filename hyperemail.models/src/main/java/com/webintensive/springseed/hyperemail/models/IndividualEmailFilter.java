package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.webintensive.springseed.hyperemail.models.Filter;

@JsonDeserialize(as = IndividualEmailFilter.class)
public class IndividualEmailFilter extends Filter implements Serializable {

	private static final long serialVersionUID = 5246675032009721817L;
	public static final String DTYPE = "2";

	public IndividualEmailFilter() {
		setFilterType(FilterType.INDIVIDUAL_EMAIL);
	}

}
