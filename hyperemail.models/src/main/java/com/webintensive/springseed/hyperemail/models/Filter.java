package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "filter_id")
@JsonSubTypes({ @Type(value = DomainFilter.class, name = DomainFilter.DTYPE),
		@Type(value = IndividualEmailFilter.class, name = IndividualEmailFilter.DTYPE),
		@Type(value = MessageContentFilter.class, name = MessageContentFilter.DTYPE),
		@Type(value = PrivateContactFilter.class, name = PrivateContactFilter.DTYPE), })
@JsonIgnoreProperties(ignoreUnknown = true)

public abstract class Filter implements Serializable {
	private static final long serialVersionUID = -7183386038692732361L;
	private Integer id;
	private Delegation delegation;
	private FilterType filterType;
	private String content;
	private Integer filter_id;

	public Integer getFilter_id() {
		return filter_id;
	}

	public void setFilter_id(Integer filter_id) {
		this.filter_id = filter_id;
	}

	public FilterType getFilterType() {
		return filterType;
	}

	public Filter() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Delegation getDelegation() {
		return delegation;
	}

	public void setDelegation(Delegation delegation) {
		this.delegation = delegation;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
	}

}
