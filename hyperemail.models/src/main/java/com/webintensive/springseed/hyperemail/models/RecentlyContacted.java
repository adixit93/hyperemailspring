package com.webintensive.springseed.hyperemail.models;

import java.io.Serializable;
import java.util.List;

public class RecentlyContacted implements Serializable{
	
	/**
	 * SOUMYA
	 */
	private static final long serialVersionUID = 1185987517941553664L;
	private Integer userEmailId;
	private String userEmail;
	private List<Contact> contact;
	private Integer id;
	public Integer getUserEmailId() {
		return userEmailId;
	}
	public void setUserEmailId(Integer userEmailId) {
		this.userEmailId = userEmailId;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public List<Contact> getContact() {
		return contact;
	}
	public void setContact(List<Contact> contact) {
		this.contact = contact;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "RecentlyContacted [userEmailId=" + userEmailId + ", userEmail=" + userEmail + ", contact=" + contact
				+ ", id=" + id + "]";
	}
	
}

