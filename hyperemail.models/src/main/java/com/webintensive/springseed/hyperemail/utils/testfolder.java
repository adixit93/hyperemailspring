package com.webintensive.springseed.hyperemail.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;

import com.sun.mail.imap.IMAPStore;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;

public class testfolder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		props.put("mail.imap.host", "foobar.com");
		props.put("mail.imap.port", "143");
		props.put("mail.pop3.starttls.enable", "true");

		List<Folder> folderList = new ArrayList<Folder>();

		try {
			Session session = Session.getDefaultInstance(props, null);

			IMAPStore store = (IMAPStore) session.getStore("imap");
			store.connect("boss1@foobar.com", "boss1");

			javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
			for (javax.mail.Folder folder : folders) {
				if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {

					Folder folderName = new Folder();
					UserEmailAccount userEmailAccount = new UserEmailAccount();
					folderName.setName(folder.getFullName());
					userEmailAccount.setEmail("boss1@foobar.com");
					userEmailAccount.setImapDomain("foobar.com");
					userEmailAccount.setPassword("boss1");
					folderName.setUserEmailAccount(userEmailAccount);
					folderList.add(folderName);
					//System.out.println(folderList.toString());
					for(Folder f : folderList){
						System.out.println(f);
					}
				}
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}

}
