package com.webintensive.springseed.hyperemail.utils;

import java.util.ArrayList;

import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;

import com.sun.mail.iap.Argument;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;

public class FolderUtils {

	public List<Folder> getFolderList(String email, String password, String imapAddress , Integer port) {
		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		props.put("mail.imap.host", imapAddress);
		props.put("mail.imap.port", "143");
		props.put("mail.pop3.starttls.enable", "true");

		List<Folder> folderList = new ArrayList<Folder>();

		try {
			Session session = Session.getInstance(props, null);

			IMAPStore store = (IMAPStore) session.getStore("imap");
			store.connect(email, password);

			javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
			for (javax.mail.Folder folder : folders) {
				if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0) {

					Folder folderName = new Folder();
					UserEmailAccount userEmailAccount = new UserEmailAccount();
					folderName.setName(folder.getFullName());
					userEmailAccount.setEmail(email);
					userEmailAccount.setImapDomain(imapAddress);
					userEmailAccount.setPassword(password);
					folderName.setUserEmailAccount(userEmailAccount);
					folderList.add(folderName);
				}
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		return folderList;
	}
	
	
	public StringBuilder addFolderToJames(final String bossUsername, String imapHost, String user, String password, final String mailbox) {
		final StringBuilder[] status = new StringBuilder[1];

		try {
			// 1) get the session object
			Properties properties = System.getProperties();
			properties.setProperty("mail.store.protocol", "imaps");

			/*
			 * Session session = Session.getDefaultInstance(properties, null);
			 * Store store = session.getStore("imaps");
			 * store.connect("imap.gmail.com", "radhesingh4395@gmail.com",
			 * password);
			 * 
			 */
			properties.put("mail.imap.host", imapHost);
			properties.put("mail.imap.port", "143");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getInstance(properties);

			// 2) create the POP3 store object and connect with the pop server
			IMAPStore emailStore = (IMAPStore) emailSession.getStore("imap");
			emailStore.connect(user + "@" + imapHost, password);

			// 3) create the folder object and open it
			javax.mail.Folder emailFolder = emailStore.getFolder("INBOX");
			emailFolder.open(javax.mail.Folder.READ_ONLY);

			IMAPFolder folder = (IMAPFolder) emailFolder;

			Object val = folder.doCommand(new IMAPFolder.ProtocolCommand() {

				@Override
				public Object doCommand(IMAPProtocol arg0) throws ProtocolException {
					Argument args = new Argument();

					args.writeString("Boss" + bossUsername + "~" + mailbox	);
					Response[] r = arg0.command("Create", args);
					Response responses = r[r.length - 1];

					if (responses.isOK()) {
						status[0] = new StringBuilder("success");
					} else {
						status[0] = new StringBuilder("fail");
					}

					return null;
				}
			});

			emailFolder.close(false);
			emailStore.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return status[0];
	}
}