package com.webintensive.springseed.service;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.webintensive.springseed.configurations.DispatcherConfig;
import com.webintensive.springseed.configurations.RootAppConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {DispatcherConfig.class, RootAppConfig.class}, loader = AnnotationConfigContextLoader.class)
public class BaseTest  extends TestCase {
	
	@Autowired
	UserAccountService userAccountService;
	
	@Test
	public void isBaseTestWorking() {
		System.out.println("Base Test is successfully setup.");
	}
	
//	@Configuration
//	static class Config {
//		@Bean
//        public static PropertyPlaceholderConfigurer propertyConfigIn() {
//            PropertyPlaceholderConfigurer ppc =  new PropertyPlaceholderConfigurer();
//            ppc.setLocation(new ClassPathResource("application.properties"));
//            return ppc;
//        }
//	}
	
//	@Configuration
//	@ComponentScan("com.springseed")
//	static class Config {
//		
//	}
}