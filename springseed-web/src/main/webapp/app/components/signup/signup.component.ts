import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {Http, Headers, HTTP_PROVIDERS} from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AuthenticationService} from '../../services/authentication.service';

declare var Materialize: any;

@Component({
	selector: 'hm-signup',
	templateUrl: 'app/components/signup/signup.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [AuthenticationService]
})

export class SignupComponent implements OnInit {
	title: string = "Sign up";
	rPassword = "";
	status = "";
	details = {};

	ngOnInit() {
		Materialize.updateTextFields();
	}

	constructor(private router: Router, public http : Http) { };

	checkPassword(){
	}

	onSubmit(details:any) {
	}
}
