"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var http_1 = require('@angular/http');
var UserEmailAccount_1 = require('../../interfaces/UserEmailAccount');
var account_service_1 = require('../../services/account.service');
var MyAccountsComponent = (function () {
    function MyAccountsComponent(router, http, accountService) {
        this.router = router;
        this.http = http;
        this.accountService = accountService;
        this.title = "Connect to my Accounts";
        this.userEmailAccount = new UserEmailAccount_1.UserEmailAccount();
    }
    ;
    MyAccountsComponent.prototype.ngOnInit = function () {
        $('.modal-trigger').leanModal({
            dismissible: true,
            opacity: .5,
            in_duration: 300,
            out_duration: 200
        });
        this.getAccounts();
    };
    MyAccountsComponent.prototype.ngAfterContentChecked = function () {
        Materialize.updateTextFields();
    };
    MyAccountsComponent.prototype.addAccount = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to Connect this account with HyperEmail?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Add it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.accountService.addAccount(_this.userEmailAccount)
                    .subscribe(function (data) {
                    swal("Added!", "Account has been successfully Added !!", "success");
                    _this.userEmailAccount = new UserEmailAccount_1.UserEmailAccount();
                    _this.getAccounts();
                });
            }
            else {
                _this.userEmailAccount = new UserEmailAccount_1.UserEmailAccount();
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    ;
    MyAccountsComponent.prototype.getAccounts = function () {
        var _this = this;
        this.accountService.getAccounts()
            .subscribe(function (data) {
            _this.allAccounts = data;
        });
    };
    ;
    MyAccountsComponent.prototype.deleteAccount = function (accountId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.accountService.deleteAccount(accountId)
                .subscribe(function (data) {
                swal("Deleted!", "It has been successfully deleted.", "success");
                _this.getAccounts();
            });
        });
    };
    ;
    MyAccountsComponent.prototype.updateAccount = function (accountId, idx) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to Update this account?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Update it!",
            closeOnConfirm: false
        }, function () {
            _this.accountService.updateAccount(accountId, _this.allAccounts[idx])
                .subscribe(function (data) {
                swal("Updated!", "Account has been successfully Updated !!", "success");
                _this.getAccounts();
            });
        });
    };
    ;
    MyAccountsComponent = __decorate([
        core_1.Component({
            selector: 'hm-myaccounts',
            templateUrl: 'app/components/myaccounts/myaccounts.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [account_service_1.AccountService]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, http_1.Http, account_service_1.AccountService])
    ], MyAccountsComponent);
    return MyAccountsComponent;
}());
exports.MyAccountsComponent = MyAccountsComponent;
//# sourceMappingURL=myaccounts.component.js.map