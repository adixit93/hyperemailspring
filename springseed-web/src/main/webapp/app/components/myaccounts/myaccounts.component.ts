import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';
import {Http, Headers, RequestOptions, Response, HTTP_PROVIDERS} from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import {UserEmailAccount} from '../../interfaces/UserEmailAccount';
import {User} from '../../interfaces/User';
import {AccountService} from '../../services/account.service';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-myaccounts',
	templateUrl: 'app/components/myaccounts/myaccounts.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [AccountService]
})

export class MyAccountsComponent implements OnInit {
	title: string = "Connect to my Accounts";
	userEmailAccount:UserEmailAccount = new UserEmailAccount();
	allAccounts: UserEmailAccount[];

	constructor(private router: Router, private http: Http, private accountService:AccountService) {
	};

	ngOnInit() {
		$('.modal-trigger').leanModal({
      dismissible: true,
      opacity: .5,
      in_duration: 300,
      out_duration: 200
    });

		this.getAccounts();
	}

	ngAfterContentChecked(){
		Materialize.updateTextFields();
	}

	addAccount(){
		swal({
				title: "Are you sure?",
				text: "You want to Connect this account with HyperEmail?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Add it!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
			 (isConfirm:any)=>{
			 		if(isConfirm){	
						this.accountService.addAccount(this.userEmailAccount)
						.subscribe(
							data => {
								swal("Added!", "Account has been successfully Added !!", "success"); 
								this.userEmailAccount = new UserEmailAccount();
								this.getAccounts();
							});
					} else {
							this.userEmailAccount = new UserEmailAccount();
						 	swal("Cancelled", "Canceled", "error");
						}
				});
	};

	getAccounts(){
		this.accountService.getAccounts()
		.subscribe(
			data=> {
				this.allAccounts = data;
			});
	};

	deleteAccount(accountId:number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this account?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.accountService.deleteAccount(accountId)
					.subscribe(
						data=> {
							swal("Deleted!", "It has been successfully deleted.", "success");
							this.getAccounts();
						});
		});
	};

	updateAccount(accountId:number, idx:number){
		swal({
				title: "Are you sure?",
				text: "You want to Update this account?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Update it!",
				closeOnConfirm: false
			},
				()=>{
					this.accountService.updateAccount(accountId, this.allAccounts[idx])
					.subscribe(
						data=> {
							swal("Updated!", "Account has been successfully Updated !!", "success"); 
							this.getAccounts();
						});
		});
	};
}
