"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var filter_service_1 = require('../../services/filter.service');
var Filter_1 = require('../../interfaces/Filter');
var PrivateMessagesComponent = (function () {
    function PrivateMessagesComponent(router, routeParams, filterService) {
        this.router = router;
        this.routeParams = routeParams;
        this.filterService = filterService;
        this.title = "Email Delegation : Which Messages are private?";
        this.filter = new Filter_1.Filter();
    }
    ;
    PrivateMessagesComponent.prototype.ngOnInit = function () {
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
        this.getKeywordsByType();
    };
    PrivateMessagesComponent.prototype.getKeywordsByType = function () {
        var _this = this;
        this.filterService.getFiltersbyType(this.delegationId, 'keyword')
            .subscribe(function (data) {
            _this.filters = data;
        });
    };
    PrivateMessagesComponent.prototype.addKeyword = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to make mails containing " + this.filter.content + " keyword as private?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Make it Private!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.filterService.addFilter(_this.delegationId, _this.filter, 'keyword')
                    .subscribe(function (data) {
                    swal("Added!", "It has been made private", "success");
                    _this.getKeywordsByType();
                    _this.filter = new Filter_1.Filter();
                });
            }
            else {
                _this.filter = new Filter_1.Filter();
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    PrivateMessagesComponent.prototype.deleteKeyword = function (filterId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this keyword Filter?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.filterService.deleteFilter(_this.delegationId, filterId)
                .subscribe(function (data) {
                swal("Deleted!", "It has been successfully deleted.", "success");
                _this.getKeywordsByType();
            });
        });
    };
    PrivateMessagesComponent = __decorate([
        core_1.Component({
            selector: 'hm-private-messages',
            templateUrl: 'app/components/privateMessages/privateMessages.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [filter_service_1.FilterService]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams, filter_service_1.FilterService])
    ], PrivateMessagesComponent);
    return PrivateMessagesComponent;
}());
exports.PrivateMessagesComponent = PrivateMessagesComponent;
//# sourceMappingURL=privateMessages.component.js.map