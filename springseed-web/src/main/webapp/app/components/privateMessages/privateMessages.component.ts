import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';


import {FilterService} from '../../services/filter.service';
import {Filter} from '../../interfaces/Filter';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-private-messages',
	templateUrl: 'app/components/privateMessages/privateMessages.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [FilterService]
})

export class PrivateMessagesComponent implements OnInit {
	title: string = "Email Delegation : Which Messages are private?";
	delegationId:number;
	filters: Filter[];
	filter: Filter = new Filter();

	constructor(private router: Router, private routeParams: RouteParams, private filterService: FilterService) { };

	ngOnInit() {
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);
		this.getKeywordsByType();
	}

	getKeywordsByType(){
		this.filterService.getFiltersbyType(this.delegationId, 'keyword')
			.subscribe(
				data=> {
					this.filters = data;
				});
	}

	addKeyword(){
		swal({
				title: "Are you sure?",
				text: "You want to make mails containing " + this.filter.content + " keyword as private?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Make it Private!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
			 (isConfirm:any)=>{
			 		if(isConfirm){
						this.filterService.addFilter(this.delegationId, this.filter, 'keyword')
							.subscribe(
								data=> {
									swal("Added!", "It has been made private", "success"); 
									this.getKeywordsByType();
									this.filter = new Filter();
								});
						}
						else{
							this.filter = new Filter();
							swal("Cancelled", "Canceled", "error");
						}
			});
	}

	deleteKeyword(filterId:number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this keyword Filter?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.filterService.deleteFilter(this.delegationId, filterId)
						.subscribe(
							data=> {
								swal("Deleted!", "It has been successfully deleted.", "success");
								this.getKeywordsByType();
							});
			});
	}
}
