import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {DelegationService} from '../../services/delegation.service';

import {Delegation} from '../../interfaces/Delegation';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-delegation',
	templateUrl: 'app/components/delegation/delegation.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [DelegationService]
})

export class DelegationComponent implements OnInit {
	title: string = "Delegation";
	allDelegations: Delegation[];
	delegation: Delegation = new Delegation();

	constructor(private router: Router, private delegationService: DelegationService) { };

	ngOnInit() {

		this.getDelegations();

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		$('.modal-trigger').leanModal({
			dismissible: true,
			opacity: .5,
			in_duration: 300,
			out_duration: 200
		});

		Materialize.updateTextFields();
	}

	selectDelegation(delegationId: number){
		this.router.navigate(['/Delegation', {delegationId: delegationId}]);
	}

	getDelegations(){
		this.delegationService.getDelegations()
		.subscribe(
			data=> {
				this.allDelegations = data;
				this.delegation = new Delegation();
			});
	}

	addDelegation(){

		swal({
				title: "Are you sure?",
				text: "You want to add  " + this.delegation.name  + "?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Add it!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
			 (isConfirm:any)=>{
			 		if(isConfirm){	
						this.delegationService.addDelegation(this.delegation)
						.subscribe(
							data => {
								swal("Added!", "Your Delegation has been successfully added.", "success"); 
								this.getDelegations();
							});
					} else {
							this.delegation = new Delegation();
						 	swal("Cancelled", "Canceled", "error");
						}
				});
	}

	deleteDelegation(delegationId : number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this delegation?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.delegationService.deleteDelegation(delegationId)
						.subscribe(
							data=> {
								swal("Deleted!", "Your Delegation has been successfully deleted.", "success"); 
								this.getDelegations();
							});
					});
	}
}
