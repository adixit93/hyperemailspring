"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var delegation_service_1 = require('../../services/delegation.service');
var Delegation_1 = require('../../interfaces/Delegation');
var DelegationComponent = (function () {
    function DelegationComponent(router, delegationService) {
        this.router = router;
        this.delegationService = delegationService;
        this.title = "Delegation";
        this.delegation = new Delegation_1.Delegation();
    }
    ;
    DelegationComponent.prototype.ngOnInit = function () {
        this.getDelegations();
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $('.modal-trigger').leanModal({
            dismissible: true,
            opacity: .5,
            in_duration: 300,
            out_duration: 200
        });
        Materialize.updateTextFields();
    };
    DelegationComponent.prototype.selectDelegation = function (delegationId) {
        this.router.navigate(['/Delegation', { delegationId: delegationId }]);
    };
    DelegationComponent.prototype.getDelegations = function () {
        var _this = this;
        this.delegationService.getDelegations()
            .subscribe(function (data) {
            _this.allDelegations = data;
            _this.delegation = new Delegation_1.Delegation();
        });
    };
    DelegationComponent.prototype.addDelegation = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to add  " + this.delegation.name + "?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Add it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.delegationService.addDelegation(_this.delegation)
                    .subscribe(function (data) {
                    swal("Added!", "Your Delegation has been successfully added.", "success");
                    _this.getDelegations();
                });
            }
            else {
                _this.delegation = new Delegation_1.Delegation();
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    DelegationComponent.prototype.deleteDelegation = function (delegationId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this delegation?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.delegationService.deleteDelegation(delegationId)
                .subscribe(function (data) {
                swal("Deleted!", "Your Delegation has been successfully deleted.", "success");
                _this.getDelegations();
            });
        });
    };
    DelegationComponent = __decorate([
        core_1.Component({
            selector: 'hm-delegation',
            templateUrl: 'app/components/delegation/delegation.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [delegation_service_1.DelegationService]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, delegation_service_1.DelegationService])
    ], DelegationComponent);
    return DelegationComponent;
}());
exports.DelegationComponent = DelegationComponent;
//# sourceMappingURL=delegation.component.js.map