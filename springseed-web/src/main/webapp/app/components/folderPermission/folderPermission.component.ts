import {Component, OnInit, AfterContentInit, AfterViewChecked, AfterContentChecked} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';


import {PreLoader} from '../../shared/preloader/preloader.component';
import {UserEmailAccount} from '../../interfaces/UserEmailAccount';
import {User} from '../../interfaces/User';
import {Folder} from '../../interfaces/Folder';

import {AccountService} from '../../services/account.service';
import {FolderService} from '../../services/folder.service';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-folder-permissions',
	templateUrl: 'app/components/folderPermission/folderPermission.component.html',
	directives: [ROUTER_DIRECTIVES, PreLoader],	
	providers: [AccountService, FolderService]
})

export class FolderPermissionComponent implements OnInit, AfterContentInit {
	title: string = "Email Delegation : Which Folders may they access?";
	delegationId: number;
	accounts: UserEmailAccount[];
	folders: Folder[];
	accessFolders: Folder[];
	selAccount: UserEmailAccount = new UserEmailAccount();
	selFolder: Folder;

	constructor(private router: Router, private routeParams: RouteParams, private accountService:AccountService, private folderService:FolderService){};

	ngOnInit() {
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);
		this.getAccounts();

		$('.collapsible').collapsible({
      accordion: false
    });

    $('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: true,
			hover: true,
			gutter: 0,
			belowOrigin: true,
			alignment: 'right'
		});

		Materialize.updateTextFields();

	}

	ngAfterContentChecked(){

		setTimeout(function(){
			$('select').material_select();
		});

		$('.modal-trigger').leanModal({
      dismissible: true, 
      opacity: .5,
      in_duration: 300,
      out_duration: 200
    });
	}

	ngAfterContentInit(){
		setTimeout(function(){
			$('select').material_select();
		});
	}

	getAccounts(){
		this.accountService.getAccounts()
		.subscribe(
			data=> {
				this.accounts = data;
			});
	};



	getFolders(){
		this.getAccessFolders();
		this.folderService.getFolders(this.selAccount.id)
			.subscribe(
				data=> {
					this.folders = data;
			});
	};

	getAccessFolders(){
		this.folderService.getAccessFolders(this.delegationId, this.selAccount.id)
			.subscribe(
				data=> {
					this.accessFolders = data;
			});
	}

	addFolder(){
		swal({
				title: "Are you sure?",
				text: "You want to delegate this folder?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delegate it!",
				closeOnConfirm: false
			},
				()=>{
					this.folderService.addFolder(this.delegationId, this.selAccount.id, this.selFolder)
						.subscribe(
							data=> {
								this.getAccessFolders();
								swal("Added!", "This Folder has been successfully delegated.", "success"); 
						});
					});
	};
}
