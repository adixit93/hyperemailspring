"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var preloader_component_1 = require('../../shared/preloader/preloader.component');
var UserEmailAccount_1 = require('../../interfaces/UserEmailAccount');
var account_service_1 = require('../../services/account.service');
var folder_service_1 = require('../../services/folder.service');
var FolderPermissionComponent = (function () {
    function FolderPermissionComponent(router, routeParams, accountService, folderService) {
        this.router = router;
        this.routeParams = routeParams;
        this.accountService = accountService;
        this.folderService = folderService;
        this.title = "Email Delegation : Which Folders may they access?";
        this.selAccount = new UserEmailAccount_1.UserEmailAccount();
    }
    ;
    FolderPermissionComponent.prototype.ngOnInit = function () {
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
        this.getAccounts();
        $('.collapsible').collapsible({
            accordion: false
        });
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true,
            hover: true,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right'
        });
        Materialize.updateTextFields();
    };
    FolderPermissionComponent.prototype.ngAfterContentChecked = function () {
        setTimeout(function () {
            $('select').material_select();
        });
        $('.modal-trigger').leanModal({
            dismissible: true,
            opacity: .5,
            in_duration: 300,
            out_duration: 200
        });
    };
    FolderPermissionComponent.prototype.ngAfterContentInit = function () {
        setTimeout(function () {
            $('select').material_select();
        });
    };
    FolderPermissionComponent.prototype.getAccounts = function () {
        var _this = this;
        this.accountService.getAccounts()
            .subscribe(function (data) {
            _this.accounts = data;
        });
    };
    ;
    FolderPermissionComponent.prototype.getFolders = function () {
        var _this = this;
        this.getAccessFolders();
        this.folderService.getFolders(this.selAccount.id)
            .subscribe(function (data) {
            _this.folders = data;
        });
    };
    ;
    FolderPermissionComponent.prototype.getAccessFolders = function () {
        var _this = this;
        this.folderService.getAccessFolders(this.delegationId, this.selAccount.id)
            .subscribe(function (data) {
            _this.accessFolders = data;
        });
    };
    FolderPermissionComponent.prototype.addFolder = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delegate this folder?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delegate it!",
            closeOnConfirm: false
        }, function () {
            _this.folderService.addFolder(_this.delegationId, _this.selAccount.id, _this.selFolder)
                .subscribe(function (data) {
                _this.getAccessFolders();
                swal("Added!", "This Folder has been successfully delegated.", "success");
            });
        });
    };
    ;
    FolderPermissionComponent = __decorate([
        core_1.Component({
            selector: 'hm-folder-permissions',
            templateUrl: 'app/components/folderPermission/folderPermission.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, preloader_component_1.PreLoader],
            providers: [account_service_1.AccountService, folder_service_1.FolderService]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams, account_service_1.AccountService, folder_service_1.FolderService])
    ], FolderPermissionComponent);
    return FolderPermissionComponent;
}());
exports.FolderPermissionComponent = FolderPermissionComponent;
//# sourceMappingURL=folderPermission.component.js.map