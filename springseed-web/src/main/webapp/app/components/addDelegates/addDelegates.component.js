"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var user_service_1 = require('../../services/user.service');
var delegation_service_1 = require('../../services/delegation.service');
var Delegation_1 = require('../../interfaces/Delegation');
var search_pipe_1 = require('../../pipes/search.pipe');
var AddDelegatesComponent = (function () {
    function AddDelegatesComponent(router, routeParams, userService, delegationservice) {
        this.router = router;
        this.routeParams = routeParams;
        this.userService = userService;
        this.delegationservice = delegationservice;
        this.title = "Email Delegation : Who may help me?";
        this.delegation = new Delegation_1.Delegation();
    }
    ;
    AddDelegatesComponent.prototype.ngOnInit = function () {
        this.getUsers();
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
        this.userID = this.userService.getUserId();
        this.getDelegates();
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });
        Materialize.updateTextFields();
    };
    AddDelegatesComponent.prototype.getUsers = function () {
        var _this = this;
        this.userService.getUsers()
            .subscribe(function (data) {
            _this.users = data;
            console.log(data);
        });
    };
    ;
    AddDelegatesComponent.prototype.getDelegates = function () {
        var _this = this;
        this.delegationservice.getDelegates(this.delegationId)
            .subscribe(function (data) {
            _this.delegates = data;
        });
    };
    ;
    AddDelegatesComponent.prototype.addDelegate = function (delegateId) {
        var _this = this;
        this.delegation.id = this.delegationId;
        this.delegation.user.id = delegateId;
        swal({
            title: "Are you sure?",
            text: "You want to add this delegate?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Add it!",
            closeOnConfirm: false
        }, function () {
            _this.delegationservice.addDelegate(_this.delegation)
                .subscribe(function (data) {
                swal("Added!", "Your Delegate has been successfully added.", "success");
                _this.getDelegates();
            });
        });
    };
    ;
    AddDelegatesComponent.prototype.deleteDelegate = function (delegateId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this delegate?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.delegationservice.deleteDelegate(_this.delegationId, delegateId)
                .subscribe(function (data) {
                swal("Deleted!", "Delegate has been successfully deleted.", "success");
                _this.getDelegates();
            });
        });
    };
    ;
    AddDelegatesComponent = __decorate([
        core_1.Component({
            selector: 'hm-add-delegates',
            templateUrl: 'app/components/addDelegates/addDelegates.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [user_service_1.UserService, delegation_service_1.DelegationService],
            pipes: [search_pipe_1.SearchFilter]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams, user_service_1.UserService, delegation_service_1.DelegationService])
    ], AddDelegatesComponent);
    return AddDelegatesComponent;
}());
exports.AddDelegatesComponent = AddDelegatesComponent;
//# sourceMappingURL=addDelegates.component.js.map