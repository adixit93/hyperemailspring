import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';

import {UserService} from '../../services/user.service';
import {DelegationService} from '../../services/delegation.service';
import {User} from '../../interfaces/User';
import {Delegation} from '../../interfaces/Delegation';
import {SearchFilter} from '../../pipes/search.pipe';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-add-delegates',
	templateUrl: 'app/components/addDelegates/addDelegates.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [UserService, DelegationService],
	pipes :[SearchFilter]
})

export class AddDelegatesComponent implements OnInit {
	title: string = "Email Delegation : Who may help me?";
	users : User[];
	filter: string;
	delegationId : number;
	delegation: Delegation = new Delegation();
	delegates: Delegation[];

	userID: number;

	constructor(private router: Router, private routeParams: RouteParams, private userService :UserService, private delegationservice: DelegationService) { };

	ngOnInit() {
		this.getUsers();
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);
		this.userID = this.userService.getUserId();
		this.getDelegates();

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		$('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: true, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    });

		Materialize.updateTextFields();
	}

	getUsers(){
		this.userService.getUsers()
		.subscribe(
			data=> {
				this.users = data;
				console.log(data);
			});
	};

	getDelegates(){
		this.delegationservice.getDelegates(this.delegationId)
			.subscribe(
				data=> {
					this.delegates = data;
				});

	};

	addDelegate(delegateId:number){
		this.delegation.id = this.delegationId;
		this.delegation.user.id = delegateId;
		swal({
				title: "Are you sure?",
				text: "You want to add this delegate?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Add it!",
				closeOnConfirm: false
			},
				()=>{
					this.delegationservice.addDelegate(this.delegation)
						.subscribe(
							data=> {
								swal("Added!", "Your Delegate has been successfully added.", "success"); 
								this.getDelegates();
							});
					});
	};

		
	

	deleteDelegate(delegateId:number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this delegate?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.delegationservice.deleteDelegate(this.delegationId, delegateId)
						.subscribe(
							data=> {
								swal("Deleted!", "Delegate has been successfully deleted.", "success"); 
								this.getDelegates();
							});
					});
	};

}
