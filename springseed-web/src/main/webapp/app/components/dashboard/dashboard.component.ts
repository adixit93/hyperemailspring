import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {UserService} from '../../services/user.service';
import 'rxjs/Rx';

declare var $: any;

@Component({
	selector: 'hm-dashboard',
	templateUrl: 'app/components/dashboard/dashboard.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [UserService]
})

export class DashboardComponent implements OnInit{
	title: string = "Dashboard";
	constructor(private router: Router, private userService: UserService) { };

	ngOnInit() {
		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
	}
}
