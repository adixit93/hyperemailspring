"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var filter_service_1 = require('../../services/filter.service');
var Filter_1 = require('../../interfaces/Filter');
var search_pipe_1 = require('../../pipes/search.pipe');
var PrivateContactsComponent = (function () {
    function PrivateContactsComponent(router, routeParams, filterService) {
        this.router = router;
        this.routeParams = routeParams;
        this.filterService = filterService;
        this.title = "Email Delegation : Which Contacts are private?";
        this.domainFilter = new Filter_1.Filter();
        this.individualFilter = new Filter_1.Filter();
        this.contactFilter = new Filter_1.Filter();
    }
    ;
    PrivateContactsComponent.prototype.ngOnInit = function () {
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
        this.fetchListContacts();
        this.getDomains();
        this.getIndividuals();
        this.getContacts();
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        Materialize.updateTextFields();
        setTimeout(function () {
            $('select').material_select();
        });
        $('.modal-trigger').leanModal({
            dismissible: true,
            opacity: .5,
            in_duration: 300,
            out_duration: 200,
        });
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true,
            hover: false,
            gutter: 0,
            belowOrigin: true,
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        });
    };
    PrivateContactsComponent.prototype.onChange = function (idx, event) {
        if (event.target.checked) {
            this.contactFilter.content = this.listContacts[idx].email;
            this.addContact(idx);
        }
        else {
            this.deleteContactFilter(idx, this.listContacts[idx].filter_id);
        }
    };
    PrivateContactsComponent.prototype.fetchListContacts = function () {
        var _this = this;
        this.filterService.fetchContacts()
            .subscribe(function (data) {
            _this.listContacts = data;
        });
    };
    PrivateContactsComponent.prototype.getContacts = function () {
        var _this = this;
        this.filterService.getFiltersbyType(this.delegationId, 'contact')
            .subscribe(function (data) {
            _this.contactFilters = data;
            _this.mapStatus();
        });
    };
    ;
    PrivateContactsComponent.prototype.addContact = function (idx) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to make " + this.contactFilter.content + " contact private?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Make it Private!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.filterService.addFilter(_this.delegationId, _this.contactFilter, 'contact')
                    .subscribe(function (data) {
                    swal("Added!", "This contact has been made private", "success");
                    _this.getContacts();
                    _this.contactFilter = new Filter_1.Filter();
                });
            }
            else {
                _this.listContacts[idx].status = false;
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    ;
    PrivateContactsComponent.prototype.getDomains = function () {
        var _this = this;
        this.filterService.getFiltersbyType(this.delegationId, 'domain')
            .subscribe(function (data) {
            _this.domainFilters = data;
        });
    };
    ;
    PrivateContactsComponent.prototype.addDomain = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to make " + this.domainFilter.content + " domain private?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Make it Private!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.filterService.addFilter(_this.delegationId, _this.domainFilter, 'domain')
                    .subscribe(function (data) {
                    swal("Added!", "This domain has been made private", "success");
                    _this.getDomains();
                    _this.domainFilter = new Filter_1.Filter();
                });
            }
            else {
                _this.domainFilter = new Filter_1.Filter();
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    ;
    PrivateContactsComponent.prototype.getIndividuals = function () {
        var _this = this;
        this.filterService.getFiltersbyType(this.delegationId, 'individual')
            .subscribe(function (data) {
            _this.individualFilters = data;
        });
    };
    ;
    PrivateContactsComponent.prototype.addIndividual = function () {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to make " + this.individualFilter.content + " individual private?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Make it Private!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.filterService.addFilter(_this.delegationId, _this.individualFilter, 'individual')
                    .subscribe(function (data) {
                    swal("Added!", "This individual has been made private", "success");
                    _this.getIndividuals();
                    _this.individualFilter = new Filter_1.Filter();
                });
            }
            else {
                _this.individualFilter = new Filter_1.Filter();
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    ;
    PrivateContactsComponent.prototype.deleteDomainFilter = function (filterId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this Domain Filter?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.filterService.deleteFilter(_this.delegationId, filterId)
                .subscribe(function (data) {
                swal("Deleted!", "It has been successfully deleted.", "success");
                _this.getDomains();
            });
        });
    };
    PrivateContactsComponent.prototype.deleteIndividualFilter = function (filterId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to delete this Individual Filter?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Delete it!",
            closeOnConfirm: false
        }, function () {
            _this.filterService.deleteFilter(_this.delegationId, filterId)
                .subscribe(function (data) {
                swal("Deleted!", "It has been successfully deleted.", "success");
                _this.getIndividuals();
            });
        });
    };
    PrivateContactsComponent.prototype.deleteContactFilter = function (idx, filterId) {
        var _this = this;
        swal({
            title: "Are you sure?",
            text: "You want to Remove this Contact Filter?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Remove it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                _this.filterService.deleteFilter(_this.delegationId, filterId)
                    .subscribe(function (data) {
                    swal("Removed!", "It has been successfully removed.", "success");
                    _this.getDomains();
                });
            }
            else {
                _this.listContacts[idx].status = true;
                swal("Cancelled", "Canceled", "error");
            }
        });
    };
    PrivateContactsComponent.prototype.mapStatus = function () {
        for (var i = 0; i < this.listContacts.length; i++) {
            for (var j = 0; j < this.contactFilters.length; j++) {
                if (this.contactFilters[j].content === this.listContacts[i].email) {
                    this.listContacts[i].status = true;
                    this.listContacts[i].filter_id = this.contactFilters[j].id;
                }
            }
        }
    };
    PrivateContactsComponent = __decorate([
        core_1.Component({
            selector: 'hm-private-contacts',
            templateUrl: 'app/components/privateContacts/privateContacts.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: [filter_service_1.FilterService],
            pipes: [search_pipe_1.SearchFilter]
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams, filter_service_1.FilterService])
    ], PrivateContactsComponent);
    return PrivateContactsComponent;
}());
exports.PrivateContactsComponent = PrivateContactsComponent;
//# sourceMappingURL=privateContacts.component.js.map