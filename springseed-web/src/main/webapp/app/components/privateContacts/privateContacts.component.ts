import {Component, OnInit, AfterContentInit, AfterViewChecked, AfterContentChecked} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';

import {FilterService} from '../../services/filter.service';
import {Filter} from '../../interfaces/Filter';
import {Contact} from '../../interfaces/Contact';
import {SearchFilter} from '../../pipes/search.pipe';

declare var $: any;
declare var Materialize: any;
declare var swal: any;

@Component({
	selector: 'hm-private-contacts',
	templateUrl: 'app/components/privateContacts/privateContacts.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [FilterService],
	pipes :[SearchFilter]
})

export class PrivateContactsComponent implements OnInit {
	title: string = "Email Delegation : Which Contacts are private?";
	filter: string;
	delegationId:number;
	domainFilters: Filter[];
	individualFilters: Filter[];
	contactFilters: Filter[];
	domainFilter: Filter = new Filter();
	individualFilter: Filter = new Filter();
	contactFilter:Filter = new Filter();
	listContacts: Contact[];

	constructor(private router: Router, private routeParams: RouteParams, private filterService: FilterService) { };

	ngOnInit() {
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);
		this.fetchListContacts();
		this.getDomains();
		this.getIndividuals();
		this.getContacts();

		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		Materialize.updateTextFields();

		setTimeout(function() {
			$('select').material_select();
		});

		$('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
      // ready: function() { alert('Ready'); }, // Callback for Modal open
      // complete: function() { alert('Closed'); } // Callback for Modal close
    });

    $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrain_width: true, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: true, // Displays dropdown below the button
      alignment: 'left' // Displays dropdown with edge aligned to the left of button
    });

	}

	onChange(idx:number, event: any){
		if(event.target.checked){
			this.contactFilter.content = this.listContacts[idx].email;
			this.addContact(idx);
		}
		else{
			this.deleteContactFilter(idx, this.listContacts[idx].filter_id);
		}
	}

	fetchListContacts(){
		this.filterService.fetchContacts()
			.subscribe(
				data=> {
					this.listContacts = data;
				});
	}

	getContacts(){
		this.filterService.getFiltersbyType(this.delegationId, 'contact')
			.subscribe(
				data=> {
					this.contactFilters = data;
					this.mapStatus();
				});
	};

	addContact(idx:number){
		swal({
				title: "Are you sure?",
				text: "You want to make " + this.contactFilter.content + " contact private?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Make it Private!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
				(isConfirm:any)=>{
			 		if(isConfirm){
						this.filterService.addFilter(this.delegationId, this.contactFilter, 'contact')
							.subscribe(
								data=> {
									swal("Added!", "This contact has been made private", "success"); 
									this.getContacts();
									this.contactFilter = new Filter();
								});
						} else {
							this.listContacts[idx].status = false;
						 	swal("Cancelled", "Canceled", "error");
						}
					});
	};


	getDomains(){
		this.filterService.getFiltersbyType(this.delegationId, 'domain')
			.subscribe(
				data=> {
					this.domainFilters = data;
				});
	};

	addDomain(){

		swal({
				title: "Are you sure?",
				text: "You want to make " + this.domainFilter.content + " domain private?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Make it Private!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
			 (isConfirm:any)=>{
			 		if(isConfirm){
						this.filterService.addFilter(this.delegationId, this.domainFilter, 'domain')
							.subscribe(
								data=> {
									swal("Added!", "This domain has been made private", "success"); 
									this.getDomains();
									this.domainFilter = new Filter();
								});
						} else {
							this.domainFilter = new Filter();
						 	swal("Cancelled", "Canceled", "error");
						}
				});
	};

	getIndividuals(){
		this.filterService.getFiltersbyType(this.delegationId, 'individual')
			.subscribe(
				data=> {
					this.individualFilters = data;
				});
	};

	addIndividual(){
		swal({
				title: "Are you sure?",
				text: "You want to make " + this.individualFilter.content + " individual private?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Make it Private!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
				(isConfirm:any)=>{
					if(isConfirm){
						this.filterService.addFilter(this.delegationId, this.individualFilter, 'individual')
							.subscribe(
								data=> {
									swal("Added!", "This individual has been made private", "success"); 
									this.getIndividuals();
									this.individualFilter = new Filter();
								});
						} else{
							this.individualFilter = new Filter();
						 	swal("Cancelled", "Canceled", "error");
						}
					});
	};

	deleteDomainFilter(filterId:number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this Domain Filter?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.filterService.deleteFilter(this.delegationId, filterId)
						.subscribe(
							data=> {
								swal("Deleted!", "It has been successfully deleted.", "success"); 
								this.getDomains();
							});
					});
	}

	deleteIndividualFilter(filterId:number){
		swal({
				title: "Are you sure?",
				text: "You want to delete this Individual Filter?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Delete it!",
				closeOnConfirm: false
			},
				()=>{
					this.filterService.deleteFilter(this.delegationId, filterId)
						.subscribe(
							data=> {
								swal("Deleted!", "It has been successfully deleted.", "success"); 
								this.getIndividuals();
							});
			});
	}

	deleteContactFilter(idx:number, filterId:number){
			swal({
				title: "Are you sure?",
				text: "You want to Remove this Contact Filter?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Remove it!",
				closeOnConfirm: false,
				closeOnCancel : false
			},
				(isConfirm:any)=>{
					if(isConfirm){
						this.filterService.deleteFilter(this.delegationId, filterId)
							.subscribe(
								data=> {
									swal("Removed!", "It has been successfully removed.", "success");
									this.getDomains();
								});
					} else {
							this.listContacts[idx].status = true;
						 	swal("Cancelled", "Canceled", "error");
						}
			});
	}

	mapStatus(){
		for(let i = 0; i < this.listContacts.length; i++){
			for(let j = 0; j < this.contactFilters.length; j++){
				if( this.contactFilters[j].content === this.listContacts[i].email){
					this.listContacts[i].status = true;
					this.listContacts[i].filter_id = this.contactFilters[j].id;
				}
			}
		}
	}

}
