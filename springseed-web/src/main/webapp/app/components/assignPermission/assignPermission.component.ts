import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-assign-permission',
	templateUrl: 'app/components/assignPermission/assignPermission.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class AssignPermissionComponent implements OnInit {
	title: string = "Email Delegation : What can they do on my behalf?";
	delegationId: number;

	constructor(private router: Router, private routeParams: RouteParams) { };

	ngOnInit() {
		$("#perm").addClass("disabledbutton");
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);

	}
}
