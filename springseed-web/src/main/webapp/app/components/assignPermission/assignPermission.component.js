"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var AssignPermissionComponent = (function () {
    function AssignPermissionComponent(router, routeParams) {
        this.router = router;
        this.routeParams = routeParams;
        this.title = "Email Delegation : What can they do on my behalf?";
    }
    ;
    AssignPermissionComponent.prototype.ngOnInit = function () {
        $("#perm").addClass("disabledbutton");
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
    };
    AssignPermissionComponent = __decorate([
        core_1.Component({
            selector: 'hm-assign-permission',
            templateUrl: 'app/components/assignPermission/assignPermission.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: []
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams])
    ], AssignPermissionComponent);
    return AssignPermissionComponent;
}());
exports.AssignPermissionComponent = AssignPermissionComponent;
//# sourceMappingURL=assignPermission.component.js.map