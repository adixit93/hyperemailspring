import {Component, OnInit} from '@angular/core';
import {Router, ROUTER_DIRECTIVES, RouteParams} from '@angular/router-deprecated';


declare var $: any;
declare var Materialize: any;

@Component({
	selector: 'hm-email-delegation',
	templateUrl: 'app/components/emailDelegation/emailDelegation.component.html',
	directives: [ROUTER_DIRECTIVES],
	providers: []
})

export class EmailDelegationComponent implements OnInit {
	title: string = "Email Delegation";
	constructor(private router: Router, private routeParams: RouteParams) { };
	delegationId: number;

	ngOnInit() {
		this.delegationId = parseInt(this.routeParams.get('delegationId'),10);
		$('.collapsible').collapsible({
      accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });

		Materialize.updateTextFields();
	}
}
