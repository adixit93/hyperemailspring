"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var EmailDelegationComponent = (function () {
    function EmailDelegationComponent(router, routeParams) {
        this.router = router;
        this.routeParams = routeParams;
        this.title = "Email Delegation";
    }
    ;
    EmailDelegationComponent.prototype.ngOnInit = function () {
        this.delegationId = parseInt(this.routeParams.get('delegationId'), 10);
        $('.collapsible').collapsible({
            accordion: false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
        Materialize.updateTextFields();
    };
    EmailDelegationComponent = __decorate([
        core_1.Component({
            selector: 'hm-email-delegation',
            templateUrl: 'app/components/emailDelegation/emailDelegation.component.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES],
            providers: []
        }), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, router_deprecated_1.RouteParams])
    ], EmailDelegationComponent);
    return EmailDelegationComponent;
}());
exports.EmailDelegationComponent = EmailDelegationComponent;
//# sourceMappingURL=emailDelegation.component.js.map