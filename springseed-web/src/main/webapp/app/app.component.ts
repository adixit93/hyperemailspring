import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, RouteParams, RouteConfig, Router} from '@angular/router-deprecated';

import {SignupComponent} from './components/signup/signup.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {MyAccountsComponent} from './components/myaccounts/myaccounts.component';
import {EmailDelegationComponent} from './components/emailDelegation/emailDelegation.component';
import {DelegationComponent} from './components/delegation/delegation.component';
import {AddDelegatesComponent} from './components/addDelegates/addDelegates.component';
import {FolderPermissionComponent} from './components/folderPermission/folderPermission.component';
import {PrivateMessagesComponent} from './components/privateMessages/privateMessages.component';
import {PrivateContactsComponent} from './components/privateContacts/privateContacts.component';
import {AssignPermissionComponent} from './components/assignPermission/assignPermission.component';


import {UserService} from './services/user.service';
import {User} from './interfaces/User';

import {NavbarComponent} from './shared/navbar/navbar.component';
import {FooterComponent} from './shared/footer/footer.component';

@Component({
    selector: 'hyper-email',
    directives: [ROUTER_DIRECTIVES, NavbarComponent, FooterComponent],
    templateUrl: 'app/app.component.html',
    providers: [UserService],
    outputs : ['user']
})

@RouteConfig([
	{ path: '/dashboard', name: 'Dashboard', component: DashboardComponent, useAsDefault : true },
	{ path: '/accounts', name: 'Accounts', component: MyAccountsComponent },
	{ path: '/delegation/:delegationId', name: 'Delegation', component: EmailDelegationComponent },
	{ path: '/delegation/:delegationId/delegates', name: 'AddDelegates', component: AddDelegatesComponent },
	{ path: '/delegation/:delegationId/folders', name: 'FolderAccess', component: FolderPermissionComponent },
	{ path: '/delegation/:delegationId/contacts', name: 'PrivateContacts', component: PrivateContactsComponent },
	{ path: '/delegation/:delegationId/messages', name: 'PrivateMessages', component: PrivateMessagesComponent },
	{ path: '/delegation/:delegationId/permissions', name: 'Permissions', component: AssignPermissionComponent },
	{ path: '/delegations', name: 'EmailDelegation', component: DelegationComponent },
])

export class AppComponent implements OnInit {
	title: String = "Main App";
	public user : User = new User();
	constructor(private router: Router, private userService: UserService) { };

	ngOnInit() {
		this.userService.getUserDetails()
			.subscribe(
				data => {
					this.user = data;
					localStorage.setItem('user',JSON.stringify(data));
				});
	}
}
