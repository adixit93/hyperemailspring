import {Component} from '@angular/core';

@Component({
    selector: 'pre-loader',
    directives: [],
    templateUrl : 'app/shared/preloader/preloader.html'
})
export class PreLoader {
}