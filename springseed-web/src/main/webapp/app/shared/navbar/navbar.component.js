"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var user_service_1 = require('../../services/user.service');
var User_1 = require('../../interfaces/User');
var NavbarComponent = (function () {
    function NavbarComponent(_elRef, userService) {
        this._elRef = _elRef;
        this.userService = userService;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        $('.button-collapse').sideNav({
            menuWidth: 300,
            edge: 'left',
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });
        $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true,
            hover: true,
            gutter: 0,
            belowOrigin: true,
            alignment: 'right' // Displays dropdown with edge aligned to the left of button
        });
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', User_1.User)
    ], NavbarComponent.prototype, "user", void 0);
    NavbarComponent = __decorate([
        core_1.Component({
            selector: 'hm-navbar',
            templateUrl: 'app/shared/navbar/navbar.html',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, router_deprecated_1.RouterLink],
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, user_service_1.UserService])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;
//# sourceMappingURL=navbar.component.js.map