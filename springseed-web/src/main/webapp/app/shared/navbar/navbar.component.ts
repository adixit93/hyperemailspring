import {Component, OnInit, ElementRef, Input} from '@angular/core';
import {ROUTER_DIRECTIVES, RouteParams, RouteConfig, Router, RouterLink} from '@angular/router-deprecated';

import {UserService} from '../../services/user.service';
import {User} from '../../interfaces/User';

declare var $: any;

@Component({
    selector: 'hm-navbar',
    templateUrl: 'app/shared/navbar/navbar.html',
		directives: [ROUTER_DIRECTIVES, RouterLink],
		providers: [UserService]
})

export class NavbarComponent{
	@Input() user:User;
	constructor(private _elRef: ElementRef, private userService: UserService) {

	}

	ngOnInit() {
	
		$('.button-collapse').sideNav({
			menuWidth: 300, // Default is 240
			edge: 'left', // Choose the horizontal origin
			closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
		});


		$('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: true, // Does not change width of dropdown to that of the activator
			hover: true, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'right' // Displays dropdown with edge aligned to the left of button
		});
	}

}
