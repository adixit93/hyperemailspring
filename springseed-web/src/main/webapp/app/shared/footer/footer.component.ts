import {Component, OnInit, ElementRef} from '@angular/core';
import {ROUTER_DIRECTIVES, RouteParams, RouteConfig, Router, RouterLink} from '@angular/router-deprecated';


declare var $: any;

@Component({
	selector: 'hm-footer',
	templateUrl: 'app/shared/footer/footer.component.html',
		directives: [ROUTER_DIRECTIVES, RouterLink]
})

export class FooterComponent {

}
