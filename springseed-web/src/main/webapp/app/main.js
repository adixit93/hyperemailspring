"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var router_deprecated_1 = require('@angular/router-deprecated');
var httpClient_service_1 = require('./services/httpClient.service');
var user_service_1 = require('./services/user.service');
var http_1 = require('@angular/http');
var app_component_1 = require('./app.component');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [router_deprecated_1.ROUTER_PROVIDERS, http_1.HTTP_PROVIDERS, httpClient_service_1.HttpClient, user_service_1.UserService
]);
//# sourceMappingURL=main.js.map