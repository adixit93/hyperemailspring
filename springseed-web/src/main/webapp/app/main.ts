import {bootstrap}    from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router-deprecated';

import {HttpClient} from './services/httpClient.service';
import {UserService} from './services/user.service';

import { HTTP_PROVIDERS, XHRBackend } from '@angular/http';

import {AppComponent} from './app.component';

bootstrap(AppComponent, [ROUTER_PROVIDERS, HTTP_PROVIDERS, HttpClient, UserService
	]);