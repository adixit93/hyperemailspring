"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require('rxjs/Observable');
var user_service_1 = require('./user.service');
var httpClient_service_1 = require("./httpClient.service");
require('rxjs/Rx');
var DelegationService = (function () {
    function DelegationService(userService, httpClient) {
        this.userService = userService;
        this.httpClient = httpClient;
    }
    ;
    DelegationService.prototype.addDelegation = function (delegation) {
        var userID = this.userService.getUserId();
        delegation.user.id = userID;
        return this.httpClient.post("/users/" + userID + "/delegation", delegation)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.deleteDelegation = function (delegationId) {
        var userID = this.userService.getUserId();
        return this.httpClient.delete("/users/" + userID + "/delegation/" + delegationId)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.getDelegations = function () {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/delegation")
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.getDelegates = function (delegationId) {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/delegation/" + delegationId + "/delegate")
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.addDelegate = function (delegation) {
        var userID = this.userService.getUserId();
        return this.httpClient.post("/users/" + userID + "/delegation/" + delegation.id + "/delegate", delegation)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.deleteDelegate = function (delegationId, delegateId) {
        var userID = this.userService.getUserId();
        return this.httpClient.delete("/users/" + userID + "/delegation/" + delegationId + "/delegate/" + delegateId)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    DelegationService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    DelegationService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    DelegationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [user_service_1.UserService, httpClient_service_1.HttpClient])
    ], DelegationService);
    return DelegationService;
}());
exports.DelegationService = DelegationService;
//# sourceMappingURL=delegation.service.js.map