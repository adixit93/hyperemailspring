"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require('rxjs/Observable');
require('rxjs/Rx');
var UserEmailAccount_1 = require('../interfaces/UserEmailAccount');
var user_service_1 = require('./user.service');
var httpClient_service_1 = require("./httpClient.service");
var FolderService = (function () {
    function FolderService(userService, httpClient) {
        this.userService = userService;
        this.httpClient = httpClient;
    }
    ;
    FolderService.prototype.getFolders = function (accountId) {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/accounts/" + accountId + "/folders")
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    FolderService.prototype.getAccessFolders = function (delegationId, accountId) {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/accounts/" + accountId + "/delegation/" + delegationId + "/folderAccesses")
            .map(this.extractData)
            .catch(this.handleError);
    };
    FolderService.prototype.addFolder = function (delegationId, accountId, folder) {
        var userID = this.userService.getUserId();
        folder.userEmailAccount = new UserEmailAccount_1.UserEmailAccount();
        folder.userEmailAccount.id = accountId;
        folder.identifier = "";
        console.log(folder);
        return this.httpClient.post("/users/" + userID + "/accounts/" + accountId + "/delegation/" + delegationId + "/folderAccesses", folder)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    FolderService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    FolderService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    FolderService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [user_service_1.UserService, httpClient_service_1.HttpClient])
    ], FolderService);
    return FolderService;
}());
exports.FolderService = FolderService;
//# sourceMappingURL=folder.service.js.map