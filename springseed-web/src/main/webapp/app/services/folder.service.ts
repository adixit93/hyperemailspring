import {Injectable, Inject} from "@angular/core";
import {Response} from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/Rx';

import {UserEmailAccount} from '../interfaces/UserEmailAccount';
import {Folder} from '../interfaces/Folder';
import {UserService} from './user.service';
import {HttpClient} from "./httpClient.service";

@Injectable()
export class FolderService{

	constructor(private userService: UserService, private httpClient: HttpClient){
	};

	getFolders(accountId:number): Observable<Folder[]>{
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/accounts/" + accountId + "/folders")
						.map(this.extractData)
						.catch(this.handleError);
	};

	getAccessFolders(delegationId: number, accountId: number){
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/accounts/" + accountId + "/delegation/" + delegationId + "/folderAccesses")
						.map(this.extractData)
						.catch(this.handleError);
	}

	addFolder(delegationId: number, accountId: number, folder: Folder): Observable<Folder>{
		let userID = this.userService.getUserId();
		folder.userEmailAccount = new UserEmailAccount();
		folder.userEmailAccount.id = accountId;
		folder.identifier = "";
		console.log(folder);
		return this.httpClient.post("/users/" + userID +"/accounts/" + accountId + "/delegation/" + delegationId + "/folderAccesses", folder)
						.map(this.extractData)
						.catch(this.handleError);
	};

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  	console.error(errMsg);
    return Observable.throw(errMsg);
  }
}