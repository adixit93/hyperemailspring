import {Injectable, Inject} from "@angular/core";
import {Response} from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import {UserEmailAccount} from '../interfaces/UserEmailAccount';
import {Filter} from '../interfaces/Filter';
import {Contact} from '../interfaces/Contact';

import {UserService} from './user.service';
import {HttpClient} from "./httpClient.service";
import 'rxjs/Rx';

import {FilterType} from '../interfaces/filter.enum'

@Injectable()
export class FilterService{

	constructor(private userService: UserService, private httpClient: HttpClient){
	};


	fetchContacts(): Observable<Contact[]> {
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/contacts")
						.map(this.extractData)
						.catch(this.handleError);
	}

	addFilter(delegationId: number, filter: Filter, type: string): Observable<Filter>{
		let userID = this.userService.getUserId();
		filter.delegation.id = delegationId;
		filter.filter_id = FilterType[type];
		return this.httpClient.post("/users/" + userID +"/delegation/" + delegationId + "/accesses?type="+type, filter)
						.map(this.extractData)
						.catch(this.handleError);
	};

	getFiltersbyType(delegationId: number, type: string): Observable<Filter[]>{
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/delegation/" + delegationId + "/filters?type="+type)
						.map(this.extractData)
						.catch(this.handleError);
	};

	deleteFilter(delegationId:number, filterId: number){
		let userID = this.userService.getUserId();
		return this.httpClient.delete("/users/" + userID +"/delegation/" + delegationId + "/accesses/" + filterId)
						.map(this.extractData)
						.catch(this.handleError);
	};

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  	console.error(errMsg);
    return Observable.throw(errMsg);
  }
}