"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var HttpClient = (function () {
    function HttpClient(http) {
        this.http = http;
        this.http = http;
    }
    HttpClient.prototype.addHeaders = function () {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return options;
    };
    HttpClient.prototype.get = function (url) {
        var options = this.addHeaders();
        return this.http.get(url, options);
    };
    HttpClient.prototype.post = function (url, data) {
        var options = this.addHeaders();
        return this.http.post(url, data, options);
    };
    HttpClient.prototype.delete = function (url) {
        var options = this.addHeaders();
        return this.http.delete(url, options);
    };
    HttpClient.prototype.put = function (url, data) {
        var options = this.addHeaders();
        return this.http.put(url, data, options);
    };
    HttpClient = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], HttpClient);
    return HttpClient;
}());
exports.HttpClient = HttpClient;
//# sourceMappingURL=httpClient.service.js.map