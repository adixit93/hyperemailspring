"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var httpClient_service_1 = require("./httpClient.service");
var Observable_1 = require('rxjs/Observable');
require('rxjs/Rx');
;
var UserService = (function () {
    function UserService(httpClient) {
        this.httpClient = httpClient;
    }
    ;
    UserService.prototype.getUserDetails = function () {
        return this.httpClient.get("/users/userid")
            .map(this.extractData)
            .catch(this.handleError);
    };
    UserService.prototype.getUsers = function () {
        return this.httpClient.get("/users")
            .map(this.extractData)
            .catch(this.handleError);
    };
    UserService.prototype.getUserId = function () {
        return JSON.parse(localStorage.getItem('user')).id;
    };
    UserService.prototype.getName = function () {
        var user = JSON.parse(localStorage.getItem('user'));
        return user;
    };
    UserService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    UserService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    UserService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [httpClient_service_1.HttpClient])
    ], UserService);
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map