import {Injectable} from "@angular/core";
import {Http, Headers, HTTP_PROVIDERS} from '@angular/http';

@Injectable()
export class AuthenticationService{
	token: string;
	// var headers = new Headers();
	// headers.append('Content-Type', 'application/x-www-form-urlencoded');
	constructor(){

	}

	getUserId(){
		return localStorage.getItem('user');
	}

	signup(details:any){
	
	}
	
	login(username : string, password : string , callback :any){

		this.token = "login"
		if (username == 'test' && password == 'test') {
			localStorage.setItem('token', this.token)
			callback("Authentication Success");
		}
		else callback("Authentication Failure");
	}

	isloggedin() {
		let token = localStorage.getItem('user');
		if (token == null || token === undefined) return false;
		else return true;
	}

	logout(){
		this.token = undefined;
		localStorage.removeItem('token')

	}
}