"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require('rxjs/Observable');
var user_service_1 = require('./user.service');
var httpClient_service_1 = require("./httpClient.service");
require('rxjs/Rx');
var filter_enum_1 = require('../interfaces/filter.enum');
var FilterService = (function () {
    function FilterService(userService, httpClient) {
        this.userService = userService;
        this.httpClient = httpClient;
    }
    ;
    FilterService.prototype.fetchContacts = function () {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/contacts")
            .map(this.extractData)
            .catch(this.handleError);
    };
    FilterService.prototype.addFilter = function (delegationId, filter, type) {
        var userID = this.userService.getUserId();
        filter.delegation.id = delegationId;
        filter.filter_id = filter_enum_1.FilterType[type];
        return this.httpClient.post("/users/" + userID + "/delegation/" + delegationId + "/accesses?type=" + type, filter)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    FilterService.prototype.getFiltersbyType = function (delegationId, type) {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/delegation/" + delegationId + "/filters?type=" + type)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    FilterService.prototype.deleteFilter = function (delegationId, filterId) {
        var userID = this.userService.getUserId();
        return this.httpClient.delete("/users/" + userID + "/delegation/" + delegationId + "/accesses/" + filterId)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    FilterService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    FilterService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    FilterService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [user_service_1.UserService, httpClient_service_1.HttpClient])
    ], FilterService);
    return FilterService;
}());
exports.FilterService = FilterService;
//# sourceMappingURL=filter.service.js.map