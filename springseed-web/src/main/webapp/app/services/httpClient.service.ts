import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, Response, HTTP_PROVIDERS} from '@angular/http';

@Injectable()
export class HttpClient {
	constructor(private http: Http) {
		this.http = http;
	}

	addHeaders() {
		let headers = new Headers({	 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		return options;
	}

	get(url:string) {
		let options = this.addHeaders();
		return this.http.get(url, options);
	}

	post(url:string, data:Object) {
		let options = this.addHeaders();
		return this.http.post(url, data, options);
	}

	delete(url:string) {
		let options = this.addHeaders();
		return this.http.delete(url, options);
	}

	put(url:string, data:Object) {
		let options = this.addHeaders();
		return this.http.put(url, data, options);
	}
}