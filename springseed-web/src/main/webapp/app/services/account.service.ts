import {Injectable, Inject} from "@angular/core";
import {Response} from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import {UserEmailAccount} from '../interfaces/UserEmailAccount';
import {UserService} from './user.service';
import {HttpClient} from "./httpClient.service";
import 'rxjs/Rx';

@Injectable()
export class AccountService{

	constructor(private userService: UserService, private httpClient: HttpClient){
	};


	addAccount(userEmailAccount:UserEmailAccount): Observable<UserEmailAccount>{
		let userID = this.userService.getUserId();
		userEmailAccount.user.id = userID;
		return this.httpClient.post("/users/" + userID +"/accounts",userEmailAccount)
						.map(this.extractData)
						.catch(this.handleError);
	};

	getAccounts(): Observable<UserEmailAccount[]>{
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/accounts")
						.map(this.extractData)
						.catch(this.handleError);
	};

	deleteAccount(accountId:number){
		let userID = this.userService.getUserId();
		return this.httpClient.delete("/users/" + userID +"/accounts/"+accountId)
						.map(this.extractData)
						.catch(this.handleError);
	};

	updateAccount(accountId:number, userEmailAccount:UserEmailAccount): Observable<UserEmailAccount>{
		let userID = this.userService.getUserId();
		userEmailAccount.user.id = userID;
		
		return this.httpClient.put("/users/" + userID +"/accounts/" + accountId ,userEmailAccount)
						.map(this.extractData)
						.catch(this.handleError);
	};

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  	console.error(errMsg);
    return Observable.throw(errMsg);
  }
}