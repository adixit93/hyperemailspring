import {Injectable, Inject} from "@angular/core";
import {HttpClient} from "./httpClient.service";
import {Response} from "@angular/http";
import { Observable }     from 'rxjs/Observable';
import 'rxjs/Rx';;

import {UserEmailAccount} from '../interfaces/UserEmailAccount';
import {User} from '../interfaces/User';

@Injectable()
export class UserService{
	
	constructor(private httpClient: HttpClient){
	};


	getUserDetails(): Observable<User>{
		return this.httpClient.get("/users/userid")
			.map(this.extractData)
			.catch(this.handleError);
	}

	getUsers(): Observable<User[]>{
		return this.httpClient.get("/users")
							.map(this.extractData)
							.catch(this.handleError);
	}

	getUserId(){
		return JSON.parse(localStorage.getItem('user')).id;
	}

	getName(){
		let user:User = JSON.parse(localStorage.getItem('user'));	
		return user;
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  	console.error(errMsg);
    return Observable.throw(errMsg);
  }
}