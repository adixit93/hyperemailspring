import {Injectable, Inject} from "@angular/core";
import {Response} from '@angular/http';
import { Observable }     from 'rxjs/Observable';

import {UserEmailAccount} from '../interfaces/UserEmailAccount';
import {Delegation} from '../interfaces/Delegation';
import {UserService} from './user.service';
import {HttpClient} from "./httpClient.service";
import 'rxjs/Rx';

@Injectable()
export class DelegationService{

	constructor(private userService: UserService, private httpClient: HttpClient){
	};


	addDelegation(delegation:Delegation): Observable<Delegation>{
		let userID = this.userService.getUserId();
		delegation.user.id = userID;
		return this.httpClient.post("/users/" + userID +"/delegation",delegation)
						.map(this.extractData)
						.catch(this.handleError);
	};

	deleteDelegation(delegationId:number){
		let userID = this.userService.getUserId();
		return this.httpClient.delete("/users/" + userID +"/delegation/" + delegationId)
						.map(this.extractData)
						.catch(this.handleError);
	};

	getDelegations(): Observable<Delegation[]>{
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/delegation")
						.map(this.extractData)
						.catch(this.handleError);
	};

	getDelegates(delegationId:number):Observable<Delegation[]>{
		let userID = this.userService.getUserId();
		return this.httpClient.get("/users/" + userID +"/delegation/" + delegationId + "/delegate")
						.map(this.extractData)
						.catch(this.handleError);
	};

	addDelegate(delegation: Delegation):Observable<Delegation>{
		let userID = this.userService.getUserId();
		return this.httpClient.post("/users/" + userID +"/delegation/" + delegation.id + "/delegate", delegation)
						.map(this.extractData)
						.catch(this.handleError);
	};

	deleteDelegate(delegationId:number, delegateId: number){
		let userID = this.userService.getUserId();
		return this.httpClient.delete("/users/" + userID +"/delegation/" + delegationId + "/delegate/" + delegateId)
						.map(this.extractData)
						.catch(this.handleError);
	};

	private extractData(res: Response) {
		let body = res.json();
		return body || {};
	}

	private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  	console.error(errMsg);
    return Observable.throw(errMsg);
  }
}