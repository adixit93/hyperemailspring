"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require('rxjs/Observable');
var user_service_1 = require('./user.service');
var httpClient_service_1 = require("./httpClient.service");
require('rxjs/Rx');
var AccountService = (function () {
    function AccountService(userService, httpClient) {
        this.userService = userService;
        this.httpClient = httpClient;
    }
    ;
    AccountService.prototype.addAccount = function (userEmailAccount) {
        var userID = this.userService.getUserId();
        userEmailAccount.user.id = userID;
        return this.httpClient.post("/users/" + userID + "/accounts", userEmailAccount)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    AccountService.prototype.getAccounts = function () {
        var userID = this.userService.getUserId();
        return this.httpClient.get("/users/" + userID + "/accounts")
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    AccountService.prototype.deleteAccount = function (accountId) {
        var userID = this.userService.getUserId();
        return this.httpClient.delete("/users/" + userID + "/accounts/" + accountId)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    AccountService.prototype.updateAccount = function (accountId, userEmailAccount) {
        var userID = this.userService.getUserId();
        userEmailAccount.user.id = userID;
        return this.httpClient.put("/users/" + userID + "/accounts/" + accountId, userEmailAccount)
            .map(this.extractData)
            .catch(this.handleError);
    };
    ;
    AccountService.prototype.extractData = function (res) {
        var body = res.json();
        return body || {};
    };
    AccountService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    AccountService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [user_service_1.UserService, httpClient_service_1.HttpClient])
    ], AccountService);
    return AccountService;
}());
exports.AccountService = AccountService;
//# sourceMappingURL=account.service.js.map