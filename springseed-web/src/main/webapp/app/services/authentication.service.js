"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var AuthenticationService = (function () {
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/x-www-form-urlencoded');
    function AuthenticationService() {
    }
    AuthenticationService.prototype.getUserId = function () {
        return localStorage.getItem('user');
    };
    AuthenticationService.prototype.signup = function (details) {
    };
    AuthenticationService.prototype.login = function (username, password, callback) {
        this.token = "login";
        if (username == 'test' && password == 'test') {
            localStorage.setItem('token', this.token);
            callback("Authentication Success");
        }
        else
            callback("Authentication Failure");
    };
    AuthenticationService.prototype.isloggedin = function () {
        var token = localStorage.getItem('user');
        if (token == null || token === undefined)
            return false;
        else
            return true;
    };
    AuthenticationService.prototype.logout = function () {
        this.token = undefined;
        localStorage.removeItem('token');
    };
    AuthenticationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map