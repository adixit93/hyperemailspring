"use strict";
var User_1 = require('./User');
var UserEmailAccount_1 = require('./UserEmailAccount');
var Folder = (function () {
    function Folder() {
        this.userEmailAccount = new UserEmailAccount_1.UserEmailAccount();
        this.user = new User_1.User();
    }
    return Folder;
}());
exports.Folder = Folder;
//# sourceMappingURL=Folder.js.map