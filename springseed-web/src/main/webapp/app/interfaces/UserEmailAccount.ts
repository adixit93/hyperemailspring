import {User} from './User';

export class UserEmailAccount{
	public id: number;
	public email: string;
	public password: string;
	public imapPort: number;
	public smtpPort: number;
	public imapDomain: string;
	public smtpDomain: string;
	public user: User;

	constructor(){
		this.user = new User();
	}
}