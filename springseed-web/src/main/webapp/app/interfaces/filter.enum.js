"use strict";
(function (FilterType) {
    FilterType[FilterType["contact"] = 1] = "contact";
    FilterType[FilterType["individual"] = 2] = "individual";
    FilterType[FilterType["domain"] = 3] = "domain";
    FilterType[FilterType["keyword"] = 4] = "keyword";
})(exports.FilterType || (exports.FilterType = {}));
var FilterType = exports.FilterType;
//# sourceMappingURL=filter.enum.js.map