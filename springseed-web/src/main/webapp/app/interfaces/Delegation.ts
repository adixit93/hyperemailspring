import {User} from './User';

export class Delegation{
	public name: string;
	public id: number;
	public user: User;

	constructor(){
		this.user = new User();
	}
}