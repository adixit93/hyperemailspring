import {User} from './User';
import {UserEmailAccount} from './UserEmailAccount';

export class Folder{
	public name: string;
	public identifier: string;
	public userEmailAccount : UserEmailAccount;
	public user: User;

	constructor(){
		this.userEmailAccount = new UserEmailAccount();
		this.user = new User();
	}
}
