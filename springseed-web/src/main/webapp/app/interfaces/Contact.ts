import {UserEmailAccount} from './UserEmailAccount';

export class Contact{
	public filter_id: number;
	public email: string;
	public countMail: number;
	public userEmailAccount : UserEmailAccount;
	public status: boolean;

	constructor(){
		this.userEmailAccount = new UserEmailAccount();
	}
}
