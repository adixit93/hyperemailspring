import {User} from './User';
import {Delegation} from './Delegation';

export class Filter{
	public content: string;
	public id: number;
	public delegation: Delegation;
	public filter_id: number;

	constructor(){
		this.delegation = new Delegation();
	}
}