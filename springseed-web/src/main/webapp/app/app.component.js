"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var dashboard_component_1 = require('./components/dashboard/dashboard.component');
var myaccounts_component_1 = require('./components/myaccounts/myaccounts.component');
var emailDelegation_component_1 = require('./components/emailDelegation/emailDelegation.component');
var delegation_component_1 = require('./components/delegation/delegation.component');
var addDelegates_component_1 = require('./components/addDelegates/addDelegates.component');
var folderPermission_component_1 = require('./components/folderPermission/folderPermission.component');
var privateMessages_component_1 = require('./components/privateMessages/privateMessages.component');
var privateContacts_component_1 = require('./components/privateContacts/privateContacts.component');
var assignPermission_component_1 = require('./components/assignPermission/assignPermission.component');
var user_service_1 = require('./services/user.service');
var User_1 = require('./interfaces/User');
var navbar_component_1 = require('./shared/navbar/navbar.component');
var footer_component_1 = require('./shared/footer/footer.component');
var AppComponent = (function () {
    function AppComponent(router, userService) {
        this.router = router;
        this.userService = userService;
        this.title = "Main App";
        this.user = new User_1.User();
    }
    ;
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getUserDetails()
            .subscribe(function (data) {
            _this.user = data;
            localStorage.setItem('user', JSON.stringify(data));
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'hyper-email',
            directives: [router_deprecated_1.ROUTER_DIRECTIVES, navbar_component_1.NavbarComponent, footer_component_1.FooterComponent],
            templateUrl: 'app/app.component.html',
            providers: [user_service_1.UserService],
            outputs: ['user']
        }),
        router_deprecated_1.RouteConfig([
            { path: '/dashboard', name: 'Dashboard', component: dashboard_component_1.DashboardComponent, useAsDefault: true },
            { path: '/accounts', name: 'Accounts', component: myaccounts_component_1.MyAccountsComponent },
            { path: '/delegation/:delegationId', name: 'Delegation', component: emailDelegation_component_1.EmailDelegationComponent },
            { path: '/delegation/:delegationId/delegates', name: 'AddDelegates', component: addDelegates_component_1.AddDelegatesComponent },
            { path: '/delegation/:delegationId/folders', name: 'FolderAccess', component: folderPermission_component_1.FolderPermissionComponent },
            { path: '/delegation/:delegationId/contacts', name: 'PrivateContacts', component: privateContacts_component_1.PrivateContactsComponent },
            { path: '/delegation/:delegationId/messages', name: 'PrivateMessages', component: privateMessages_component_1.PrivateMessagesComponent },
            { path: '/delegation/:delegationId/permissions', name: 'Permissions', component: assignPermission_component_1.AssignPermissionComponent },
            { path: '/delegations', name: 'EmailDelegation', component: delegation_component_1.DelegationComponent },
        ]), 
        __metadata('design:paramtypes', [router_deprecated_1.Router, user_service_1.UserService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map