import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {User} from '../interfaces/User';

@Pipe({
	name: 'searchFilter'
})

export class SearchFilter implements PipeTransform {
	transform(users: Object[], args: Object): any {
		let filter = args;
		if (filter['username'] && Array.isArray(users)) {
			let filterKeys = Object.keys(filter);
			return users.filter(item =>
				filterKeys.reduce((memo, keyName) =>
					memo && item[keyName].toLocaleLowerCase().indexOf(filter[keyName].toLocaleLowerCase()) > -1 , true));
		} else if (filter['email'] && Array.isArray(users)) {
			let filterKeys = Object.keys(filter);
			return users.filter(item =>
				filterKeys.reduce((memo, keyName) =>
					memo && item[keyName].toLocaleLowerCase().indexOf(filter[keyName].toLocaleLowerCase()) > -1 , true));
		} 

		else {
			return users;
		}
	}
}