import { SearchFilter } from '../pipes/search.pipe';

describe('SearchFilter Pipe', () => {
  let pipe: SearchFilter;
  beforeEach(() => {
    pipe = new SearchFilter();
  });

  it('transforms array by filtering the objects having "adi" as substr in username field', () => {
    let inp: Object[] = [{"username" : "aditya"},{"username" : "jklm"},{"username" : "ssdaditya"},{"username" : "sdsaditya"}];
    let out: Object[] = [{"username" : "aditya"},{"username" : "ssdaditya"},{"username" : "sdsaditya"}];
    let args : Object = {"username" : "adi"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });

  it('transforms array by filtering the objects having "ya" as substr in email field', () => {
    let inp: Object[] = [{"email" : "aditya@gmail.com"},{"email" : "shreya@gmail.com"},{"email" : "adi@gmail.com"},{"email" : "ssadi@gmail.com"}];
    let out: Object[] = [{"email" : "aditya@gmail.com"},{"email" : "shreya@gmail.com"}];
    let args : Object = {"email" : "ya"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });

  it('transforms array by filtering the objects having "gmail" as substr in email field', () => {
    let inp: Object[] = [{"email" : "aditya@gmail.com"},{"email" : "shreya@gmail.com"},{"email" : "adi@gmail.com"},{"email" : "ssadi@gmail.com"}];
    let out: Object[] = [{"email" : "aditya@gmail.com"},{"email" : "shreya@gmail.com"},{"email" : "adi@gmail.com"},{"email" : "ssadi@gmail.com"}];
    let args : Object = {"email" : "gmail"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });

  it('transforms array by filtering the objects having "yahoo" as substr in email field', () => {
    let inp: Object[] = [{"email" : "aditya@gmail.com"},{"email" : "shreya@gmail.com"},{"email" : "adi@yahoo.com"},{"email" : "ssadi@gmail.com"}];
    let out: Object[] = [{"email" : "adi@yahoo.com"}];
    let args : Object = {"email" : "yahoo"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });

  it('transforms array by filtering the objects having "ya" as substr in username field', () => {
    let inp: Object[] = [{"username" : "aditya"},{"username" : "pragya"},{"username" : "adya"},{"username" : "shreya"},{"username" : "mrityunjay"},{"username" : "durgesh"}];
    let out: Object[] = [{"username" : "aditya"},{"username" : "pragya"},{"username" : "adya"},{"username" : "shreya"}];
    let args : Object = {"username" : "ya"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });

  it('transforms array by filtering the objects having "ty" as substr in username field', () => {
    let inp: Object[] = [{"username" : "aditya"},{"username" : "pragya"},{"username" : "adya"},{"username" : "shreya"},{"username" : "mrityunjay"},{"username" : "durgesh"}];
    let out: Object[] = [{"username" : "aditya"},{"username" : "mrityunjay"}];
    let args : Object = {"username" : "ty"};
    expect(pipe.transform(inp, args)).toEqual(out);
  });
});