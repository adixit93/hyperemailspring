"use strict";
var search_pipe_1 = require('../pipes/search.pipe');
describe('SearchFilter Pipe', function () {
    var pipe;
    beforeEach(function () {
        pipe = new search_pipe_1.SearchFilter();
    });
    it('transforms array by filtering the objects having "adi" as substr in username field', function () {
        var inp = [{ "username": "aditya" }, { "username": "jklm" }, { "username": "ssdaditya" }, { "username": "sdsaditya" }];
        var out = [{ "username": "aditya" }, { "username": "ssdaditya" }, { "username": "sdsaditya" }];
        var args = { "username": "adi" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
    it('transforms array by filtering the objects having "ya" as substr in email field', function () {
        var inp = [{ "email": "aditya@gmail.com" }, { "email": "shreya@gmail.com" }, { "email": "adi@gmail.com" }, { "email": "ssadi@gmail.com" }];
        var out = [{ "email": "aditya@gmail.com" }, { "email": "shreya@gmail.com" }];
        var args = { "email": "ya" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
    it('transforms array by filtering the objects having "gmail" as substr in email field', function () {
        var inp = [{ "email": "aditya@gmail.com" }, { "email": "shreya@gmail.com" }, { "email": "adi@gmail.com" }, { "email": "ssadi@gmail.com" }];
        var out = [{ "email": "aditya@gmail.com" }, { "email": "shreya@gmail.com" }, { "email": "adi@gmail.com" }, { "email": "ssadi@gmail.com" }];
        var args = { "email": "gmail" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
    it('transforms array by filtering the objects having "yahoo" as substr in email field', function () {
        var inp = [{ "email": "aditya@gmail.com" }, { "email": "shreya@gmail.com" }, { "email": "adi@yahoo.com" }, { "email": "ssadi@gmail.com" }];
        var out = [{ "email": "adi@yahoo.com" }];
        var args = { "email": "yahoo" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
    it('transforms array by filtering the objects having "ya" as substr in username field', function () {
        var inp = [{ "username": "aditya" }, { "username": "pragya" }, { "username": "adya" }, { "username": "shreya" }, { "username": "mrityunjay" }, { "username": "durgesh" }];
        var out = [{ "username": "aditya" }, { "username": "pragya" }, { "username": "adya" }, { "username": "shreya" }];
        var args = { "username": "ya" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
    it('transforms array by filtering the objects having "ty" as substr in username field', function () {
        var inp = [{ "username": "aditya" }, { "username": "pragya" }, { "username": "adya" }, { "username": "shreya" }, { "username": "mrityunjay" }, { "username": "durgesh" }];
        var out = [{ "username": "aditya" }, { "username": "mrityunjay" }];
        var args = { "username": "ty" };
        expect(pipe.transform(inp, args)).toEqual(out);
    });
});
//# sourceMappingURL=search.pipe.spec.js.map