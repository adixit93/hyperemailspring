// import {Http, BaseRequestOptions, Response} from '@angular/http';
// import {MockBackend} from '@angular/http/testing';
// import {Injector, provide} from '@angular/core';
// it('should get a response', () => {
//   var connection; //this will be set when a new connection is emitted from the backend.
//   var text; //this will be set from mock response
//   var injector = Injector.resolveAndCreate([
//     MockBackend,
//     {provide: Http, useFactory: (backend, options) => {
//       return new Http(backend, options);
//     }, deps: [MockBackend, BaseRequestOptions]}]);
//   var backend = injector.get(MockBackend);
//   var http = injector.get(Http);
//   backend.connections.subscribe(c => connection = c);
//   http.request('something.json').subscribe(res => {
//     text = res.text();
//   });
//   connection.mockRespond(new Response({body: 'Something'}));
//   expect(text).toBe('Something');
// }); 
//# sourceMappingURL=http.service.spec.js.map