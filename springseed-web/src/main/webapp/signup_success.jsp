<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>User Registration</title>
</head>
<body>
	<h1>User Registration Successful</h1>
		<br>
		<table>
			<tr>
				<td><label>Username</label></td>
				<td>${ user.username }</td>
			</tr>
			<tr>
				<td><label>Email</label></td>
				<td>${ user.email }</td>
			</tr>
		</table>
	<br>
	The account has been successfully created. A verification link has been sent to the email provided. <a href="./login">Login</a>
	</a>
</body>
</html>