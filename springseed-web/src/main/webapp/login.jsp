<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title>HyperEmail|Login</title>
	<meta charset="UTF-8">
	<base href="/">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/assets/css/materialize.min.css" media="screen,projection">
	<link rel="stylesheet" href="/assets/css/style.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<title>User Registration</title>
</head>

<body id="main">
	<div class="bg-img"></div>
	<main>

		<div class="navbar-fixed"> 
			<nav class="white">
				<div class="nav-wrapper container">
					<a class="brand-logo">HyperEmail</a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">
						<li><a href="./login">Login<i class="material-icons left">home</i></a></li>
						<li><a href="./signup">Signup<i class="material-icons left">dashboard</i></a></li>
					</ul>
					<ul class="side-nav" id="mobile-demo">
						<li><a href="./login">Login<i class="material-icons left">home</i></a></li>
						<li><a href="./signup">Signup<i class="material-icons left">dashboard</i></a></li>
					</ul>
				</div>
			</nav>
		</div>

		<div class="container">
			<div class="row component-content">
				<form action="${loginUrl}" method="post" class="col l4 offset-l8 s10 offset-s2 login z-depth-2">
					<p>${SPRING_SECURITY_LAST_EXCEPTION.message}</p>
					<c:if test="${param.error != null}">
						<p>${param.error}</p>
					</c:if>
					<c:if test="${param.logout != null}">
						<p>You have been logged out.</p>
					</c:if>
					<div class="row">
						<h4 class="col s8 offset-s2 l6">Login</h4>
						<a href="./signup" class="btn-flat hoverable waves-effect waves-light col offset-s2 s6 l6" type="submit" name="action">Sign Up
							<i class="material-icons right">send</i>
						</a>
					</div>
					<div class="row">
						<div class="input-field offset-s2 col s6 l12">
							<i class="material-icons prefix">account_circle</i>
							<input id="icon_prefix" type="text" name="username" class="validate" required>
							<label for="icon_prefix">Username</label>
						</div>
					</div>
					<div class="row">
						<div class="input-field offset-s2 col s6 l12">
							<i class="material-icons prefix">lock</i>
							<input id="icon_telephone" type="password" name="password" class="validate" required>
							<label for="icon_telephone">Password</label>
						</div>
					</div>
					<div class="row">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						<button class="btn waves-effect waves-light offset-s3 s5 col l11 offset-l1" type="submit" name="action">Submit
							<i class="material-icons right">send</i>
						</button>
					</div>
				</form>
			</div>
		</div>
	</main>
	<footer class="page-footer">
		<div class="container footer-container">
			<div class="row">
				 <div class="col l8 offset-l2  s12">
	        <img class="img-responsive" src="assets/img/world-map.png">
	      </div>
			</div>
		</div>
		<div class="footer-copyright hm-footer">
			<div class="container">
				© Copyright 2016 Belzabar Software. All rights reserved.
			</div>
		</div>
	</footer>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/materialize.min.js"></script>
<script>
	 $( document ).ready(function(){
	 		$('.button-collapse').sideNav({
			menuWidth: 300, // Default is 240
			edge: 'left', // Choose the horizontal origin
			closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
		});
	 })
</script>
</body>
</html>