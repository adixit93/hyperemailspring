<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title>HyperEmail|Signup</title>
	<meta charset="UTF-8">
	<base href="/">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/assets/css/materialize.min.css" media="screen,projection">
	<link rel="stylesheet" href="/assets/css/style.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<title>User Registration</title>
</head>
<body id="main">
	<div class="bg-img"></div>
	<main>
		<div class="navbar-fixed"> 
			<nav class="white">
				<div class="nav-wrapper container">
					<a class="brand-logo">HyperEmail</a>
					<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
					<ul class="right hide-on-med-and-down">
						<li><a href="./login">Login<i class="material-icons left">home</i></a></li>
						<li><a href="./signup">Signup<i class="material-icons left">dashboard</i></a></li>
					</ul>
					<ul class="side-nav" id="mobile-demo">
						<li><a href="./login">Login<i class="material-icons left">home</i></a></li>
						<li><a href="./signup">Signup<i class="material-icons left">dashboard</i></a></li>
					</ul>
				</div>
			</nav>
		</div>

	<div class="container">
		<div class="row component-content">

			<form :form modelAttribute="user" method="POST" enctype="utf8" action="./signup" class="col l8 offset-l4 signup z-depth-2" >
				<p>${SPRING_SECURITY_LAST_EXCEPTION.message}</p>
				<c:if test="${error != null}">
				<p>${error}</p>
			</c:if>
			<div class="row">
				<h4 class="col l6 s11 offset-s1">Signup</h4>
				<a  href="./login" class="btn-flat hoverable waves-effect waves-light col l6 s8 offset-s1" name="action">Already Have Account ? Login
					<i class="material-icons right">send</i>
				</a>
			</div>
			<div class="row">
				<div class="input-field col l4 s8 offset-s1">
					<i class="material-icons prefix">perm_identity</i>
					<input id="icon_fname" name="firstName" type="text" class="validate" required>
					<label for="icon_fname">First Name</label>
				</div>
				<div class="input-field col l4 s8 offset-s1">
					<i class="material-icons prefix">perm_identity</i>
					<input id="icon_mname" name="middleName" type="text" class="validate">
					<label for="icon_mname">Middle Name</label>
				</div>
				<div class="input-field col l4 s8 offset-s1">
					<i class="material-icons prefix">perm_identity</i>
					<input id="icon_lname" type="text" name="lastName" class="validate" required>
					<label for="icon_lname">Last Name</label>
				</div>

				<div class="input-field col l12 s8 offset-s1">
					<i class="material-icons prefix">email</i>
					<input id="icon_email" type="email" name="email" class="validate" required>
					<label for="icon_email">Email</label>
				</div>

				<div class="input-field col l12 s8 offset-s1">
					<i class="material-icons prefix">account_circle</i>
					<input id="icon_prefix" type="text" name="username" class="validate" required>
					<label for="icon_prefix">Username</label>
				</div>
				<div class="input-field col l12 s8 offset-s1">
					<i class="material-icons prefix">lock</i>
					<input id="icon_password" type="password" name="password" class="validate" required>
					<label for="icon_password">Password</label>
				</div>
				<div class="row">
					<button class="btn waves-effect waves-light col l3 offset-l9 s8 offset-s1" type="submit"  name="action">Submit
						<i class="material-icons right">send</i>
					</button>
				</div>
			</form>
		</div>

		</div>
	</main>

	<footer class="page-footer">
		<div class="container footer-container">
			<div class="row">
				 <div class="col l8 offset-l2  s12">
	        <img class="img-responsive" src="assets/img/world-map.png">
	      </div>
			</div>
		</div>
		<div class="footer-copyright hm-footer">
			<div class="container">
				© Copyright 2016 Belzabar Software. All rights reserved.
			</div>
		</div>
	</footer>

	<script src="/assets/js/jquery.min.js"></script>
	<script src="/assets/js/materialize.min.js"></script>
	<script>
	 $( document ).ready(function(){
	 		$('.button-collapse').sideNav({
			menuWidth: 300, // Default is 240
			edge: 'left', // Choose the horizontal origin
			closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
		});
	 })
</script>
</body>
</html>