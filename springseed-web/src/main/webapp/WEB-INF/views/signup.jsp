<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>User Registration</title>
</head>
<body>
	<h1>User Registration</h1>
	<form:form modelAttribute="user" method="POST" enctype="utf8" action="./signup">
		<p>${SPRING_SECURITY_LAST_EXCEPTION.message}</p>
		<c:if test="${error != null}">
		<p>${error}</p>
		</c:if>
		<table>
			<tr>
				<td><label>Username *</label></td>
				<td><form:input path="username" value="" /></td>
				<form:errors path="username" element="div" />
			</tr>
			<tr>
				<td><label>Email *</label></td>
				<td><form:input path="email" value="" /></td>
				<form:errors path="email" element="div" />
			</tr>
			<tr>
				<td><label>Password *</label></td>
				<td><form:password path="password" value="" /></td>
				<form:errors path="password" element="div" />
			</tr>
		</table>
		<br /><br />
		<button type="submit">Sign up</button> | <a href="./login">Login</a> 
	</form:form>
</body>
</html>