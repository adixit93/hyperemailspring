<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login Page</title>
</head>
<body>
	<h1>Login Page</h1>

	<c:url value="/login" var="loginUrl" />
	<form action="${loginUrl}" method="post">
		<p>${SPRING_SECURITY_LAST_EXCEPTION.message}</p>
		<c:if test="${param.error != null}">
		<p>${param.error}</p>
		</c:if>
		<c:if test="${param.logout != null}">
		<p>You have been logged out.</p>
		</c:if>
		
		<table>
			<tr>
				<td><label for="username">Username</label></td>
				<td><input type="text" id="username" name="username" /></td>
			</tr>
			<tr>
				<td><label for="password">Password</label></td>
				<td><input type="password" id="password" name="password" /></td>
			</tr>
		</table>
		<br /><br />
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<button type="submit" class="btn">Log in</button> | 
		<a href="./forgotpassword">Forgot Password</a> | 
		<a href="./signup">Sign Up</a>
	</form>
</body>
</html>