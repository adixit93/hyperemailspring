<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reset Password</title>
</head>
<body>
	<h1>Reset Password</h1>
	<c:if test="${successMsg != null}">
		${successMsg}
	</c:if>
	<c:if test="${errorMsg != null}">
		${errorMsg}
	</c:if>
	<c:if test="${errorMsg == null && successMsg == null}">
		<form action="../" method="post">
		<input type="hidden" name="token" value="${user.forgottenPasswordToken}" />
		<table>
			<tr>
				<td>
					<label for="username">Username</label>
				</td>
				<td>
					<input type="text" id="username" name="username" value="${user.username}" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td><label for="email">Email</label></td>
				<td><input type="text" id="email" name="email" value="${user.email}" readonly="readonly" /></td>
			</tr>
			<tr>
				<td><label for="password">Password</label></td>
				<td><input type="password" id="password" name="password" /></td>
			</tr>
			<tr>
				<td>
					<label for="cpassword">Confirm Password</label>
				</td>
				<td>
					<input type="password" id="cpassword" name="confirm_password" />	    
				</td>
			</tr>
		</table>
		<br />
		<br />
		<button type="submit">Reset Password</button> | <a href="../../login">Login</a>
	</form></c:if>
</body>
</html>