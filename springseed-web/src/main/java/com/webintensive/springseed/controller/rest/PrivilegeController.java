package com.webintensive.springseed.controller.rest;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.webintensive.springseed.exceptionhandling.customexceptions.PrivilegeAssignmentFailedException;
import com.webintensive.springseed.pojomodel.UserPrivilege;
import com.webintensive.springseed.service.UserPrivilegeService;

@RestController
@RequestMapping(value = "/testapi/privilege")
public class PrivilegeController {

	private static final String DEFAULT_REST_RETURN_TYPE = "application/json";
	private static final Logger logger = LoggerFactory.getLogger(PrivilegeController.class);

	@Autowired
	private UserPrivilegeService userPrivilegeService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = DEFAULT_REST_RETURN_TYPE)
	public UserPrivilege getPrivilegeById(@PathVariable int id) {
		return userPrivilegeService.getPrivilegeById(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = DEFAULT_REST_RETURN_TYPE)
	public UserPrivilege createPrivilege(@RequestParam("name") String name,
			@RequestParam("description") String description) {
		return userPrivilegeService.persistPrivilege(name, description);
	}

	@RequestMapping(value = "/assign/{privId}/user/{userId}", method = RequestMethod.POST, produces = DEFAULT_REST_RETURN_TYPE)
	public void assignUserPrivilege(@PathVariable("userId") int userId, @PathVariable("privId") short privId)
			throws PrivilegeAssignmentFailedException {
		int assigned = userPrivilegeService.assignUserPrivilege(userId, privId);
		if (assigned <= 0) {
			throw new PrivilegeAssignmentFailedException(new StringBuilder("Failed to assign privilege ")
					.append(privId).append(" to user ").append(userId).toString());
		}
	}
	
	@RequestMapping(value = "/assign/{privId}/group/{groupId}", method = RequestMethod.POST, produces = DEFAULT_REST_RETURN_TYPE)
	public void assignGroupPrivilege(@PathVariable("groupId") int groupId, @PathVariable("privId") short privId)
			throws PrivilegeAssignmentFailedException {
		int assigned = userPrivilegeService.assignGroupPrivilege(groupId, privId);
		if (assigned <= 0) {
			throw new PrivilegeAssignmentFailedException(new StringBuilder("Failed to assign privilege ")
					.append(privId).append(" to group ").append(groupId).toString());
		}
	}

	@ExceptionHandler(PrivilegeAssignmentFailedException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String privilegeAssigmentFailureHandler(HttpServletRequest req, Exception exception) {
		logger.error(exception.getMessage(), exception);
		return exception.getMessage();
	}
}
