package com.webintensive.springseed.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidForgotPasswordTokenException;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.UserAccountService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class MVCController {

	private static final Logger logger = LoggerFactory.getLogger(MVCController.class);

	@Autowired
	private UserAccountService userAccountService;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return "home";
		}

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		return "home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		logger.info("Redirecting to the login page.");
		return "login";
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard() {
		logger.info("Redirecting to the dashboard.");
		return "dashboard";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signUpGet(Model model) {
		logger.info("Redirecting to the signup page.");
		model.addAttribute("user", new UserAccount());
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signUpPost(@ModelAttribute("user") User user, Map<String, Object> model,
			HttpServletRequest request) {
		String response = null;
		UserAccount createdAccount = null;
		try {
			logger.info("/signup");
			createdAccount = userAccountService.createUserAccount(null, user.getEmail(), user.getUsername(),
					user.getPassword(), request.getRemoteAddr(), user.getFirstName(), user.getMiddleName(), user.getLastName());
			if (null != createdAccount) {
				model.put("user", createdAccount);
				response = "signup_success";
			}
		} catch (SpringSeedException e) {
			model.put("error", e.getMessage());
			response = "signup";
		} catch (Exception e) {
            logger.error("Unwanted exception occurred",e);
			model.put("error", "Unwanted exception occurred.");
		}

		return response;
	}

	@RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
	public String forgotPassword() {
		logger.info("Redirecting to the forgot password page.");
		return "forgot_password";
	}

	@RequestMapping(value = "/forgotpassword/response", method = RequestMethod.POST)
	public String generateForgotPasswordToken(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "email", required = false) String email, HttpServletRequest request) {
		logger.info("Generating the forgot password token for username- {} or email {}.", username, email);
		try {
			userAccountService.generateForgotPasswordLink(username, email);
			logger.info("Successfully generated a password token for username {} or email {}.", username, email);
			request.setAttribute("successMsg",
					"A reset password link has been generated and mailed to you. Please follow the link to reset you password.");
		} catch (SpringSeedException e) {
			logger.error(new StringBuilder("Failed to generate password token for the user ").append(username)
					.append(" - ").append(email).toString(), e);
			request.setAttribute("errorMsg", "Failed to generate password token - " + e.getMessage());
		}
		return "forgot_password";
	}

	@RequestMapping(value = "/resetpassword/token/{token}", method = RequestMethod.GET)
	public String resetPassworByToken(@PathVariable String token, HttpServletRequest request, Model model) {
		UserAccount userAccount = null;
		try {
			userAccount = userAccountService.getUserAccountByForgotPasswordToken(token);
			model.addAttribute("user", userAccount);
		} catch (InvalidForgotPasswordTokenException e) {
			request.setAttribute("errorMsg", "Invalid forgot password token.");
			logger.error("The forgot password token is invalid - {}", token, e);
		}
		return "reset_password";
	}

	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public String resetPassword(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "token", required = true) String token,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "confirm_password", required = true) String confirmPassword,
			HttpServletRequest request, Model model) {
		if ((null != password) && (null != confirmPassword) && (password.equals(confirmPassword))) {
			try {
				userAccountService.updatePassword(username, password, token);

				model.addAttribute("successMsg", "Password has been successfully updated. Please login.");
			} catch (InvalidForgotPasswordTokenException e) {
				request.setAttribute("user", prepareUserAccount(username, email, token));
				request.setAttribute("errorMsg", e.getMessage());
				logger.error("The forgot password token is invalid - {}", token, e);
			}
		} else {
			request.setAttribute("errorMsg", "The password and confirm password, do not match.");
			request.setAttribute("user", prepareUserAccount(username, email, token));
		}

		return "reset_password";
	}

	private UserAccount prepareUserAccount(String username, String email, String token) {
		UserAccount user = new UserAccount();
		user.setUsername(username);
		user.setEmail(email);
		user.setForgottenPasswordToken(token);
		return user;
	}

}
