package com.webintensive.springseed.controller.rest;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webintensive.springseed.pojomodel.UserGroup;
import com.webintensive.springseed.service.UserGroupService;


@RestController
@RequestMapping(value = "/testapi/group")
public class GroupController {


	private static final String DEFAULT_REST_RETURN_TYPE = "application/json";

	@Autowired
	private UserGroupService userGroupService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = DEFAULT_REST_RETURN_TYPE)
	public UserGroup getUserGroupById(@PathVariable int id) throws Exception {
		return userGroupService.getUserGroupById(id);

	}

	@RequestMapping(value = "/", method = RequestMethod.POST, produces = DEFAULT_REST_RETURN_TYPE)
	public UserGroup createUserGroup(@RequestParam("name") String name, @RequestParam("description") String description) throws Exception {
		return userGroupService.persistUserGroup(name, description);
	}

	@RequestMapping(value = "/", method = RequestMethod.GET, produces = DEFAULT_REST_RETURN_TYPE)
	public List<UserGroup> getAllGroups() throws Exception {
		return userGroupService.getAllUserGroups();
	}
}
