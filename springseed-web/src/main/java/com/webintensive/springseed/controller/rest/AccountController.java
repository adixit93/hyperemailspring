package com.webintensive.springseed.controller.rest;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.ActivatingActiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InactiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidActivationTokenException;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.UserAccountService;

@RestController
@RequestMapping(value = "/testapi/account")
public class AccountController {

	private static final String DEFAULT_REST_RETURN_TYPE = "application/json";

	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
	
	@Autowired
	private UserAccountService userAccountService;

	@RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = DEFAULT_REST_RETURN_TYPE)
	public UserAccount getUserGroupById(@PathVariable String username) {
		return userAccountService.loadUserByUsername(username);
	}

	@RequestMapping(value = "/activation/{token}", method = RequestMethod.GET)
	public void activateToken(@PathVariable("token") String token) throws InvalidActivationTokenException,
			InactiveUserException, ActivatingActiveUserException {
		userAccountService.activateUser(token);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST, produces = DEFAULT_REST_RETURN_TYPE)
	public UserAccount createUserAccount(@RequestParam("groupId") Integer groupId, @RequestParam("email") String email,
			@RequestParam("username") String username,@RequestParam("firstName") String firstName,@RequestParam("middleName") String middleName,@RequestParam("lastName") String lastName, @RequestParam("password") String password, HttpServletRequest request) throws SpringSeedException {
		UserAccount account = null;
		try {
			account = userAccountService.createUserAccount(groupId, email, username, password, request.getRemoteAddr(),firstName, middleName, lastName);
		} catch (Exception e) {
			logger.error("Faield to create user.", e);
			throw new SpringSeedException("Failed to create user account.");
		}
		
		return account;
	}
}
