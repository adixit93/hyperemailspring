package com.webintensive.springseed.controller.rest;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.webintensive.springseed.exceptionhandling.customexceptions.HyperEmailException;

import com.webintensive.springseed.hyperemail.models.Contact;
import com.webintensive.springseed.hyperemail.models.Delegation;

import com.webintensive.springseed.hyperemail.models.ErrorInfo;
import com.webintensive.springseed.hyperemail.models.Filter;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.RecentlyContacted;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;
import com.webintensive.springseed.service.UserEmailAccountService;

@RestController
@RequestMapping(value = "users")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserEmailAccountService userEmailAccountService;

	// Get Spring security logged in username

	@RequestMapping(value = "/userid", method = RequestMethod.GET)
	public User printWelcome(ModelMap model, Principal principal) throws HyperEmailException {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		return userEmailAccountService.getUserInfoByUsername(username);
	}

	// Delegation

	@RequestMapping(value = "/{userId}/delegation", method = RequestMethod.GET)
	public List<Delegation> getDelegation(@PathVariable int userId) throws HyperEmailException {
		return userEmailAccountService.getDelegation(userId);
	}

	@RequestMapping(value = "/{userId}/delegation", method = RequestMethod.POST)
	public Delegation addDelegation(@RequestBody Delegation delegation) throws HyperEmailException {
		return userEmailAccountService.addDelegation(delegation);
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}", method = RequestMethod.DELETE)
	public Integer deleteDelegation(@PathVariable("delegationId") Integer delegationId) throws HyperEmailException {
		return userEmailAccountService.deleteDelegation(delegationId);
	}

	// Who may help me?

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<User> getUserList() throws HyperEmailException {
		return userEmailAccountService.userList();
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/delegate", method = RequestMethod.GET)
	public List<Delegation> getDelegateUserList(@PathVariable("delegationId") int delegationId,
			@PathVariable("userId") int userId) throws HyperEmailException {
		return userEmailAccountService.getDelegateUserList(delegationId, userId);
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/delegate", method = RequestMethod.POST)
	public Delegation addDelegate(@RequestBody Delegation delegation) throws HyperEmailException {
		return userEmailAccountService.createDelegate(delegation);
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/delegate/{delegateId}", method = RequestMethod.DELETE)
	public int deleteDelegate(@PathVariable int delegateId) throws HyperEmailException {
		return userEmailAccountService.deleteDelegate(delegateId);

	}

	// Connect to my account

	@RequestMapping(value = "/{userId}/accounts", method = RequestMethod.GET)

	public List<UserEmailAccount> getUserAccounts(@PathVariable int userId) throws HyperEmailException {
		return userEmailAccountService.getAllAccounts(userId);
	}

	@RequestMapping(value = "/{userId}/accounts", method = RequestMethod.POST)
	public UserEmailAccount addUserAccount(@RequestBody UserEmailAccount userEmailAccount) throws HyperEmailException {
		return userEmailAccountService.addUserEmailAccount(userEmailAccount);
	}

	@RequestMapping(value = "/{userId}/accounts/{accountId}", method = RequestMethod.DELETE)
	public int deleteUserAccount(@PathVariable int accountId) throws HyperEmailException {
		return userEmailAccountService.deleteUserEmailAccount(accountId);
	}

	@RequestMapping(value = "/{userId}/accounts/{accountId}", method = RequestMethod.PUT)
	public UserEmailAccount editUserAccount(@RequestBody UserEmailAccount userEmailAccount) throws HyperEmailException {
		return userEmailAccountService.editUserEmailAccount(userEmailAccount);
	}

	// Which folder may they access?

	@RequestMapping(value = "/{userId}/accounts/{accountId}/folders", method = RequestMethod.GET)
	public List<Folder> getFolderList(@PathVariable int accountId) throws HyperEmailException {
		return userEmailAccountService.getFolder(accountId);
	}

	@RequestMapping(value = "/{userId}/accounts/{accountId}/delegation/{delegationId}/folderAccesses", method = RequestMethod.GET)

	public List<Folder> getAccessFolderList(@PathVariable("delegationId") int delegationId,
			@PathVariable("accountId") int accountId, @PathVariable("userId") int userId) throws HyperEmailException {
		return userEmailAccountService.getAccessFolder(delegationId, accountId);
	}

	@RequestMapping(value = "/{userId}/accounts/{accountId}/delegation/{delegationId}/folderAccesses", method = RequestMethod.POST)
	public Folder setFolderAccess(@RequestBody Folder folder, @PathVariable("delegationId") int delegationId,
			@PathVariable("userId") int userId) throws HyperEmailException {
		// LOG.info(folder.toString());
		return userEmailAccountService.createFolder(userId, folder, delegationId);
	}

	@RequestMapping(value = "/{userId}/accounts/{accountId}/delegation/{delegationId}/folders/{folderId}/accesses", method = RequestMethod.POST)
	public void setPermission(@RequestBody Delegation delegation) throws HyperEmailException {
		userEmailAccountService.createFolderPermission(delegation);
	}

	// Which contacts are private?

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/accesses", method = RequestMethod.POST)
	public Filter addPrivateContacts(@RequestBody Filter filter,

			@RequestParam(value = "type", required = false) String type) throws HyperEmailException {
		return userEmailAccountService.addPrivateContact(filter, type);
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/accesses/{accessId}", method = RequestMethod.DELETE)
	public int removePrivateContacts(@PathVariable("accessId") int accessId) throws HyperEmailException {
		return userEmailAccountService.deletePrivateDetails(accessId);

	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/accesses", method = RequestMethod.GET)
	public List<Filter> getFilters(@PathVariable("delegationId") int delegationId) throws HyperEmailException {

		return userEmailAccountService.getFilter(delegationId);
	}

	@RequestMapping(value = "/{userId}/delegation/{delegationId}/filters", method = RequestMethod.GET)
	public List<Filter> getFiltersByType(@PathVariable("delegationId") int delegationId,
			@RequestParam(value = "type", required = false) String type) throws HyperEmailException {

		return userEmailAccountService.getFiltersByType(type, delegationId);
	}

	/*
	 * 
	 * James api
	 * 
	 */

	// get folder list
	@RequestMapping(value = "/accounts/{email}/folders", method = RequestMethod.GET)
	public List<Folder> getFolderListByEmail(@PathVariable("email") String email) throws HyperEmailException {

		return userEmailAccountService.getFolderByEmail(email);

	}

	// get connect to my accounts info based on username
	@RequestMapping(value = "/{username}/account", method = RequestMethod.GET)
	public List<UserEmailAccount> getEmailAccounts(@PathVariable("username") String username)
			throws HyperEmailException {
		return userEmailAccountService.getUserEmailAccounts(username);
	}

	// get folder list delegated to a user
	@RequestMapping(value = "/{username}/folder", method = RequestMethod.GET)
	public List<Delegation> getFolderListByDelegateUserName(@PathVariable("username") String username)
			throws HyperEmailException {
		return userEmailAccountService.getFolderListByDelegateUserName(username);
	}

	// get account info
	@RequestMapping(value = "/{userId}/accountdetails", method = RequestMethod.GET)
	public User getUserDetailsByUserId(@PathVariable("userId") int userId) throws HyperEmailException {

		return userEmailAccountService.getUserDetails(userId);
	}

	// get filters by user's username and delegate's username
	@RequestMapping(value = "/{userName}/{delegateName}/filters", method = RequestMethod.GET)
	public List<Filter> getFiltersByEmail(@PathVariable("userName") String userName,
			@PathVariable("delegateName") String delegateName) throws HyperEmailException {

		return userEmailAccountService.getFiltersByEmail(userName, delegateName);
	}

	// get user details bassed on username
	@RequestMapping(value = "/{username}", method = RequestMethod.GET)
	public User getUserDetails(@PathVariable("username") String username) throws HyperEmailException {
		return userEmailAccountService.getUserDetails(username);
	}

	/*
	 * SOUMYA
	 */
	@RequestMapping(value = "/recent-contacts", method = RequestMethod.POST)
	public void contactList(@RequestBody RecentlyContacted recentlyContacted) throws HyperEmailException {
		userEmailAccountService.addContacts(recentlyContacted);
	}

	@RequestMapping(value = "/{userId}/contacts", method = RequestMethod.GET)
	public List<Contact> getContactList(@PathVariable("userId") int userId) throws HyperEmailException {
		return userEmailAccountService.getContactList(userId);
	}

	// Exception handling
	@ExceptionHandler(HyperEmailException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorInfo hyperEmailExceptionHandler(HttpServletRequest req, HyperEmailException exception) {
		LOG.error(exception.getMessage());
		exception.printStackTrace();
		return new ErrorInfo(req.getRequestURI(), exception);

	}
}
