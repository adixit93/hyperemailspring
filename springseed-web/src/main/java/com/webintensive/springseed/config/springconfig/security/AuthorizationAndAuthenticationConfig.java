package com.webintensive.springseed.config.springconfig.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value={APISecurityConfig.class, SecurityConfig.class})
public class AuthorizationAndAuthenticationConfig {

}
