package com.webintensive.springseed.config.springconfig.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.webintensive.springseed.pojomodel.ExtendedUser;
import com.webintensive.springseed.service.UserAccountService;
import com.webintensive.springseed.service.impl.UserAccountServiceImpl;

@Component
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);
	
	@Value("${login.landing.url}")
	private String loginLandingPage;
	@Autowired
	private UserAccountService userAccountService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		setDefaultTargetUrl(loginLandingPage);
		super.onAuthenticationSuccess(request, response, authentication);
		String username = ((ExtendedUser)authentication.getPrincipal()).getUsername();
		// update last login
		try {
			userAccountService.updateLastLogin(username);
			if (logger.isDebugEnabled()) {
				logger.debug("Last login timestamp updated for user {}", username);
			}
		} catch (Exception e) {
			logger.error("Failed to update the last login timestamp for user {}", username, e);
		}
	}
}
