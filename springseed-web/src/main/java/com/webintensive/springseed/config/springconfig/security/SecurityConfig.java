package com.webintensive.springseed.config.springconfig.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.dao.ReflectionSaltSource;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.webintensive.springseed.config.springconfig.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(2)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	public SecurityConfig() {
	}

	@Autowired
	Environment environment;
	
	@Value("${login.url}")
	private String loginPage;
	
	@Autowired
	private ShaPasswordEncoder shaPasswordEncoder;
	@Autowired
	private CustomTokenBasedRememberMeService tokenBasedRememberMeService;
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	// @formatter:off
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//		authProvider.setPasswordEncoder(shaPasswordEncoder);
		authProvider.setUserDetailsService(userDetailsServiceImpl);
//		authProvider.setSaltSource(saltSource());
		auth.authenticationProvider(authProvider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf()
			.disable()
        .authorizeRequests()
        	.antMatchers("/WEB-INF/views/**").permitAll()
//        	.antMatchers("/assets/**").permitAll()
//        	.antMatchers("/systemjs.config.js").permitAll()
//	        .antMatchers("/resources/**").permitAll()
	        .antMatchers("/forgotpassword/**").permitAll()
	        .antMatchers("/resetpassword/**").permitAll()
	        .antMatchers("/signup/**").permitAll()
            .antMatchers("/dashboard/**").permitAll()
            .antMatchers("/users/**").permitAll()
           
            // TODO: remove this once testing is complete
            .antMatchers("/testapi/**").permitAll()
            .anyRequest().authenticated()
            .and()
       .formLogin()
        .successHandler(authenticationSuccessHandler)
	        .failureHandler(authenticationFailureHandler)
  	.loginPage(loginPage)
           .permitAll()
           .and()
        .rememberMe()
        	.rememberMeServices(tokenBasedRememberMeService);
	}
	
	@Override
	  public void configure(WebSecurity web) throws Exception {
	    web
	      .ignoring()
	      	 .antMatchers("/WEB-INF/views/**")
	         .antMatchers("/assets/**")
	         .antMatchers("/dashboard/**")
	         .antMatchers("/delegation/**");
//	         .antMatchers("/typings/**")
//	         .antMatchers("/app/**");
	  }
	// @formatter:on

	@Bean
	public ReflectionSaltSource saltSource() {
		ReflectionSaltSource rss = new ReflectionSaltSource();
		rss.setUserPropertyToUse("salt");
		return rss;
	}
}