package com.webintensive.springseed.config.springconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

//import com.webintensive.springseed.config.springconfig.util.SpringConfigUtil;
//import com.webintensive.springseed.config.springconfig.util.SpringFeaturesEnum;

/**
 * @author gauravg
 */
@EnableWebMvc
@Configuration
@ComponentScan(basePackages="com.webintensive.springseed.controller")
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	
	private static LocaleChangeInterceptor localeChangeInterceptor;
	
	{
		localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("locale");
	}
	
	/*-
	 * Configuration alternative for the static resource mapping.
	 * 
	 * <resources mapping="/resources/**" location="/resources/" /> 
	 *  
	 *  (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
//	    registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	    registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
	    registry.addResourceHandler("/node_modules/**").addResourceLocations("/node_modules/");
	    registry.addResourceHandler("/typings/**").addResourceLocations("/typings/");
	    registry.addResourceHandler("/app/**").addResourceLocations("/app/");
	    registry.addResourceHandler("systemjs.config.js").addResourceLocations("systemjs.config.js");
	}
	
	/*-
	 * All the MVC interceptors must go here. Configuration alternative for the following-
	 * <mvc:interceptors>    
	 *	    <bean class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor"
	 *	          p:paramName="locale" />
	 *	</mvc:interceptors>
	 * 
	 * (non-Javadoc)
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry)
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// add the local change interceptor only if internationalization is required
//		if (SpringConfigUtil.getInstance().isFeatureEnabled(environment, SpringFeaturesEnum.INTERNATIONALIZATION)) {
			registry.addInterceptor(localeChangeInterceptor);			
//		}
	}
	
	/*-
	 * Configuration alternative for the following.
	 * 
	 *  <beans:bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
	 *		<beans:property name="prefix" value="/WEB-INF/views/" />
	 *		<beans:property name="suffix" value=".jsp" />
	 *	</beans:bean>
	 * 
	 * @return
	 */
	@Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

}