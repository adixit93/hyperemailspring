//package com.webintensive.springseed.config.springconfig;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletException;
//
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.filter.DelegatingFilterProxy;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//@EnableWebMvc
//@Configuration
//@ComponentScan(basePackages = "com.webintensive.springseed.controller")
//public class DispatcherServlet extends AbstractAnnotationConfigDispatcherServletInitializer {
//
//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		servletContext
//				.addFilter("cors",
//						new DelegatingFilterProxy("com.webintensive.springseed.config.springconfig.CorsCommon"))
//				.addMappingForUrlPatterns(null, false, "/*");
//
//		super.onStartup(servletContext);
//	}
//
//	@Override
//	protected Class<?>[] getRootConfigClasses() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected Class<?>[] getServletConfigClasses() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	protected String[] getServletMappings() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	// Various other required methods...
//
//}
