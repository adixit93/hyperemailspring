package com.webintensive.springseed.config.springconfig.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.UserAccountService;

@Component
public class AuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationFailureHandler.class);

	@Value("${maximum.failed.login.attempts}")
	private int maxFailedLoginAttempts;
	@Value("${login.error.landing.url}")
	private String loginFailedLandingPage;
	
	@Autowired
	private UserAccountService userAccountService;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		setDefaultFailureUrl(loginFailedLandingPage);
		super.onAuthenticationFailure(request, response, exception);
		UserAccount user = null;
		final String username = request.getParameter("username");
		
		// update login failure attempts
		try {
			user = userAccountService.loadUserByUsername(username);
			if (null != user && user.isUserEnabled() && !user.isUserBlocked()) {
				if (user.getFailLoginAttempts() >= maxFailedLoginAttempts) {
					logger.info(
							"Max number of failed login attempts exceeded for user - {}. Attempting to suspend user indefinitely.",
							username);
					userAccountService.suspendUserAccount(username, user.getEmail());
					logger.info("User - {}, was successfully suspended.", username);
				}
				
				userAccountService.updateFailedLoginCount(username, request.getRemoteAddr());
				if (logger.isDebugEnabled()) {
					logger.debug("Authentication attempt failed for username {}. Incrementing the failure count.", username);
				}				
			}
		} catch (Exception e) {
			logger.error("Failed to update the last login timestamp for user {}", username, e);
		}
	}
}
