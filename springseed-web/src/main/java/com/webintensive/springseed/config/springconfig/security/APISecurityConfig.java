package com.webintensive.springseed.config.springconfig.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.RememberMeAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.webintensive.springseed.config.springconfig.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan(basePackages = { "com.webintensive.springseed" })
@Order(1)
public class APISecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String tokenKey = "Random token";

	public APISecurityConfig() {
	}

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@Autowired
	private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	// @formatter:off
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//		auth.authenticationProvider(rememberMeAuthenticationProvider());
	}
	
	@Override protected void configure(HttpSecurity http) throws Exception {
        http
        	.antMatcher("/api/**")
        	.csrf()
        		.disable()
            .authorizeRequests().anyRequest().authenticated().and()
        	.addFilterBefore(rememberMeAuthenticationFilter(), BasicAuthenticationFilter.class )
        		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        		.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);
    }
	
	@Override
	  public void configure(WebSecurity web) throws Exception {
	    web
	      .ignoring()
//	      	.antMatchers("/assets/**")
//	      	.antMatchers("/systemjs.config.js")
//	      	.antMatchers("/node_modules/**")
//	      	.antMatchers("/typings/**");
	        .antMatchers("/resources/**");
	  }
	// @formatter:on

	@Bean
	public RememberMeAuthenticationFilter rememberMeAuthenticationFilter() throws Exception {
		return new RememberMeAuthenticationFilter(authenticationManager(), tokenBasedRememberMeService());
	}

	@Bean
	public CustomTokenBasedRememberMeService tokenBasedRememberMeService() {
		CustomTokenBasedRememberMeService service = new CustomTokenBasedRememberMeService(tokenKey,
				userDetailsServiceImpl);
		service.setAlwaysRemember(true);
		service.setCookieName("authToken");
		return service;
	}

	@Bean
	RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
		return new RememberMeAuthenticationProvider(tokenKey);
	}

}