package com.webintensive.springseed.config.springconfig;

import javax.servlet.ServletContext;

import javax.servlet.ServletRegistration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.webintensive.springseed.config.springconfig.security.AuthorizationAndAuthenticationConfig;
import com.webintensive.springseed.configurations.DispatcherConfig;

/**
 * @author gauravg
 */
@Configuration
@EnableAsync
public class AppInitializer extends AbstractSecurityWebApplicationInitializer {
	public AppInitializer() {
		super(new Class[] { AuthorizationAndAuthenticationConfig.class });
	}

	@Override
	protected void beforeSpringSecurityFilterChain(ServletContext container) {
		// set all the init parameters here
		container.setInitParameter("javax.servlet.jsp.jstl.fmt.localizationContext", "messages");

		/*-
		<servlet>
			<servlet-name>appServlet</servlet-name>
			<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
			<init-param>
				<param-name>contextConfigLocation</param-name>
				<param-value>/WEB-INF/spring/appServlet/servlet-context.xml</param-value>
			</init-param>
			<load-on-startup>1</load-on-startup>
		</servlet>
		<servlet-mapping>
			<servlet-name>appServlet</servlet-name>
			<url-pattern>/</url-pattern>
		</servlet-mapping>
		 */
		// Create the dispatcher servlet's Spring application context

		AnnotationConfigWebApplicationContext dispatcherContext = getAnnotatedContext();
		dispatcherContext.register(DispatcherConfig.class);

		// Register and map the dispatcher servlet
		ServletRegistration.Dynamic dispatcher = container.addServlet("dispatcher",
				new DispatcherServlet(dispatcherContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");

	}
	
	/*-
	 * The following code contains the web.xml code and its java alternative.
	 * 
	 * (non-Javadoc) 
	 * @see org.springframework.web.WebApplicationInitializer#onStartup(javax.servlet
	 * .ServletContext)
	 */
	// @Override
	// public void onStartup(ServletContext container) throws ServletException {
	// /*-
	// <listener>
	// <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
	// </listener>
	// */
	// // Creates a root application context
	// AnnotationConfigWebApplicationContext rootContext = new
	// AnnotationConfigWebApplicationContext();
	// // Manages the life cycle of the root application context
	// container.addListener(new ContextLoaderListener(rootContext));
	//
	// // set all the init parameters here
	// container.setInitParameter("javax.servlet.jsp.jstl.fmt.localizationContext",
	// "messages");
	//*/
	//
	// FilterRegistration.Dynamic securityFilter =
	// container.addFilter("springSecurityFilterChain",
	// DelegatingFilterProxy.class);
	// securityFilter.addMappingForUrlPatterns(null, false, "/*");
	//
	//
	//
// /*-
	// <servlet>
	// <servlet-name>appServlet</servlet-name>
	// <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
	// <init-param>
	// <param-name>contextConfigLocation</param-name>
	// <param-value>/WEB-INF/spring/appServlet/servlet-context.xml</param-value>
	// </init-param>
	// <load-on-startup>1</load-on-startup>
	// </servlet>
	// <servlet-mapping>
	// <servlet-name>appServlet</servlet-name>
	// <url-pattern>/</url-pattern>
	// </servlet-mapping>
	// */
	// // Create the dispatcher servlet's Spring application context
	// AnnotationConfigWebApplicationContext dispatcherContext =
	// getAnnotatedContext();
	// dispatcherContext.register(DispatcherConfig.class);
	// dispatcherContext.register(MainAppConfig.class);
	//
	// // Register and map the dispatcher servlet
	// ServletRegistration.Dynamic dispatcher =
	// container.addServlet("dispatcher", new DispatcherServlet(
	// dispatcherContext));
	// dispatcher.setLoadOnStartup(1);
	// dispatcher.addMapping("/");
	//
	// }

	private AnnotationConfigWebApplicationContext getAnnotatedContext() {
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
		context.setConfigLocation("com.webintensive.springseed");
		return context;
	}

	
}