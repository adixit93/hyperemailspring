package com.webintensive.springseed.config.springconfig.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

public class CustomTokenBasedRememberMeService extends TokenBasedRememberMeServices {

	public CustomTokenBasedRememberMeService(String key, UserDetailsService userDetailsService) {
		super(key, userDetailsService);
	}

	private final String HEADER_SECURITY_TOKEN = "authToken";

	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices#extractRememberMeCookie(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected String extractRememberMeCookie(HttpServletRequest request) {
		String token = request.getHeader(HEADER_SECURITY_TOKEN);
		if ((token == null) || (token.length() == 0)) {
			return null;
		}

		return token;
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices#onLoginSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
	 */
	@Override
	public void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication successfulAuthentication) {
		super.onLoginSuccess(request, response, successfulAuthentication);
		
	}
}