//package com.webintensive.springseed.dao.mapper;
//
//import org.apache.ibatis.annotations.Insert;
//import org.apache.ibatis.annotations.Options;
//
//import com.webintensive.springseed.hyperemail.models.UserSignup;
//
//public interface AuthenticationMapper {
//	@Insert("INSERT INTO public.auth(first_name,middle_name,last_name,email,username,password) VALUES(#{firstName},#{middleName},#{lastName},#{email},#{username},#{password})")
//	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
//	public void addUser(UserSignup details);
//}
