package com.webintensive.springseed.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.webintensive.springseed.pojomodel.UserPrivilege;

/**
 * MyBatis mapper for user privileges.
 * 
 * @author gauravg
 */
public interface UserPrivilegeMapper {
	@Select("SELECT * FROM user_privileges WHERE upriv_id = #{privId}")
	@ResultMap("userPrivMap")
	public UserPrivilege getPrivilegeById(@Param("privId") int privId);
	
	@Insert("INSERT INTO user_privileges (upriv_name, upriv_desc) VALUES (#{name}, #{description})")
	public int createPrivilege(UserPrivilege privilege);
	
	@Insert("INSERT INTO user_privileges (upriv_name, upriv_desc) VALUES (#{name}, #{description})")
	public int persistPrivilege(@Param("name") String name, @Param("description") String description);
	
	@Select("SELECT * FROM user_privileges")
	@ResultMap("userPrivMap")
	public List<UserPrivilege> getAllPrivileges();
	
	@Insert("INSERT INTO user_privilege_users (upriv_users_uacc_fk, upriv_users_upriv_fk) VALUES (#{userId}, #{privId})")
	public int assignUserPrivilege(@Param("userId") int userId, @Param("privId") short privId);
	
	@Insert("INSERT INTO user_privilege_groups (upriv_groups_ugrp_fk, upriv_groups_upriv_fk) VALUES (#{groupId}, #{privId})")
	public int assignGroupPrivilege(@Param("groupId") int groupId, @Param("privId") short privId);
	
	@Select("(select upa.upriv_users_upriv_fk as upriv_id, up.upriv_name, up.upriv_desc from user_accounts ua    left join user_privilege_users upa   on ua.uacc_id = upa.upriv_users_uacc_fk   left join user_privileges up   on upa.upriv_users_upriv_fk = up.upriv_id   where ua.uacc_id = #{userId})   union    (   select upg.upriv_groups_upriv_fk as upriv_id, up.upriv_name, up.upriv_desc from user_groups ug    left join user_privilege_groups upg   on ug.ugrp_id = upg.upriv_groups_ugrp_fk   right join user_privileges up   on upg.upriv_groups_upriv_fk = up.upriv_id   where ug.ugrp_id = #{groupId});")
	@ResultMap("userPrivMap")
	public List<UserPrivilege> getAllPrivilegesByUsername(@Param("userId") int userId, @Param("groupId") int groupId);
}