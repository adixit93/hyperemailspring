package com.webintensive.springseed.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.webintensive.springseed.pojomodel.UserGroup;

/**
 * MyBatis mapper for user groups.
 * 
 * @author gauravg
 */
public interface UserGroupMapper {
	@Select("SELECT * FROM user_groups WHERE ugrp_id = #{groupId}")
	@ResultMap("userGroupMap")
	public UserGroup getUserGroupById(@Param("groupId") int groupId) throws Exception;
	
	@Select("SELECT * FROM user_groups WHERE ugrp_name = #{groupName}")
	@ResultMap("userGroupMap")
	public UserGroup getUserGroupByGroupName(@Param("groupName") String groupName) throws Exception;
	
	 //Convert admin to foreign key regenerate the pojo and update this query
	@Insert("INSERT INTO user_groups(ugrp_name, ugrp_desc, ugrp_admin)VALUES( #{name}, #{description}, 0)")
	@Options(useGeneratedKeys=true,keyProperty="id",keyColumn="ugrp_id")
	@ResultMap("userGroupMap")
	public void createUserGroup(UserGroup userGroup) throws Exception;
	
	@Select("SELECT * FROM user_groups")
	@ResultMap("userGroupMap")
	public List<UserGroup> getAllUserGroups() throws Exception;
	
	@Delete("DELETE FROM user_groups where ugrp_id = #{groupId}")
	public void deleteUserGroup(@Param("groupId") int groupId) throws Exception;
	 
}