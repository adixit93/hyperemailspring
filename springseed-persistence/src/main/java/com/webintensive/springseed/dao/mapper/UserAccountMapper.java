package com.webintensive.springseed.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.pojomodel.UserAccount;

/**
 * MyBatis mapper for user account mapper.
 * 
 * @author gauravg
 */
public interface UserAccountMapper {
	@Select("select * from user_accounts where uacc_username = #{username}")
	@ResultMap("detailedUserAccountMap")
	public UserAccount getUserAccountByUsername(@Param("username") String username);

	@Select("select count(1) from user_accounts where uacc_username = #{username}")
	@ResultType(value=Integer.class)
	public Integer getUserCountByUsername(@Param("username") String username);
	
	@Select("select * from user_accounts where uacc_email = #{email}")
	@ResultMap("detailedUserAccountMap")
	public UserAccount getUserAccountByEmail(@Param("email") String email);

	@Select("select count(DISTINCT uacc_email) from user_accounts where uacc_email = #{email}")
	@ResultType(value=Integer.class)
	public Integer getUserCountByEmail(@Param("email") String email);
	
	@Select("select * from user_accounts ua left join user_groups ug on ua.uacc_group_fk = ug.ugrp_id where ua.uacc_username = #{username}")
	@ResultMap("detailedUserAccountMap")
	public UserAccount getDetailedUserAccountByUsername(@Param("username") String username);

	@Select("select uacc_username, uacc_email, uacc_id, uacc_forgotten_password_token, uacc_forgotten_password_expire from user_accounts where uacc_forgotten_password_token = #{token}")
	@ResultMap("userAccountMap")
	public UserAccount getUserAccountByForgotPasswordToken(@Param("token") String token) throws Exception;

	@Insert("INSERT INTO user_accounts(uacc_group_fk,uacc_email,uacc_username,uacc_password,uacc_ip_address,uacc_salt,uacc_activation_token,uacc_active,uacc_suspend,uacc_date_added)VALUES(#{userGroup.id},#{email},#{username},#{password},#{ipAddress},#{salt},#{activationToken},1,1,now())")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "uacc_id")
	public void createUserAccount(UserAccount userAccount);

	@Insert("INSERT INTO user_accounts( uacc_group_fk,uacc_email,uacc_username,uacc_password,uacc_ip_address,uacc_salt,uacc_activation_token,uacc_active,uacc_suspend,uacc_date_added)VALUES( #{groupId},#{email},#{username},#{password},#{ipAddress},#{salt},#{activationToken},1,1,now())")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "uacc_id")
	public int persistUserAccount(@Param("groupId") int groupId, @Param("email") String email,
			@Param("username") String username, @Param("password") String password,
			@Param("ipAddress") String ipAddress, @Param("salt") String salt,
			@Param("activationToken") String activationToken) throws Exception;

	@Select("SELECT uacc_id, uacc_username, uacc_email, uacc_active, uacc_suspend FROM user_accounts WHERE uacc_activation_token = #{token}")
	@ResultMap("userAccountMap")
	public UserAccount getUserAccountByActivationToken(@Param("token") String token) throws Exception;

	@Select("UPDATE user_accounts SET uacc_suspend = 0 WHERE uacc_id = #{id}")
	public void activate(@Param("id") int id) throws Exception;

	@Update("UPDATE user_accounts SET uacc_date_last_login = now(), uacc_fail_login_attempts = 0 WHERE uacc_username = #{username}")
	public void updateLastLogin(@Param("username") String username) throws Exception;

	@Update("UPDATE user_accounts SET uacc_fail_login_attempts = uacc_fail_login_attempts + 1, uacc_fail_login_ip_address = #{ipAddress}  WHERE uacc_username = #{username}")
	public void updateFailedLoginCount(@Param("username") String username, @Param("ipAddress") String ipAddress);

	@Update("UPDATE user_accounts SET uacc_fail_login_attempts = 0, uacc_suspend = 1 WHERE uacc_username = #{username}")
	public void suspendUserAccount(@Param("username") String username);

	@Update("UPDATE user_accounts SET uacc_forgotten_password_token = #{token}, uacc_forgotten_password_expire = DATE_ADD(now(), INTERVAL 2 HOUR) WHERE uacc_username = #{username} OR uacc_email = #{email}")
	public void generateForgotPasswordToken(@Param("username") String username, @Param("email") String email,
			@Param("token") String token);

	@Update("UPDATE user_accounts SET uacc_password = #{password}, uacc_salt = #{salt}, uacc_fail_login_attempts = 1, uacc_forgotten_password_token = NULL, uacc_forgotten_password_expire = NULL  WHERE uacc_username=#{username}")
	public void updatePassword(@Param("username") String username, @Param("password") String password,
			@Param("salt") String salt);
	

}