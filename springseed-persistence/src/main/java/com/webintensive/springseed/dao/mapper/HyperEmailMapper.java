package com.webintensive.springseed.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.TypeDiscriminator;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.Case;

import com.webintensive.springseed.exceptionhandling.customexceptions.HyperEmailException;
import com.webintensive.springseed.hyperemail.models.Contact;
import com.webintensive.springseed.hyperemail.models.Delegation;

import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.FolderPermission;
import com.webintensive.springseed.hyperemail.models.IndividualEmailFilter;
import com.webintensive.springseed.hyperemail.models.MessageContentFilter;
import com.webintensive.springseed.hyperemail.models.PrivateContactFilter;
import com.webintensive.springseed.hyperemail.models.DomainFilter;
import com.webintensive.springseed.hyperemail.models.Filter;

import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;

public interface HyperEmailMapper {

	@Select("select * from user_details u join user_accounts ua on ua.uacc_id = u.user_accounts_uacc_id where  ua.uacc_username=#{username}")
	@Results(value = { @Result(property = "username", column = "uacc_username"),
			@Result(property = "password", column = "uacc_password"),
			@Result(property = "firstName", column = "first_name"),
			@Result(property = "middleName", column = "middle_name"),
			@Result(property = "lastName", column = "last_name")

	})
	public User getUserInfoByUsername(@Param("username") String username);

	@Insert("INSERT INTO user_details (first_name,middle_name,last_name,email,user_accounts_uacc_id) VALUES (#{firstName},#{middleName},#{lastName},#{email},#{id})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void addUserDetails(User user);

	@Select("SELECT * FROM user_email_account where user_id=#{userId}")
	@ResultMap("userEmailAccountMap")
	public List<UserEmailAccount> getAllAccounts(@Param("userId") int userId);

	@Select("select * from delegate_information di join delegation d on di.delegation_id=d.id join user_details ud on ud.id = di.delegate_id join user_accounts ua on ua.uacc_id = ud.user_accounts_uacc_id where d.user_id = #{userId} and di.delegation_id = #{delegationId}")
	@Results(value = { @Result(property = "id", column = "id"), @Result(property = "user.id", column = "user_id"),
			@Result(property = "user.firstName", column = "first_name"),
			@Result(property = "user.middleName", column = "middle_name"),
			@Result(property = "user.lastName", column = "last_name"),
			@Result(property = "user.username", column = "uacc_username"),
			@Result(property = "user.password", column = "uacc_password"),
			@Result(property = "name", column = "name") })
	public List<Delegation> getDelegateUserList(@Param("delegationId") int delegationId, @Param("userId") int userId);

	@Select("SELECT * FROM user_email_account ua left join user_details u on u.id=ua.user_id ")
	@ResultMap("userEmailAccountMap")
	public List<UserEmailAccount> getAllEmailAccounts();

	@Select("SELECT id from user_details ud left join user_accounts ua on ua.uacc_id=ud.user_accounts_uacc_id where ua.uacc_username=#{username}")
	public int getIdByUserName(@Param("username") String username);

	@Insert("INSERT INTO user_email_account (user_id,password,email,imap_domain,imap_port,smtp_domain,smtp_port) VALUES (#{user.id},#{password},#{email},#{imapDomain},#{imapPort},#{smtpDomain},#{smtpPort})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void addUserEmailAccount(UserEmailAccount userEmailAccount);

	@Delete("DELETE FROM user_email_account CASCADE WHERE id = #{accountId}")
	public int deleteUserEmailAccount(@Param("accountId") int accountId);

	@Update("UPDATE user_email_account SET password = #{password},email = #{email},imap_domain = #{imapDomain},smtp_domain = #{smtpDomain},imap_port=#{imapPort},smtp_port=#{smtpPort} WHERE id=#{id}")
	public void editUserEmailAccount(UserEmailAccount userEmailAccount);

	@Insert("INSERT INTO delegate_information(delegate_id,delegation_id)VALUES(#{user.id},#{id})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void createDelegate(Delegation delegation);

	@Delete("DELETE FROM delegate_information where id = #{delegateId}")
	public int deleteDelegate(@Param("delegateId") int delegateId);

	@Select("select * from folder where user_email_accounts_id=#{accountId} and id=#{userId}")
	@ResultMap("folderMap")
	public List<Folder> getFolder(@Param("accountId") int accountId);

	@Select("select * from folder_folderAccess where delegationId=#{delegationId}")
	public List<Folder> getAccessFolder(@Param("delegationId") int delegationId);

	@Select("select * from folder where user_email_account_id=#{accountId}")
	public List<Folder> getFolderListByEmailId(@Param("accountId") int accountId);

	@Insert("INSERT INTO folder(name,identifier,user_email_account_id)VALUES(#{name},#{identifier},#{userEmailAccount.id})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void addFolder(Folder folder);

	@Insert("INSERT INTO folder(name,identifier,userEmailAccountId)VALUES(#{name},#{identifier},#{userEmailAccount.id})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void FolderAccess(Folder folder);

	@Insert("INSERT INTO filter(filter_id,delegation_id,content)VALUES(#{id},#{delegation.id},#{content})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void createPrivateContact(Filter filter);

	@Select("select id from lookup_filter where type=#{type}")
	public int getFilterIdByFilterType(String type);

	@Select("select * from delegation where user_id = #{id}")
	// @Result(property = "user.id", column = "user_id")
	public List<Delegation> getDelegation(@Param("id") int id);

	@Insert("INSERT INTO delegation(user_id,name)VALUES(#{user.id},#{name})")
	@Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
	public void addDelegation(Delegation delegation);

	@Delete("DELETE FROM delegation where id = #{delegationId}")
	public int deleteDelegation(@Param("delegationId") int delegationId);

	@Insert("INSERT INTO folder_folderaccess(folderaccess_id,delegation_id,folder_id)VALUES(#{folderaccessId},#{delegationId},#{folderId})")
	public void addFolderPermission(@Param("folderId") Integer folderId,
			@Param("folderaccessId") Integer folderaccessId, @Param("delegationId") Integer delegationId);

	@Select("select * from user_email_account where id=#{accountId}")
	@ResultMap("userEmailAccountMap")
	public UserEmailAccount getAccountInfoById(@Param("accountId") Integer accountId);

	@Select("select id from lookup_folderaccess where access_rights=#{type}")
	public int getPermissionId(@Param("type") String type);

	@Select("select id from lookup_filter where type=#{type}")
	public int getContactId(@Param("type") String type);

	@Insert("INSERT INTO filter(filter_id,delegation_id,content)VALUES(#{filterId},#{delegationId},#{content})")
	public void addContact(@Param("filterId") Integer filterId, @Param("content") String content,
			@Param("delegationId") Integer delegationId);

	@Select("select folder_id from folder_folderaccess where delegation_id=#{delegationId}")
	public Integer getFolderId(@Param("delegationId") int delegationId);

	@Select("select distinct on(f.id)  f.id,name,identifier,f.user_email_account_id  from folder f"
			+ " left join folder_folderaccess ffa on f.id = ffa.folder_id where ffa.delegation_id = #{delegationId} and f. user_email_account_id=#{accountId}")
	@Results(value = { @Result(property = "userEmailAccount.id", column = "user_email_account_id")

	})
	public List<Folder> getFolderAccess(@Param("delegationId") int delegationId, @Param("accountId") int accountId);

	@Select("select * from user_email_accounts where delegationId=#{delegationId}")
	public Integer getContactEmail(@Param("email") int email);

	@Select("select access_rights from lookup_folderaccess lf left join  folder_folderaccess fa on fa.folderaccess_id=lf.id  where fa.folder_id=#{folderId} and fa.delegation_id=#{delegationId}")
	public List<FolderPermission> getFolderPermission(@Param("folderId") int folderId,
			@Param("delegationId") int delegationId);

	@Delete("DELETE FROM filter WHERE id = #{accessId}")
	public int deletePrivateDetail(@Param("accessId") int accessId);


	@Select("Select * from filter where delegation_id = #{delegationId}")
	@TypeDiscriminator(column = "filter_id", cases = {
			@Case(value = DomainFilter.DTYPE, type = DomainFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = IndividualEmailFilter.DTYPE, type = IndividualEmailFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = MessageContentFilter.DTYPE, type = MessageContentFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = PrivateContactFilter.DTYPE, type = PrivateContactFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }) })
	List<Filter> getFilter(@Param("delegationId") int delegationId);

	@Select("Select * from filter where delegation_id = #{delegationId} and filter_id =#{type}")
	@TypeDiscriminator(column = "filter_id", cases = {
			@Case(value = DomainFilter.DTYPE, type = DomainFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = IndividualEmailFilter.DTYPE, type = IndividualEmailFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = MessageContentFilter.DTYPE, type = MessageContentFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = PrivateContactFilter.DTYPE, type = PrivateContactFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }) })
	List<Filter> getFiltersByType(@Param("delegationId") int delegationId, @Param("type") Integer type);

	// HyperEmail
	@Select("select * from user_details ud join user_accounts ua on ua.uacc_id = ud.user_accounts_uacc_id")
	@ResultMap("userMap")
	// @Results(value = { @Result(property = "username", column =
	// "uacc_username")})
	public List<User> getUserAccount();

	@Select("Select * from filter f left join delegation d on f.delegation_id=d.id left join delegate_information di on di.delegation_id=d.id where di.delegate_id=#{delegateId} and d.user_id=#{userId} ")
	@TypeDiscriminator(column = "filter_id", cases = {
			@Case(value = DomainFilter.DTYPE, type = DomainFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = IndividualEmailFilter.DTYPE, type = IndividualEmailFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = MessageContentFilter.DTYPE, type = MessageContentFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }),
			@Case(value = PrivateContactFilter.DTYPE, type = PrivateContactFilter.class, results = {
					@Result(property = "delegation.id", column = "delegation_id") }) })
	List<Filter> getFiltersByEmail(@Param("userId") Integer userId, @Param("delegateId") Integer delegateId);

	// james
	@Select("SELECT * FROM user_details u left join user_accounts ua on u.user_accounts_uacc_id = ua.uacc_id where ua.uacc_username=#{username}")
	public User getUserAccounts(@Param("username") String username);

	@Select("SELECT * FROM user_email_account where user_id=#{userId} ")
	@ResultMap("userEmailAccountMap")
	public List<UserEmailAccount> getUserEmailAccounts(@Param("userId") Integer userId);

	@Select("SELECT * FROM user_email_account where email=#{email} ")
	@ResultMap("userEmailAccountMap")
	public UserEmailAccount getUserEmailAccountByEmail(@Param("email") String email);

	@Select("SELECT * FROM user_accounts ua left join user_details u on ua.uacc_id=u.user_accounts_uacc_id left join delegation d on d.user_id=u.id left join delegate_information di on di.delegation_id=d.id where delegate_id=#{userId} ")
	@Results(value = { @Result(property = "username", column = "uacc_username"),
			@Result(property = "password", column = "uacc_password")

	})
	public List<User> getUserList(@Param("userId") int userId);

	@Select("select * from user_details ud join user_accounts ua on ua.uacc_id = ud.user_accounts_uacc_id where ud.id=#{userId} ")
	// @ResultMap("userMap")
	@Results(value = { @Result(property = "password", column = "uacc_password"),
			@Result(property = "username", column = "uacc_username") })
	public User getUserInfo(@Param("userId") int userId);

	@Select("SELECT * FROM folder f left join folder_folderaccess fa on f.id=fa.folder_id left join delegation d on fa.delegation_id=d.id where d.user_id=#{userId}")
	@Results(value = { @Result(property = "userEmailAccount.id", column = "user_email_account_id") })
	public List<Folder> getFolderByUserId(@Param("userId") int userId);

	/*
	 * SOUMYA
	 */

	@Select("SELECT * FROM user_email_account where  email=#{email}")
	@ResultMap("userEmailAccountMap")
	public UserEmailAccount getAccountDetailsByEmail(@Param("email") String email);

	@Select("select * from contacts where  user_email_account_id=#{userEmailId} and contact=#{contact}")
	@Results(value = { @Result(property = "id", column = "id"),
			@Result(property = "account.id", column = "user_email_account_id"),
			@Result(property = "countMail", column = "count"), @Result(property = "email", column = "contact") })
	public Contact getContactInfoByUseEmailAndContact(@Param("userEmailId") Integer userEmailId,
			@Param("contact") String contact);

	@Insert("INSERT INTO contacts(user_email_account_id,contact,count) VALUES (#{userEmailId},#{contact},#{count})")
	public void insertContact(@Param("userEmailId") Integer userEmailId, @Param("contact") String contact,
			@Param("count") Integer count);

	@Update("UPDATE contacts SET count = #{count} where id=#{id}")
	public void updateContact(@Param("id") Integer id, @Param("count") Integer count);

	@Select("select * from contacts c join user_email_account uea on uea.id = c.user_email_account_id where  uea.user_id=#{userId} ORDER BY	c.count DESC")
	@Results(value = { @Result(property = "id", column = "id"),
			@Result(property = "userEmailAccount.id", column = "user_email_account_id"),
			@Result(property = "countMail", column = "count"), @Result(property = "email", column = "contact") })
	public List<Contact> getContactListByUserId(@Param("userId") Integer userId);

}
