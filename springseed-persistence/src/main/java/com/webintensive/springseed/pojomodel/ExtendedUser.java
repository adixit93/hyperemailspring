package com.webintensive.springseed.pojomodel;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * A user model that conforms to the spring security's user detail service norms.
 * 
 * @author gauravg
 */
public class ExtendedUser extends User {

	private static final long serialVersionUID = -2804363123057109029L;
	private String salt;

	public ExtendedUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}
}