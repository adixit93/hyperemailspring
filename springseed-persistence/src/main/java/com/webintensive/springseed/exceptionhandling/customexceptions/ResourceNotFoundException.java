package com.webintensive.springseed.exceptionhandling.customexceptions;

public class ResourceNotFoundException extends SpringSeedException {
	private static final long serialVersionUID = 1500388614383849015L;
	
	public ResourceNotFoundException(String msg) {
		super(msg);
	}

}
