package com.webintensive.springseed.exceptionhandling.customexceptions.useraccount;

import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;

public class UserAlreadyExsistsException extends SpringSeedException {
	private static final long serialVersionUID = 1500388614383849015L;
	
	public UserAlreadyExsistsException(String msg) {
		super(msg);
	}

}
