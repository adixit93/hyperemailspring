package com.webintensive.springseed.exceptionhandling.customexceptions.useraccount;

import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;

public class InvalidForgotPasswordTokenException extends SpringSeedException {
	private static final long serialVersionUID = 1500388614383849035L;
	
	public InvalidForgotPasswordTokenException(String msg) {
		super(msg);
	}

}
