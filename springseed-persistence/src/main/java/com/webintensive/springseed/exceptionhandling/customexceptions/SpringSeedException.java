package com.webintensive.springseed.exceptionhandling.customexceptions;

/**
 * Generic project wide exception.
 * 
 * @author gauravg
 */
public class SpringSeedException extends Exception {
	private static final long serialVersionUID = 1500388614383849015L;
	
	public SpringSeedException(String msg) {
		super(msg);
	}
	
	public SpringSeedException(Exception e) {
		super(e);
	}
	
	public SpringSeedException(String msg, Exception e) {
		super(msg, e);
	}

}
