package com.webintensive.springseed.exceptionhandling;

import org.omg.CORBA.portable.UnknownException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import com.webintensive.springseed.exceptionhandling.customexceptions.ResourceNotFoundException;
import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;

/**
 * This class holds the exception handlers for general exceptions which are
 * common to the REST API.
 * 
 * @author gauravg
 */
@ControllerAdvice(annotations = RestController.class)
public class GlobalRestControllerExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalRestControllerExceptionHandler.class);

	@ResponseStatus(HttpStatus.CONFLICT)
	// 409
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseBody
	public String handleDataIntegrityConflict(DataIntegrityViolationException e) {
		logger.error("Data integrity violation exception.", e);
		return e.getMessage();
	}

	@ExceptionHandler(value = ResourceNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	// 404
	@ResponseBody
	public String resourceNotFoundExceptionHandler(Exception exception) {
		logger.error("Resource not found exception.", exception);
		return exception.getMessage();
	}

	@ExceptionHandler(value = AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	// 403
	@ResponseBody
	public String accessDeniedException(AccessDeniedException e) {
		logger.error("Access denied exception.", e);
		return e.getMessage();
	}
	
	@ExceptionHandler(value ={ SpringSeedException.class, UnknownException.class})
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	// 500
	@ResponseBody
	public String genericSpringSeedException(Exception e) {
		logger.error("Unknown exception occurred.", e);
		return e.getMessage();
	}
}
