package com.webintensive.springseed.exceptionhandling.customexceptions;

public class PrivilegeAssignmentFailedException extends SpringSeedException {
	private static final long serialVersionUID = 1500388614383849032L;

	public PrivilegeAssignmentFailedException(String msg) {
		super(msg);
	}
}
