package com.webintensive.springseed.exceptionhandling.customexceptions;

public class HyperEmailException extends SpringSeedException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HyperEmailException(Exception e) {
		super(e);
	}

	public HyperEmailException(String msg) {
		super(msg);
	}
	
	public HyperEmailException(String msg, Exception e) {
		super(msg, e);
	}

}
