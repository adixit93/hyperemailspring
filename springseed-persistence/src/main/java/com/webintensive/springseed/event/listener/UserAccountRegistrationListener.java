package com.webintensive.springseed.event.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


import com.webintensive.springseed.event.NewUserAccountRegisteredEvent;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.generic.MailerService;

/**
 * This class represents the listener to the account creation event.
 * TODO: Use a queue for mails.
 * TODO: Use a proper template for emails. 
 * 
 * @author gauravg
 */
@Component
public class UserAccountRegistrationListener implements ApplicationListener<NewUserAccountRegisteredEvent> {

	@Autowired
    private MailerService mailerService;
	@Value("${SERVER_URL}")
	private String serverURL;
	@Value("${ACCOUNT_ACTIVATION_URI}")
	private String activationURI;
	private static final Logger logger1 = LoggerFactory.getLogger(UserAccountRegistrationListener.class);
	@Override
	public void onApplicationEvent(NewUserAccountRegisteredEvent event) {
		final UserAccount user = event.getUserAccount();
		final String message = new StringBuilder("Your activation token is - ").append(user.getActivationToken())
				.append("<br/>").append("Activation link - ").append(serverURL).append(activationURI)
				.append(user.getActivationToken()).toString();
		final String subject = "Welcome Mail from Seed project";
		logger1.info(message);
		mailerService.sendMail(user.getEmail(), subject, message);
	}

}
