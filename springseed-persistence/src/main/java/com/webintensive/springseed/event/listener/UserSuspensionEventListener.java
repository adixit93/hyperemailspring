package com.webintensive.springseed.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.webintensive.springseed.event.UserSuspendedEvent;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.generic.MailerService;

/**
 * This class represents the listener to the account creation event. 
 * TODO: Use a queue for mails. 
 * TODO: Use a proper template for emails.
 * 
 * @author gauravg
 */
@Component
public class UserSuspensionEventListener implements ApplicationListener<UserSuspendedEvent> {

	@Autowired
	private MailerService mailerService;

	@Override
	public void onApplicationEvent(UserSuspendedEvent event) {
		final UserAccount user = event.getUserAccount();

		final StringBuilder message = new StringBuilder("Hi ")
				.append(user.getUsername())
				.append(", \nThe maximum login attempts have been succeeded. We are suspending your account for security reasons. Please take this up with admin for further assistance.");
		mailerService.sendMail(user.getEmail(), "Maximum login attempts exceeded. Account suspeneded.",
				message.toString());
	}

}
