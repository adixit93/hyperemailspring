package com.webintensive.springseed.event;

import org.springframework.context.ApplicationEvent;

import com.webintensive.springseed.pojomodel.UserAccount;


/**
 * An {@link ApplicationEvent}.
 * 
 * @author gauravg
 */
public class NewForgotPasswordTokenCreatedEvent extends UserAccountEvent {
	public NewForgotPasswordTokenCreatedEvent(UserAccount userAccount) {
		super(userAccount);
	}

	private static final long serialVersionUID = -7984221651123762468L;
}