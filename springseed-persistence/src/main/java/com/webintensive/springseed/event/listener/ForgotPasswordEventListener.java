package com.webintensive.springseed.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.webintensive.springseed.event.NewForgotPasswordTokenCreatedEvent;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.service.generic.MailerService;

/**
 * This class represents the listener to the event. TODO: Use a queue for mails.
 * TODO: Use a proper template for emails.
 * 
 * @author gauravg
 */
@Component
public class ForgotPasswordEventListener implements ApplicationListener<NewForgotPasswordTokenCreatedEvent> {

	@Autowired
	private MailerService mailerService;
	@Value("${SERVER_URL}")
	private String serverURL;
	@Value("${RESET_PASSWORD_URI}")
	private String resetPasswordURI;

	// TODO: use i18n instead
	private final static String SUBJECT = "Reset Password Link";

	@Override
	public void onApplicationEvent(NewForgotPasswordTokenCreatedEvent event) {
		final UserAccount user = event.getUserAccount();
		final StringBuilder message = new StringBuilder(
				"Reset password link has been generated. Please follow the link to update your password -\n")
				.append(serverURL).append(resetPasswordURI).append(user.getActivationToken());
		mailerService.sendMail(user.getEmail(), SUBJECT, message.toString());
	}

}
