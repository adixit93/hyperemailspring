package com.webintensive.springseed.event;

import org.springframework.context.ApplicationEvent;

import com.webintensive.springseed.pojomodel.UserAccount;

/**
 * An {@link ApplicationEvent}.
 * 
 * @author gauravg
 */
public class UserSuspendedEvent extends UserAccountEvent {
	private static final long serialVersionUID = -7984221651123768438L;
	
	public UserSuspendedEvent(UserAccount userAccount) {
		super(userAccount);
	}
}