package com.webintensive.springseed.event;

import org.springframework.context.ApplicationEvent;

import com.webintensive.springseed.pojomodel.UserAccount;

/**
 * An {@link ApplicationEvent}. A generic event for all the application events
 * related to user accounts.
 * 
 * @author gauravg
 */
public class UserAccountEvent extends ApplicationEvent {
	private static final long serialVersionUID = -7984221651123768438L;
	private final UserAccount userAccount;

	public UserAccountEvent(UserAccount userAccount) {
		super(userAccount);
		this.userAccount = userAccount;
	}

	public UserAccount getUserAccount() {
		return userAccount;
	}
}