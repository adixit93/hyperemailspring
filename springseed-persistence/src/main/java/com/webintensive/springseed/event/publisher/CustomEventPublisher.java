package com.webintensive.springseed.event.publisher;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;

/**
 * Event publisher for the project. Its merely a way of telling Springs, that we
 * will be using application events.
 * 
 * @author gauravg
 */
@Service
public class CustomEventPublisher implements ApplicationEventPublisherAware {
	private ApplicationEventPublisher publisher;

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.publisher = applicationEventPublisher;
	}

	public void publish(ApplicationEvent event) {
		this.publisher.publishEvent(event);
	}
}
