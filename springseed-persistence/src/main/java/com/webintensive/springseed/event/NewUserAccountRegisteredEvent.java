package com.webintensive.springseed.event;

import org.springframework.context.ApplicationEvent;

import com.webintensive.springseed.pojomodel.UserAccount;

/**
 * An {@link ApplicationEvent}.
 * 
 * @author gauravg
 */
public class NewUserAccountRegisteredEvent extends UserAccountEvent {
	private static final long serialVersionUID = -7984221651123768468L;

	public NewUserAccountRegisteredEvent(UserAccount userAccount) {
		super(userAccount);
	}
}