package com.webintensive.springseed.service;

import java.util.List;

import com.webintensive.springseed.exceptionhandling.customexceptions.HyperEmailException;

import com.webintensive.springseed.hyperemail.models.Contact;
import com.webintensive.springseed.hyperemail.models.Delegation;
import com.webintensive.springseed.hyperemail.models.Filter;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.RecentlyContacted;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;

public interface UserEmailAccountService {

	// Add User Details
	User addUserDetails(User user) throws HyperEmailException;

	// Connect to my accounts
	List<UserEmailAccount> getAllAccounts(int userId) throws HyperEmailException;
	UserEmailAccount addUserEmailAccount(UserEmailAccount userEmailAccount) throws HyperEmailException;
	int deleteUserEmailAccount(int accountId) throws HyperEmailException;
	UserEmailAccount editUserEmailAccount(UserEmailAccount userEmailAccount) throws HyperEmailException;

	// Delegation
	List<Delegation> getDelegation(int userId) throws HyperEmailException;
	Delegation addDelegation(Delegation delegation) throws HyperEmailException;
	int deleteDelegation(Integer delegationId) throws HyperEmailException;

	// Who may help me?
	List<User> userList() throws HyperEmailException;
	List<Delegation> getDelegateUserList(int delegationId, int userId) throws HyperEmailException;
	Delegation createDelegate(Delegation delegation) throws HyperEmailException;
	int deleteDelegate(int delegateId) throws HyperEmailException;

	// Which Folders they may access?
	List<Folder> getFolder(int accountId) throws HyperEmailException;
	List<Folder> getAccessFolder(int delegationId, int accountId) throws HyperEmailException;
	Folder createFolder(Integer userId, Folder folder, int delegationId) throws HyperEmailException;
	void createFolderPermission(Delegation delegation) throws HyperEmailException;

	// which contacts are private
	
	Filter addPrivateContact(Filter filter, String type) throws HyperEmailException;
	int deletePrivateDetails(int accessId) throws HyperEmailException;
	List<Filter> getFilter(int delegationId) throws HyperEmailException;
	List<Filter> getFiltersByType(String type, int delegationId) throws HyperEmailException;
	User getUserInfoByUsername(String username) throws HyperEmailException;

	// James
	List<UserEmailAccount> getUserEmailAccounts(String username) throws HyperEmailException;
	List<Filter> getFiltersByEmail(String userEmail,String delegateEmail) throws HyperEmailException;
	List<Folder> getFolderByEmail(String email) throws HyperEmailException;
	List<Delegation> getFolderListByDelegateUserName(String username) throws HyperEmailException;
	User getUserDetails(String username) throws HyperEmailException;

	User getUserDetails(int userId) throws HyperEmailException;

	/*
	 * SOUMYA
	 */
	void addContacts(RecentlyContacted recentlyContacted) throws HyperEmailException;
	List<Contact> getContactList(int userId) throws HyperEmailException;
}


