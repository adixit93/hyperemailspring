package com.webintensive.springseed.service;

import java.util.List;

import com.webintensive.springseed.pojomodel.UserPrivilege;

public interface UserPrivilegeService {
	UserPrivilege getPrivilegeById(int privId);
	List<UserPrivilege> getAllPrivileges();
	int createPrivilege(UserPrivilege userPrivilege);
	UserPrivilege persistPrivilege(String name, String description);
	
	int assignUserPrivilege(int userId, short privId);
	int assignGroupPrivilege(int groupId, short privId);
	List<UserPrivilege> getAllPrivilegesByUsername(int userId, int groupId);
}
