package com.webintensive.springseed.service.security.token;

import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import org.springframework.stereotype.Service;

/**
 * @author gauravg
 */
@Service("saltGeneratorService")
public class TokenGeneratorService {
	
	private static final Random RANDOM = new SecureRandom();
	
	public String generateRandomSalt() {
		byte[] salt = new byte[32];
	    RANDOM.nextBytes(salt);
	    // the angular bracket are necessary as spring expects them enclosing the salt.
		return new StringBuilder("{").append(salt).append("}").toString();
	}
	
	public String generateActivationToken() {
		return generateRandomUUID();
	}
	
	public String generateForgotPasswordToken() {
		return generateRandomUUID();
	}
	
	private String generateRandomUUID() {
		return UUID.randomUUID().toString();
	}
}
