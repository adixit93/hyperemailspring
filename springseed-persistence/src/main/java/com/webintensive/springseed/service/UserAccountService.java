package com.webintensive.springseed.service;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.ActivatingActiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InactiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidActivationTokenException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidForgotPasswordTokenException;
import com.webintensive.springseed.pojomodel.UserAccount;

public interface UserAccountService {
	UserAccount loadUserByUsername(String username) throws UsernameNotFoundException;

	UserAccount createUserAccount(Integer groupId, String email, String username, String password, String ipAddress, String firstName, String middleName, String lastName) throws Exception;

	boolean activateUser(String token) throws InvalidActivationTokenException, InactiveUserException,
			ActivatingActiveUserException;
	
	void updateLastLogin(String username) throws Exception;
	
	void updateFailedLoginCount(String username, String ipAddress);
	
	void suspendUserAccount(String username, String email);
	
	void generateForgotPasswordLink(String username, String email) throws SpringSeedException;
	
	UserAccount getUserAccountByForgotPasswordToken(String token) throws InvalidForgotPasswordTokenException;

	void updatePassword(String username, String password, String token) throws InvalidForgotPasswordTokenException;
}
