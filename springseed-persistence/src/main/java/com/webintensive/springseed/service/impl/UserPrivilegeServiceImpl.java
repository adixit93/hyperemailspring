package com.webintensive.springseed.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webintensive.springseed.dao.mapper.UserPrivilegeMapper;
import com.webintensive.springseed.pojomodel.UserPrivilege;
import com.webintensive.springseed.service.UserPrivilegeService;

@Service("userPrivilegeService")
public class UserPrivilegeServiceImpl implements UserPrivilegeService {

	private static final Logger logger = LoggerFactory.getLogger(UserPrivilegeServiceImpl.class);
	
	@Autowired
	private UserPrivilegeMapper userPrivilegeMapper;

	@Override
	public UserPrivilege getPrivilegeById(int privId) {
		return userPrivilegeMapper.getPrivilegeById(privId);
	}

	@Override
	public List<UserPrivilege> getAllPrivileges() {
		return userPrivilegeMapper.getAllPrivileges();
	}

	@Override
	public int createPrivilege(UserPrivilege userPrivilege) {
		return userPrivilegeMapper.createPrivilege(userPrivilege);
	}

	@Override
	public UserPrivilege persistPrivilege(String name, String description) {
		int primaryKey = userPrivilegeMapper.persistPrivilege(name, description);
		logger.info("Created a new privilege named {} with primary key {}", name, primaryKey);
		return userPrivilegeMapper.getPrivilegeById(primaryKey);
	}

	@Override
	public int assignUserPrivilege(int userId, short privId) {
		return userPrivilegeMapper.assignUserPrivilege(userId, privId);
	}

	@Override
	public int assignGroupPrivilege(int groupId, short privId) {
		return userPrivilegeMapper.assignGroupPrivilege(groupId, privId);
	}

	@Override
	public List<UserPrivilege> getAllPrivilegesByUsername(int userId, int groupId) {
		return userPrivilegeMapper.getAllPrivilegesByUsername(userId, groupId);
	}	
}
