package com.webintensive.springseed.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webintensive.springseed.dao.mapper.UserGroupMapper;
import com.webintensive.springseed.pojomodel.UserGroup;
import com.webintensive.springseed.service.UserGroupService;

@Service("userGroupService")
public class UserGroupServiceImpl implements UserGroupService {

	@Autowired
	private UserGroupMapper userGroupMapper;

	@Override
	public UserGroup getUserGroupById(int userGroupId) throws Exception {
		return userGroupMapper.getUserGroupById(userGroupId);
	}

	@Override
	public List<UserGroup> getAllUserGroups() throws Exception {
		return userGroupMapper.getAllUserGroups();
	}

	@Override
	public UserGroup persistUserGroup(String name, String description) throws Exception {
		UserGroup group = new UserGroup();
		group.setName(name);
		group.setDescription(description);
		group.setAdmin((byte) 0);
		userGroupMapper.createUserGroup(group);
		return group;
	}

	@Override
	public void deleteUserGroup(int id) throws Exception {
		userGroupMapper.deleteUserGroup(id);
	}

	@Override
	public UserGroup getUserGroupByGroupName(String groupName) throws Exception {
		return userGroupMapper.getUserGroupByGroupName(groupName);
	}
}