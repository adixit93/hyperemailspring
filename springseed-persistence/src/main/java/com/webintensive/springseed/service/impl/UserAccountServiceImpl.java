package com.webintensive.springseed.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.webintensive.springseed.dao.mapper.HyperEmailMapper;
import com.webintensive.springseed.dao.mapper.UserAccountMapper;
import com.webintensive.springseed.event.NewForgotPasswordTokenCreatedEvent;
import com.webintensive.springseed.event.NewUserAccountRegisteredEvent;
import com.webintensive.springseed.event.UserSuspendedEvent;
import com.webintensive.springseed.event.publisher.CustomEventPublisher;
import com.webintensive.springseed.exceptionhandling.customexceptions.SpringSeedException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.ActivatingActiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InactiveUserException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidActivationTokenException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.InvalidForgotPasswordTokenException;
import com.webintensive.springseed.exceptionhandling.customexceptions.useraccount.UserAlreadyExsistsException;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.pojomodel.UserGroup;
import com.webintensive.springseed.service.UserAccountService;
import com.webintensive.springseed.service.UserEmailAccountService;
import com.webintensive.springseed.service.UserGroupService;
import com.webintensive.springseed.service.security.token.TokenGeneratorService;

@Service("userAccountService")
public class UserAccountServiceImpl implements UserAccountService {

	private static final Logger logger = LoggerFactory.getLogger(UserAccountServiceImpl.class);
	@Value("${SERVER_URL}")
	private String serverURL;
	@Value("${default.user.group}")
	private String defaultUserGroupName;
	@Autowired
	private UserAccountMapper userAccountMapper;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private ShaPasswordEncoder shaPasswordEncoder;
	@Autowired
	private TokenGeneratorService tokenGeneratorService;
	@Autowired
	private CustomEventPublisher eventPublisher;
	
	@Autowired
	private UserEmailAccountService userEmailAccountService;

	@Override
	public UserAccount loadUserByUsername(String username) throws UsernameNotFoundException {
		return userAccountMapper.getDetailedUserAccountByUsername(username);
	}

	@Override
	public UserAccount createUserAccount(Integer groupId, String email, String username, String password,
			String ipAddress, String firstName, String middleName, String lastName) throws UserAlreadyExsistsException, SpringSeedException, Exception {
		UserGroup group = null;
		int usersWithSameEmail;
		int usersWithSameUsername;
		String activationToken = tokenGeneratorService.generateActivationToken();
	
//		
//		String salt = tokenGeneratorService.generateRandomSalt();
//		logger.info(activationToken,salt);
//		logger.info("hello");
//		password = shaPasswordEncoder.encodePassword(password, salt);
		logger.info(password);
		// instead a procedure must be created to get the two statistics in one query
		usersWithSameEmail = userAccountMapper.getUserCountByEmail(email);
		usersWithSameUsername = userAccountMapper.getUserCountByUsername(username);
		
		if (usersWithSameEmail > 0 && usersWithSameUsername > 0) {
			throw new UserAlreadyExsistsException("The email and username combination has already been registered.");
		} else if (usersWithSameEmail > 0) {
			throw new UserAlreadyExsistsException("The email is already registered.");
		} else if (usersWithSameUsername > 0) {
			throw new UserAlreadyExsistsException("The username has already been registered.");
		}

		if (null == groupId) {
			// need a group id
			if (null == defaultUserGroupName) {
				throw new SpringSeedException("The default user group name is not specified.");
			}
			group = userGroupService.getUserGroupByGroupName(defaultUserGroupName);
			if (group == null) {
				
				userGroupService.persistUserGroup(defaultUserGroupName, "Default Group");
				//throw new SpringSeedException("Illegal value of the default user group name has been provided.");
			}
		} else {
			group = new UserGroup();
			group.setId(groupId);
		}

		UserAccount account = new UserAccount();
		account.setUserGroup(group);
		account.setEmail(email);
		account.setUsername(username);
		account.setPassword(password);
		account.setIpAddress(ipAddress);
		account.setSalt("");
		account.setActivationToken(activationToken);

		userAccountMapper.createUserAccount(account);
        
		if (null != account && account.getId() > 0) {
			logger.info("User account with username - {} has been created. Primary key - {}", username, account.getId());
			eventPublisher.publish(new NewUserAccountRegisteredEvent(account));
			User user = new User();
			user.setEmail(account.getEmail());
			user.setId(account.getId());
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setMiddleName(middleName);
			userEmailAccountService.addUserDetails(user);
			
		}
		return account;
	}

	@Override
	public boolean activateUser(String token) throws InvalidActivationTokenException, InactiveUserException,
			ActivatingActiveUserException {
		logger.info("Activation request received for token {}.", token);
		UserAccount user = null;
		try {
			user = userAccountMapper.getUserAccountByActivationToken(token);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		if (null == user) {
			throw new InvalidActivationTokenException("Invalid activation token received - " + token);
		}
		if (!user.isUserEnabled()) {
			throw new InactiveUserException(new StringBuilder("User - ").append(user.getUsername())
					.append(" is not active.").toString());
		}
		if (!user.isUserBlocked()) {
			throw new ActivatingActiveUserException(new StringBuilder("User - ").append(user.getUsername())
					.append(" is suspended or not verified.").toString());
		}

		try {
			userAccountMapper.activate(user.getId());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		logger.info("User - {}, was successfully activated.", user.getUsername());
		return true;
	}

	@Override
	public void updateLastLogin(String username) throws Exception {
		userAccountMapper.updateLastLogin(username);
	}

	@Override
	public void updateFailedLoginCount(String username, String ipAddress) {
		userAccountMapper.updateFailedLoginCount(username, ipAddress);
	}

	@Override
	public void suspendUserAccount(String username, String email) {
		// send the suspension mail to the user, informing about the reason of
		// the suspension.
		// suspend the user
		UserAccount account = null;
		userAccountMapper.suspendUserAccount(username);
		
		account = new UserAccount();
		account.setEmail(email);
		account.setUsername(username);
		eventPublisher.publish(new UserSuspendedEvent(account));
	}

	@Override
	public void generateForgotPasswordLink(String username, String email) throws SpringSeedException {
		UserAccount user = null;
		String token = null;
		if ((null != username) && !username.isEmpty()) {
			user = userAccountMapper.getDetailedUserAccountByUsername(username);
		} else if ((null != email) && !email.isEmpty()) {
			user = userAccountMapper.getUserAccountByEmail(email);
		} else {
			throw new SpringSeedException(
					"Expected username or email while generating forgot password link, found none.");
		}

		if (null == user) {
			throw new UsernameNotFoundException(new StringBuilder("User with username - ").append(username)
					.append(", or with email - ").append(email).append("doesn't exist.").toString());
		}

		token = tokenGeneratorService.generateForgotPasswordToken();
		user.setActivationToken(token);
		userAccountMapper.generateForgotPasswordToken(username, email, token);
		
		eventPublisher.publish(new NewForgotPasswordTokenCreatedEvent(user));			
		
		logger.info("Generated the forgot password token for user ({}, {}).", username, email);
	}

	@Override
	public UserAccount getUserAccountByForgotPasswordToken(String token) throws InvalidForgotPasswordTokenException {
		UserAccount user = null;
		try {
			user = userAccountMapper.getUserAccountByForgotPasswordToken(token);
			if (null == user) {
				logger.error("No user has forgot password token set to {}", token);
				throw new InvalidForgotPasswordTokenException("No user has forgot password token set to " + token);
			}
			if ((null != user.getForgottenPasswordExpire()) && (user.getForgottenPasswordExpire().before(new Date()))) {
				logger.error("Password reset reqest reeived for an expired token {}", token);
				throw new InvalidForgotPasswordTokenException("The forgot password token has expired. Please generate a new token.");
			}
		} catch (Exception e) {
			logger.error("Failed to reteive user with forgot password token set to {}", token, e);
		}
		return user;
	}

	@Override
	public void updatePassword(String username, String password, String token) throws InvalidForgotPasswordTokenException {
		UserAccount userAccount = null;
		try {
			userAccount = getUserAccountByForgotPasswordToken(token);
		} catch (InvalidForgotPasswordTokenException e) {
			logger.error("Invlid token {} used for resetting password.", token);
		}
		
		if ((null != userAccount.getForgottenPasswordExpire()) && (userAccount.getForgottenPasswordExpire().before(new Date()))) {
			logger.error("Password reset reqest reeived for an expired token {}", token);
			throw new InvalidForgotPasswordTokenException("The forgot password token has expired. Please generate a new token.");
		}
		
		String salt = tokenGeneratorService.generateRandomSalt();
		password = shaPasswordEncoder.encodePassword(password, salt);
		
		userAccountMapper.updatePassword(username, password, salt);
	}

}
