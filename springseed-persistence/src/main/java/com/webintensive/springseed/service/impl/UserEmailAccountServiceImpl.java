package com.webintensive.springseed.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.Trigger;
import org.springframework.stereotype.Service;

import com.sun.mail.iap.Argument;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.webintensive.springseed.dao.mapper.HyperEmailMapper;
import com.webintensive.springseed.dao.mapper.UserAccountMapper;
import com.webintensive.springseed.exceptionhandling.customexceptions.HyperEmailException;
import com.webintensive.springseed.hyperemail.models.Contact;
import com.webintensive.springseed.hyperemail.models.Delegation;
import com.webintensive.springseed.hyperemail.models.Filter;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.FolderPermission;
import com.webintensive.springseed.hyperemail.models.RecentlyContacted;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;
import com.webintensive.springseed.hyperemail.utils.FolderUtils;
import com.webintensive.springseed.hyperemail.utils.TriggerJmx;
import com.webintensive.springseed.service.UserEmailAccountService;

@Service("userEmailAccountService")
public class UserEmailAccountServiceImpl implements UserEmailAccountService {

	private static final Logger logger = LoggerFactory.getLogger(UserEmailAccountServiceImpl.class);

	FolderUtils folderUtils = new FolderUtils();
	@Autowired
	private HyperEmailMapper hyperEmailMapper;

	@Override
	public User getUserInfoByUsername(String username) throws HyperEmailException {
		User user;
		try {
			user = hyperEmailMapper.getUserInfoByUsername(username);

		} catch (Exception e) {
			logger.error("Failed to fetch user id", e);
			throw new HyperEmailException("Failed to fetch user id", e);
		}
		return user;
	}

	@Override
	public User addUserDetails(User user) throws HyperEmailException {
		try {
			hyperEmailMapper.addUserDetails(user);
		} catch (Exception e) {
			logger.error("Failed to add user details in user_details.", e);
			throw new HyperEmailException("Failed to add user details.");
		}
		return user;
	}

	@Override
	public List<UserEmailAccount> getAllAccounts(int userId) throws HyperEmailException {
		List<UserEmailAccount> listAccounts = null;
		try {
			listAccounts = hyperEmailMapper.getAllAccounts(userId);
		} catch (Exception e) {
			logger.error("Failed to fetch all user email accounts.", e);
			throw new HyperEmailException("Failed to fetch all user email accounts.");
		}
		return listAccounts;
	}

	@Override
	public UserEmailAccount addUserEmailAccount(UserEmailAccount userEmailAccount) throws HyperEmailException {
		logger.info("Add Email Account");
		try {
			hyperEmailMapper.addUserEmailAccount(userEmailAccount);
			// James james = new James();
			// james.triggerJames();
			TriggerJmx.trigger(userEmailAccount.getUser().getId());
		} catch (Exception e) {
			logger.error("Failed to add email accounts.", e);
			throw new HyperEmailException("Failed to add user email accounts.");
		}
		return userEmailAccount;
	}

	@Override
	public int deleteUserEmailAccount(int accountId) throws HyperEmailException {
		logger.info("Delete Email Account with account ID" + accountId);
		int deletedEmailAccount;

		try {
			deletedEmailAccount = hyperEmailMapper.deleteUserEmailAccount(accountId);
		} catch (Exception e) {
			logger.error("Failed to delete user email accounts.", e);
			throw new HyperEmailException("Failed to delete user email accounts.");
		}
		return deletedEmailAccount;
	}

	@Override
	public UserEmailAccount editUserEmailAccount(UserEmailAccount userEmailAccount) throws HyperEmailException {
		logger.info("Edit Email Account");
		try {
			hyperEmailMapper.editUserEmailAccount(userEmailAccount);
		} catch (Exception e) {
			logger.error("Failed to edit user email accounts.", e);
			throw new HyperEmailException("Failed to edit user email accounts.");
		}
		return userEmailAccount;
	}

	@Override
	public Delegation createDelegate(Delegation delegation) throws HyperEmailException {
		try {
			hyperEmailMapper.createDelegate(delegation);
		} catch (Exception e) {
			logger.error("Failed to add delegate to the delegation.", e);
			throw new HyperEmailException("Failed to add delegate to the delegation.");
		}
		return delegation;
	}

	@Override
	public int deleteDelegate(int delegateId) throws HyperEmailException {
		int deletedDelegate;
		try {
			deletedDelegate = hyperEmailMapper.deleteDelegate(delegateId);
		} catch (Exception e) {
			logger.error("Failed to delete delegate from the delegation.", e);
			throw new HyperEmailException("Failed to delete delegate from the delegation.");
		}
		return deletedDelegate;
	}

	@Override
	public List<Folder> getFolder(int accountId) throws HyperEmailException {
		List<Folder> listFolders;
		FolderUtils folderName = new FolderUtils();

		UserEmailAccount account = new UserEmailAccount();
		try {
			account = hyperEmailMapper.getAccountInfoById(accountId);
			listFolders = folderName.getFolderList(account.getEmail(), account.getPassword(), account.getImapDomain(),
					account.getImapPort());
		} catch (Exception e) {
			logger.error("Failed to get folders of account with Account ID : " + accountId, e);
			throw new HyperEmailException("Failed to get folders of account");
		}
		return listFolders;

	}

	@Override
	public Folder createFolder(Integer userId, Folder folder, int delegationId) throws HyperEmailException {
		try {
			String bossUsername = hyperEmailMapper.getUserInfo(userId).getUsername();
			List<Delegation> delegateUserList = hyperEmailMapper.getDelegateUserList(delegationId, userId);
  
			StringBuilder status = new StringBuilder("fail");
			for (Delegation delegation : delegateUserList) {
				status = folderUtils.addFolderToJames(bossUsername, "james12345.com",
						delegation.getUser().getUsername(), delegation.getUser().getPassword(), folder.getName());
				logger.info("Status " + status);
			}

			if (status != null) {
				if (status.toString().equals("success")) {
					hyperEmailMapper.addFolder(folder);
					hyperEmailMapper.addFolderPermission(folder.getId(), 2, delegationId);
				} else {
					throw new HyperEmailException("Failed to add folders of this account");
				}
			} else {
				throw new HyperEmailException("Failed to add folders of this account");
			}

		} catch (Exception e) {
			logger.error("Failed to add folders of this account", e);
			throw new HyperEmailException("Failed to add folders of this account");
		}
		return folder;
	}

	@Override
	public void createFolderPermission(Delegation delegation) throws HyperEmailException {
		try {
			List<Folder> folderList = delegation.getFolder();

			for (Folder folder : folderList) {
				List<FolderPermission> permissionList = folder.getFolderPermissions();
				for (FolderPermission permission : permissionList) {
					int folderAccesssId;
					folderAccesssId = hyperEmailMapper.getPermissionId(permission.name().toString());
					hyperEmailMapper.addFolderPermission(folder.getId(), folderAccesssId, delegation.getId());

				}
			}
		} catch (Exception e) {
			logger.error("Failed to add Permission to this folder of this account", e);
			throw new HyperEmailException("Failed to add Permission to this folder of this account");
		}
	}

	@Override
	public List<User> userList() throws HyperEmailException {
		List<User> listAccounts;
		try {
			listAccounts = hyperEmailMapper.getUserAccount();
		} catch (Exception e) {
			logger.error("Failed to get all User Accounts.", e);
			throw new HyperEmailException("Failed to get all User Accounts.");
		}
		return listAccounts;
	}

	@Override
	public List<Delegation> getDelegation(int userId) throws HyperEmailException {
		logger.info("Fetch delegations of user with id " + userId);
		List<Delegation> listDelegation;
		try {
			listDelegation = hyperEmailMapper.getDelegation(userId);
		} catch (Exception e) {
			logger.error("Failed to get delegations of user.", e);
			throw new HyperEmailException("Failed to get delegations of user.");
		}
		return listDelegation;
	}

	@Override
	public Delegation addDelegation(Delegation delegation) throws HyperEmailException {
		try {
			hyperEmailMapper.addDelegation(delegation);

		} catch (Exception e) {
			logger.error("Failed to add new delegation.", e);
			throw new HyperEmailException("Failed to add new delegation.");
		}
		return delegation;
	}

	@Override
	public int deleteDelegation(Integer delegationId) throws HyperEmailException {

		int deletedDelegation;
		try {
			deletedDelegation = hyperEmailMapper.deleteDelegation(delegationId);
		} catch (Exception e) {
			logger.error("Failed to delete  delegation.", e);
			throw new HyperEmailException("Failed to delete  delegation.");
		}
		return deletedDelegation;
	}

	@Override
	public List<Folder> getAccessFolder(int delegationId, int accountId) throws HyperEmailException {
		List<Folder> listFolders;
		List<FolderPermission> permission = new ArrayList<FolderPermission>();
		try {
			listFolders = hyperEmailMapper.getFolderAccess(delegationId, accountId);
			for (Folder folder : listFolders) {
				permission = hyperEmailMapper.getFolderPermission(folder.getId(), delegationId);
				folder.setFolderPermissions(permission);
			}

		} catch (Exception e) {
			logger.error("Failed to get the folders access list.", e);
			throw new HyperEmailException("Failed to get the folders access list.");
		}
		return listFolders;
	}

	@Override
	public Filter addPrivateContact(Filter filter, String type) throws HyperEmailException {
		int filterId;
		try {
			filterId = hyperEmailMapper.getContactId(type);
			filter.setId(filterId);
			hyperEmailMapper.createPrivateContact(filter);
		} catch (Exception e) {
			logger.error("Failed to add private" + type, e);
			throw new HyperEmailException("Failed to add private" + type);
		}
		return filter;

	}

	@Override
	public int deletePrivateDetails(int accessId) throws HyperEmailException {
		int deletedEmailAccount;
		try {
			deletedEmailAccount = hyperEmailMapper.deletePrivateDetail(accessId);
		} catch (Exception e) {
			logger.error("Failed to delete private delete", e);
			throw new HyperEmailException("Failed to delete private delete");
		}
		return deletedEmailAccount;
	}

	@Override
	public List<Filter> getFilter(int delegationId) throws HyperEmailException {
		List<Filter> filter;
		try {
			filter = hyperEmailMapper.getFilter(delegationId);
		} catch (Exception e) {
			logger.error("Failed to fetch all the private access filters", e);
			throw new HyperEmailException("Failed to fetch all the private access filters ");
		}
		return filter;

	}

	@Override
	public List<Filter> getFiltersByType(String type, int delegationId) throws HyperEmailException {
		List<Filter> filter;
		int filterId;
		try {
			filterId = hyperEmailMapper.getContactId(type);
			filter = hyperEmailMapper.getFiltersByType(delegationId, filterId);
		} catch (Exception e) {
			logger.error("Failed to fetch " + type + " filters", e);
			throw new HyperEmailException("Failed to fetch " + type + " filters", e);
		}
		return filter;
	}

	@Override
	public List<Delegation> getDelegateUserList(int delegationId, int userId) throws HyperEmailException {
		List<Delegation> user = null;
		try {
			user = hyperEmailMapper.getDelegateUserList(delegationId, userId);
		} catch (Exception e) {
			logger.error("Failed to fetch userlist", e);
			throw new HyperEmailException("Failed to fetch userlist", e);
		}
		return user;
	}

	@Override
	public List<UserEmailAccount> getUserEmailAccounts(String username) throws HyperEmailException {
		List<UserEmailAccount> userEmailAccounts;
		int userId;
		try {
			userId = hyperEmailMapper.getUserAccounts(username).getId();
			userEmailAccounts = hyperEmailMapper.getUserEmailAccounts(userId);
		} catch (Exception e) {
			logger.error("FailaccountIded to get account information", e);
			throw new HyperEmailException("Failed to get user email account information");
		}
		return userEmailAccounts;

	}

	@Override
	public List<Filter> getFiltersByEmail(String username, String delegateName) throws HyperEmailException {
		Integer userId;
		Integer DelegateId;
		List<Filter> filterList;
		try {
			userId = hyperEmailMapper.getIdByUserName(username);
			DelegateId = hyperEmailMapper.getIdByUserName(delegateName);

			filterList = hyperEmailMapper.getFiltersByEmail(userId, DelegateId);
		} catch (Exception e) {
			logger.error("Failed to get filter list", e);
			throw new HyperEmailException("Failed to get filter list");
		}
		return filterList;
	}

	@Override
	public List<Folder> getFolderByEmail(String email) throws HyperEmailException {
		List<Folder> listFolders;
		FolderUtils folderName = new FolderUtils();
		try {
			String password = hyperEmailMapper.getUserEmailAccountByEmail(email).getPassword();
			UserEmailAccount account = hyperEmailMapper.getUserEmailAccountByEmail(email);
			listFolders = folderName.getFolderList(email, password, account.getImapDomain(), account.getImapPort());
		} catch (Exception e) {
			logger.error("Failed to get folder list", e);
			throw new HyperEmailException("Failed to get folder list");
		}

		return listFolders;
	}

	@Override
	public List<Delegation> getFolderListByDelegateUserName(String username) throws HyperEmailException {
		Integer userId;
		List<Delegation> delegationList = new ArrayList<Delegation>();

		List<User> userList;

		try {
			userId = hyperEmailMapper.getUserAccounts(username).getId();
			userList = hyperEmailMapper.getUserList(userId);

			for (User user : userList) {

				Delegation delegation = new Delegation();
				List<Folder> folderList = new ArrayList<Folder>();

				username = user.getUsername();
				userId = hyperEmailMapper.getUserAccounts(username).getId();

				folderList = hyperEmailMapper.getFolderByUserId(userId);

				delegation.setUser(user);
				delegation.setFolder(folderList);
				delegationList.add(delegation);

			}
		} catch (Exception e) {
			logger.error("Failed to get folder list", e);
			throw new HyperEmailException("Failed to get folder list");
		}
		return delegationList;
	}

	@Override
	public User getUserDetails(int userId) throws HyperEmailException {

		List<UserEmailAccount> userEmailAccounts;
		User user;
		try {
			user = hyperEmailMapper.getUserInfo(userId);
			userEmailAccounts = hyperEmailMapper.getAllAccounts(userId);
			user.setUserEmailAccounts(userEmailAccounts);
		} catch (Exception e) {
			logger.error("Failed to user Information", e);
			throw new HyperEmailException("Failed to get user Information");
		}

		logger.info("usererer" + user);

		return user;
	}

	public void addContacts(RecentlyContacted recentlyContacted) throws HyperEmailException {
		UserEmailAccount account;
		try {
			/*
			 * String s = recentlyContacted.getUserEmail(); s =
			 * s.substring(s.indexOf("<") + 1); s = s.substring(0,
			 * s.indexOf(">")); recentlyContacted.setUserEmail(s);
			 */

			logger.info("recently contacted" + recentlyContacted);
			account = hyperEmailMapper.getAccountDetailsByEmail(recentlyContacted.getUserEmail());
			logger.info("account " + account);
			recentlyContacted.setUserEmailId(account.getId());
			for (Contact contact : recentlyContacted.getContact()) {
				Contact contact1 = hyperEmailMapper
						.getContactInfoByUseEmailAndContact(recentlyContacted.getUserEmailId(), contact.getEmail());
				if (contact1 == null)
					hyperEmailMapper.insertContact(recentlyContacted.getUserEmailId(), contact.getEmail(),
							contact.getCountMail());
				else {
					contact.setId(contact1.getId());
					int count = contact1.getCountMail() + contact.getCountMail();
					hyperEmailMapper.updateContact(contact.getId(), count);
				}
			}

		} catch (Exception e) {
			logger.error("Failed to add contact list", e);
			throw new HyperEmailException("Failed to add contact list", e);
		}
	}

	@Override
	public List<Contact> getContactList(int userId) throws HyperEmailException {
		List<Contact> contactList;
		try {
			contactList = hyperEmailMapper.getContactListByUserId(userId);
		} catch (Exception e) {
			logger.error("Failed to get contact list.", e);
			throw new HyperEmailException("Failed to get contact list.");
		}
		return contactList;
	}

	@Override
	public User getUserDetails(String username) throws HyperEmailException {
		User user;
		try {
			user = hyperEmailMapper.getUserInfoByUsername(username);
		} catch (Exception e) {
			logger.error("Failed to get user information", e);
			throw new HyperEmailException("Failed to get user information");
		}
		return user;
	}
}
