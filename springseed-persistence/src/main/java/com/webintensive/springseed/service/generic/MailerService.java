package com.webintensive.springseed.service.generic;


import java.util.Properties;  
import javax.mail.*;  
import javax.mail.internet.*;  


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.webintensive.springseed.service.impl.UserAccountServiceImpl;

@Service("mailerService")
public class MailerService {

	@Autowired
	private SimpleMailMessage simpleMailMessage;
	
	private static final Logger logger = LoggerFactory.getLogger(MailerService.class);
	@Autowired
	private JavaMailSender mailSender;
	/**
	 * This method will send compose and send the message
	 * */
	@Async
	public void sendMail(String to, String subject, String body) {
//		simpleMailMessage.setTo(to);
//		simpleMailMessage.setSubject(subject);
//		simpleMailMessage.setText(body);
//		logger.info("Message " +  to);
//		mailSender.send(simpleMailMessage);
		
//		to = "radhesingh4395@gmail.com";
		//Get the session object  
		  Properties props = new Properties();  
		  props.put("mail.smtp.host", "smtp.gmail.com");  
		  props.put("mail.smtp.socketFactory.port", "465");  
		  props.put("mail.smtp.socketFactory.class",  
		            "javax.net.ssl.SSLSocketFactory");  
		  props.put("mail.smtp.auth", "true");  
		  props.put("mail.smtp.port", "465");  
		   
		  Session session = Session.getDefaultInstance(props,  
		   new javax.mail.Authenticator() {  
		   protected PasswordAuthentication getPasswordAuthentication() {  
		   return new PasswordAuthentication("radhesingh4395@gmail.com","radhesingh@4395");//change accordingly  
		   }  
		  });  
		   
		  //compose message  
		  try {  
		   MimeMessage message = new MimeMessage(session);  
		   message.setFrom(new InternetAddress(to));//change accordingly  
		   message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
		   message.setSubject("Activation link");  
		   message.setText(body);  
		     
		   //send message  
		   Transport.send(message);  
		  
		   System.out.println("message sent successfully");  
		   
		  } catch (MessagingException e) {throw new RuntimeException(e);}  
	}
}
