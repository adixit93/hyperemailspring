package com.webintensive.springseed.service;

import java.util.List;

import com.webintensive.springseed.pojomodel.UserGroup;

public interface UserGroupService {
	UserGroup getUserGroupById(int userGroupId) throws Exception;
	UserGroup getUserGroupByGroupName(String groupName) throws Exception;
	List<UserGroup> getAllUserGroups() throws Exception;
	UserGroup persistUserGroup(String name, String description) throws Exception;
	void deleteUserGroup(int id) throws Exception;
}
