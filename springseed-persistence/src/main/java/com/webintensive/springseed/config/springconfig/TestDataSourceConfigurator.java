package com.webintensive.springseed.config.springconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Data source configuration specifically for testing purposes. The maven tests
 * will use this data source. @{@link Profile} annotation will be responsible
 * for overriding the {@link DataSourceConfigurator}.
 * 
 * @author gauravg
 */
@Configuration
@Profile("integration")
public class TestDataSourceConfigurator extends DataSourceConfigurator {
	private @Value("jdbc:mysql://${MYSQL_DB_HOST}:${MYSQL_DB_PORT}/${MYSQL_TEST_DATABASE}?zeroDateTimeBehavior=convertToNull") String testDBURI;

	/* (non-Javadoc)
	 * @see com.webintensive.springseed.config.springconfig.DataSourceConfigurator#dataSource()
	 */
	@Override
	@Bean
	public DriverManagerDataSource dataSource() {
		DriverManagerDataSource managerDS = new DriverManagerDataSource();
		managerDS.setDriverClassName(driverClassName);
		managerDS.setUrl(testDBURI);
		managerDS.setUsername(mysqlUsername);
		managerDS.setPassword(mysqlPassword);
		return managerDS;
	}
}
