package com.webintensive.springseed.config.springconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * A separate configuration class for data source configurations. The data
 * source configuration must precede the database layer configuration as the
 * database layer will be dependent on the data source configured here.
 * 
 * @author gauravg
 */
@Configuration
public class DataSourceConfigurator {
	protected @Value("jdbc:postgresql://${MYSQL_DB_HOST}:${MYSQL_DB_PORT}/${MYSQL_DATABASE}?zeroDateTimeBehavior=convertToNull") String dbURI;
	protected @Value("${MYSQL_DB_USERNAME}") String mysqlUsername;
	protected @Value("${MYSQL_DB_PASSWORD}") String mysqlPassword;
	protected @Value("${MYSQL_DRIVER_CLASS_NAME}") String driverClassName;

	/**
	 * The bean definition for data source.
	 * 
	 * @return
	 */
	@Bean
	public DriverManagerDataSource dataSource() {
		DriverManagerDataSource managerDS = new DriverManagerDataSource();
		managerDS.setDriverClassName(driverClassName);
		managerDS.setUrl(dbURI);
		managerDS.setUsername(mysqlUsername);
		managerDS.setPassword(mysqlPassword);
		return managerDS;
	}
}
