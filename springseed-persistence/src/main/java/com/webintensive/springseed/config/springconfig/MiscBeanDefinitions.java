package com.webintensive.springseed.config.springconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * Define all the miscellaneous beans here. Instead of cluttering the already
 * existing configuration files.
 * 
 * @author gauravg
 */
@Configuration
public class MiscBeanDefinitions {
	@Bean
	public ShaPasswordEncoder shaPasswordEncoder() {
		return new ShaPasswordEncoder(256);
	}
}
