package com.webintensive.springseed.config.springconfig;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.webintensive.springseed.pojomodel.ExtendedUser;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.pojomodel.UserPrivilege;
import com.webintensive.springseed.service.UserAccountService;
import com.webintensive.springseed.service.UserPrivilegeService;

/**
 * The user details service is used extensively by the spring security for
 * various authentication and authorisation decisions.
 * 
 * @author gauravg
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserAccountService userAccountService;
	@Autowired
	private UserPrivilegeService userPrivilegeService;

	@Override
	public ExtendedUser loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAccount userAccount = userAccountService.loadUserByUsername(username);
		if (null == userAccount) {
			throw new UsernameNotFoundException("The email address " + username + " is not registered.");
		}
		userAccount.setUserPrivileges(userPrivilegeService.getAllPrivilegesByUsername(userAccount.getId(), userAccount
				.getUserGroup().getId()));
		return buildUserDetails(userAccount);

	}

	private ExtendedUser buildUserDetails(final UserAccount userAccount) {
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		// populate all the authorities
		for (UserPrivilege priv : userAccount.getUserPrivileges()) {
			if (null != priv) {
				authorities.add(new SimpleGrantedAuthority(priv.getName()));
			}
		}
		// populate all the roles
		authorities.add(new SimpleGrantedAuthority("ROLE_" + userAccount.getUserGroup()));
		ExtendedUser user = new ExtendedUser(userAccount.getUsername(), userAccount.getPassword(),
				userAccount.getActive() == 1, true, true, userAccount.getSuspend() != 1, authorities);
		user.setSalt(userAccount.getSalt());
		return user;

	}

}