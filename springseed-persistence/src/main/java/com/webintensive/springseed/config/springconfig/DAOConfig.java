package com.webintensive.springseed.config.springconfig;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.ibatis.mapping.ResultFlag;
import org.apache.ibatis.mapping.ResultMap.Builder;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import com.webintensive.springseed.hyperemail.models.Delegation;
import com.webintensive.springseed.hyperemail.models.Filter;
import com.webintensive.springseed.hyperemail.models.Folder;
import com.webintensive.springseed.hyperemail.models.User;
import com.webintensive.springseed.hyperemail.models.UserEmailAccount;
import com.webintensive.springseed.pojomodel.UserAccount;
import com.webintensive.springseed.pojomodel.UserGroup;
import com.webintensive.springseed.pojomodel.UserPrivilege;

/**
 * All the database related configuration goes here. TODO: connection pooling
 * for the datasources.
 * 
 * @author gauravg
 */
@Configuration
@EnableTransactionManagement()
@Import(DataSourceConfigurator.class)
@MapperScan("com.webintensive.springseed.dao.mapper")
public class DAOConfig implements TransactionManagementConfigurer {

	private static final String RESULT_MAP_GROUP_BASIC_KEY = "com.webintensive.springseed.dao.mapper.UserGroupMapper.userGroupMap";
	private static final String RESULT_MAP_ACCOUNT_BASIC_KEY = "com.webintensive.springseed.dao.mapper.UserAccountMapper.userAccountMap";
	private static final String RESULT_MAP_ACCOUNT_DETAILED_KEY = "com.webintensive.springseed.dao.mapper.UserAccountMapper.detailedUserAccountMap";
	private static final String RESULT_MAP_PRIV_BASIC_KEY = "com.webintensive.springseed.dao.mapper.UserPrivilegeMapper.userPrivMap";
	private static final String RESULT_MAP_LIST_PRIV_BASIC_KEY = "com.webintensive.springseed.dao.mapper.UserPrivilegeMapper.userPrivListMap";
	private static final String RESULT_MAP_LIST_HYPEREMAIL_BASIC_KEY = "com.webintensive.springseed.dao.mapper.HyperEmailMapper.userMap";
	private static final String RESULT_MAP_LIST_USER_EMAIL_ACCOUNT_BASIC_KEY = "com.webintensive.springseed.dao.mapper.HyperEmailMapper.userEmailAccountMap";
	private static final String RESULT_MAP_LIST_DELEGATION_BASIC_KEY = "com.webintensive.springseed.dao.mapper.HyperEmailMapper.delegationMap";
	private static final String RESULT_MAP_LIST_FOLDER_BASIC_KEY = "com.webintensive.springseed.dao.mapper.HyperEmailMapper.folderMap";
	private static final String RESULT_MAP_LIST_FILTER_BASIC_KEY = "com.webintensive.springseed.dao.mapper.HyperEmailMapper.filterMap";

	@Autowired
	DataSource dataSource;

	/**
	 * Transaction manager for the context managed transactions.
	 * 
	 * @return
	 */
	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.transaction.annotation.
	 * TransactionManagementConfigurer#annotationDrivenTransactionManager()
	 */
	@Override
	public PlatformTransactionManager annotationDrivenTransactionManager() {
		return transactionManager();
	}

	/**
	 * The SQL session factory bean.
	 * 
	 * @return
	 * @throws Exception
	 */
	@Bean
	public SqlSessionFactory sqlSessionFactory() throws Exception {
		org.apache.ibatis.mapping.Environment environment = new org.apache.ibatis.mapping.Environment("",
				new JdbcTransactionFactory(), dataSource);
		org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration(environment);

		registerUserPrivMaps(config);
		registerUserGroupMaps(config);
		registerUserAccountMaps(config);
		registerUserMap(config);
		registerUserEmailAccountMap(config);
		registerDelegationMap(config);
		registerFolderMap(config);
		registerFilterMap(config);

		return new SqlSessionFactoryBuilder().build(config);
	}

	/**
	 * Registers the user group map. This map will be used for mapping the SQL
	 * query results into an object.
	 * 
	 * @param config
	 */
	private void registerUserGroupMaps(org.apache.ibatis.session.Configuration config) {
		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to ugrp_id
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "ugrp_id", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map admin to ugrp_admin
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "admin", "ugrp_admin", Byte.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map description to ugrp_desc
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "description", "ugrp_desc",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map name to ugrp_name
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "name", "ugrp_name", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_GROUP_BASIC_KEY, UserGroup.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());
	}

	/**
	 * Registers the user account map. This map will be used for mapping the SQL
	 * query results into an object.
	 * 
	 * @param config
	 */
	private void registerUserAccountMaps(org.apache.ibatis.session.Configuration config) {
		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to uaccId
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "uacc_id", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map activationToken to uacc_activation_oken
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "activationToken",
				"uacc_activation_token", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map dateAdded to uacc_date_added
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "dateAdded", "uacc_date_added",
				Date.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map dateFailLoginBan to uaccDateFailLoginBan
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "dateFailLoginBan",
				"date_fail_login_ban", Date.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map dateLastLogin to uaccDateLastLogin
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "dateLastLogin", "date_last_login",
				Date.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map email to uaccEmail
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "email", "uacc_email",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map failLoginAttempts to uaccFailLoginAttempts
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "failLoginAttempts",
				"uacc_fail_login_attempts", Short.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map failLoginIpAddress to uaccFailLoginIpAddress
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "failLoginIpAddress",
				"uacc_fail_login_ip_address", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map forgottenPasswordExpire to uaccForgottenPasswordExpire
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "forgottenPasswordExpire",
				"uacc_forgotten_password_expire", Date.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map forgottenPasswordToken to uaccForgottenPasswordToken
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "forgottenPasswordToken",
				"uacc_forgotten_password_token", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map ipAddress to uaccIpAddress
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "ipAddress", "uacc_ip_address",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map password to uaccPassword
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "password", "uacc_password",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map salt to uaccSalt
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "salt", "uacc_salt", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map suspend to uaccSuspend
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "suspend", "uacc_suspend",
				Byte.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map active to 'uacc_active'
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "active", "uacc_active",
				Byte.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map updateEmail to uaccUpdateEmail
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "updateEmail", "uacc_update_email",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map updateEmailToken to uaccUpdateEmailToken
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "updateEmailToken",
				"uacc_update_email_token", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		// map username to uaccUsername
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "username", "uacc_username",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_ACCOUNT_BASIC_KEY, UserAccount.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

		// advanced detailed map
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "userGroup");
		resultBuilder.flags(flags);
		resultBuilder.javaType(UserGroup.class);
		resultBuilder.foreignColumn("uacc_group_fk");
		resultBuilder.nestedResultMapId(RESULT_MAP_GROUP_BASIC_KEY);
		resultMappings.add(resultBuilder.build());

		resultMapBuilder = new Builder(config, RESULT_MAP_ACCOUNT_DETAILED_KEY, UserAccount.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

	}

	/**
	 * Registers the user privilege map. This map will be used for mapping the
	 * SQL query results into an object.
	 * 
	 * @param config
	 */
	private void registerUserPrivMaps(org.apache.ibatis.session.Configuration config) {
		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to upriv_id
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "upriv_id", Short.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map description to upriv_desc
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "description", "upriv_desc",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map name to ugrp_name
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "name", "upriv_name", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_PRIV_BASIC_KEY, UserPrivilege.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

		resultMapBuilder = new Builder(config, RESULT_MAP_LIST_PRIV_BASIC_KEY, ArrayList.class, resultMappings, false);

	}

	private void registerUserMap(org.apache.ibatis.session.Configuration config) {

		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to id
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "id", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map first_name to firstName
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "firstName", "first_name",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map middle_name to middleName
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "middleName", "middle_name",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map last_name to lastName
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "lastName", "last_name",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "email", "email",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "username", "uacc_username",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());
		
		

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_LIST_HYPEREMAIL_BASIC_KEY, User.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

	}

	private void registerUserEmailAccountMap(org.apache.ibatis.session.Configuration config) {

		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to id
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "id", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map password to password
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "password", "password",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map "domainName", "domain_name"
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "imapDomain", "imap_domain",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "smtpDomain", "smtp_domain",
				String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "smtpPort", "smtp_port",
				Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "imapPort", "imap_port",
				Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "user.id", "user_id",
				Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		

		
		// map last_name to lastName
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "email", "email", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_LIST_USER_EMAIL_ACCOUNT_BASIC_KEY,
				UserEmailAccount.class, resultMappings, false);
		config.addResultMap(resultMapBuilder.build());

	}

	private void registerDelegationMap(org.apache.ibatis.session.Configuration config) {
		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map description to upriv_desc
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "id", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map name to ugrp_name
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "name", "name", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_LIST_DELEGATION_BASIC_KEY, Delegation.class,
				resultMappings, false);
		config.addResultMap(resultMapBuilder.build());

	}

	private void registerFolderMap(org.apache.ibatis.session.Configuration config) {
		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map description to upriv_desc
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "id", "id", String.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		// map name to ugrp_name
		resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(config, "userEmailAccount.id",
				"user_email_account_id", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_LIST_FOLDER_BASIC_KEY, Folder.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

	}

	private void registerFilterMap(org.apache.ibatis.session.Configuration config) {

		// registering result maps
		List<ResultFlag> flags = new ArrayList<ResultFlag>();
		List<ResultMapping> resultMappings = new ArrayList<ResultMapping>();
		flags.add(ResultFlag.ID);

		// map id to id
		org.apache.ibatis.mapping.ResultMapping.Builder resultBuilder = new org.apache.ibatis.mapping.ResultMapping.Builder(
				config, "delegation.id", "delegation", Integer.class);
		resultBuilder.flags(flags);
		resultMappings.add(resultBuilder.build());

		Builder resultMapBuilder = new Builder(config, RESULT_MAP_LIST_FILTER_BASIC_KEY, Filter.class, resultMappings,
				false);
		config.addResultMap(resultMapBuilder.build());

	}

}
