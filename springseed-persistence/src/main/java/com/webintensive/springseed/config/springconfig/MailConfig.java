package com.webintensive.springseed.config.springconfig;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.webintensive.springseed.configurations.DispatcherConfig;

/**
 * Mailer configuration.
 * 
 * @author gauravg
 */
@Configuration
@Import(DispatcherConfig.class)
public class MailConfig {

	@Value("${email.host}")
	private String host;

	@Value("${email.from}")
	private String from;

	@Value("${email.port}")
	private int port;

	@Value("${email.username}")
	private String username;

	@Value("${email.password}")
	private String password;

	@Value("${email.protocol}")
	private String protocol;

	@Bean
	public JavaMailSender javaMailService() {
		Properties properties = new Properties();
		properties.setProperty("mail.smtp.auth", "false");
		properties.setProperty("mail.smtp.starttls.enable", "false");

		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setHost(host);
		javaMailSender.setPassword(password);
		javaMailSender.setUsername(username);
		javaMailSender.setPort(port);
		javaMailSender.setProtocol(protocol);
		javaMailSender.setJavaMailProperties(properties);
		return javaMailSender;
	}

	@Bean
	public SimpleMailMessage simpleMailMessage() {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom(from);
		simpleMailMessage.setReplyTo(from);
		return simpleMailMessage;
	}
}