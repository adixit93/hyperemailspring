package com.webintensive.springseed.utils;

import org.springframework.test.context.ActiveProfilesResolver;

public class ProfileResolver implements ActiveProfilesResolver {

	@Override
	public String[] resolve(Class<?> testClass) {
		final String activeProfile = System.getProperty("spring.profiles.active");
        return new String[] { activeProfile == null ? "integration" : activeProfile };
	}

}
