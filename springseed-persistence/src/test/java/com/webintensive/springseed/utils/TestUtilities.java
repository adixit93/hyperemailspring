package com.webintensive.springseed.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public final class TestUtilities {

	private static final SecureRandom random = new SecureRandom();
	// will result in a maximum of 10 digit string
	private static final int RANDOM_STRING_LENGTH = 50;

	private TestUtilities() {

	}

	private static final TestUtilities instance = new TestUtilities();

	public String getRandomString() {
		return new BigInteger(RANDOM_STRING_LENGTH, random).toString(32);
	}

	public final static TestUtilities instance() {
		return instance;
	}
}
