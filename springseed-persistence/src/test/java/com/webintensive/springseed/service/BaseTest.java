package com.webintensive.springseed.service;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.webintensive.springseed.config.springconfig.TestDataSourceConfigurator;
import com.webintensive.springseed.configurations.DispatcherConfig;
import com.webintensive.springseed.configurations.RootAppConfig;
import com.webintensive.springseed.utils.ProfileResolver;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {DispatcherConfig.class, RootAppConfig.class, TestDataSourceConfigurator.class}, loader = AnnotationConfigContextLoader.class)
@ActiveProfiles(resolver=ProfileResolver.class)
public class BaseTest  extends TestCase {
	@Test
	public void isBaseTestWorking() {
		System.out.println("Spring test for persistence module has been successfully set up.");
	}
}