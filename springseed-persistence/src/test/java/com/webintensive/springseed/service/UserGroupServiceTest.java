package com.webintensive.springseed.service;

import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.webintensive.springseed.pojomodel.UserGroup;
import com.webintensive.springseed.utils.TestUtilities;

/**
 * This is a collection of tests for the user group service.
 * 
 * @author gauravg
 */
public class UserGroupServiceTest extends BaseTest {
	private String groupName;
	private String groupDescription;

	@BeforeClass
	public static void boot() {
	}

	@Before
	public void init() {
		groupName = TestUtilities.instance().getRandomString();
		groupDescription = TestUtilities.instance().getRandomString() + " "
				+ TestUtilities.instance().getRandomString();
	}

	@Autowired
	UserGroupService userGroupService;

	/**
	 * Create a new group by supplying name and description.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldCreateUserGroupWithNameAndDescription() throws Exception {
		UserGroup group = userGroupService.persistUserGroup(groupName, groupDescription);

		assertNotNull("The group id of the newly persisted group must not be null.", group.getId());
		assertEquals("The group must be persisted with the provided group name.", group.getName(), groupName);
		assertEquals("The group must be persisted with the provided group description.", group.getDescription(),
				groupDescription);
		assertNotNull("The group must have an admin.", group.getAdmin());
	}

	/**
	 * Fetching a newly created group by its primary key.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldFetchGroupByID() throws Exception {
		UserGroup userGroup = userGroupService.persistUserGroup(groupName, groupDescription);
		UserGroup group = userGroupService.getUserGroupById(userGroup.getId());

		assertNotNull("The user group persisted with #persistUserGroup must be available using #getUserGroupById.",
				group);
		assertNotNull("#getUserGroupById must return the desired group with not null id", group.getId());
		assertEquals("#getUserGroupById must return the desired group", new Integer(group.getId()), group.getId());
	}

	/**
	 * Delete method must delete a newly created group.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldDeleteGroup() throws Exception {
		UserGroup userGroup = userGroupService.persistUserGroup(groupName, groupDescription);
		userGroupService.deleteUserGroup(userGroup.getId());
		UserGroup group = userGroupService.getUserGroupById(userGroup.getId());

		assertNull("The user group deleted using #deleteUserGroup was never deleted.", group);
	}

	/**
	 * Create 2 groups. Now, the return all group method must return a list with
	 * at least two groups.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnAllGroups() throws Exception {
		String anotherGroupName = TestUtilities.instance().getRandomString();
		String anotherGroupDescription = TestUtilities.instance().getRandomString() + " "
				+ TestUtilities.instance().getRandomString();
		@SuppressWarnings("unused")
		UserGroup userGroup = userGroupService.persistUserGroup(groupName, groupDescription);
		@SuppressWarnings("unused")
		UserGroup anotherUserGroup = userGroupService.persistUserGroup(anotherGroupName, anotherGroupDescription);

		List<UserGroup> allGroups = userGroupService.getAllUserGroups();
		assertTrue("Fetch all groups must return at least 2 groups.", (null != allGroups && allGroups.size() >= 2));
	}

	/**
	 * Creates a group and fetches the same group by its name. Asserts that the
	 * two names and IDs are the same.
	 * 
	 * @throws Exception
	 */
	@Test
	public void shouldReturnGroupByUniqueName() throws Exception {
		UserGroup userGroup = userGroupService.persistUserGroup(groupName, groupDescription);
		UserGroup requestedGroup = userGroupService.getUserGroupByGroupName(groupName);

		assertNotNull("The group returned must not be null.", requestedGroup);
		assertEquals("The name of the group must be the literal with which the group was created.",
				userGroup.getName(), requestedGroup.getName());
		assertEquals("The group id must be the same.", userGroup.getId(), requestedGroup.getId());
	}
}
