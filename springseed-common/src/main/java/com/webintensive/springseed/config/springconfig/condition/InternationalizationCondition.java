package com.webintensive.springseed.config.springconfig.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.webintensive.springseed.config.springconfig.util.SpringConfigUtil;
import com.webintensive.springseed.config.springconfig.util.SpringFeaturesEnum;

/**
 * @author gauravg
 */
public class InternationalizationCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		return SpringConfigUtil.getInstance().isFeatureEnabled(context.getEnvironment(), SpringFeaturesEnum.INTERNATIONALIZATION);
	}
}
