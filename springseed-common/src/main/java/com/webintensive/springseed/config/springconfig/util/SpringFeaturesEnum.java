package com.webintensive.springseed.config.springconfig.util;

/**
 * All the spring features which can be toggled on and off must be listed here.
 * 
 * @author gauravg
 */
public enum SpringFeaturesEnum {
	INTERNATIONALIZATION("spring.feature.internationalization", "Internationalization");

	String propertyName;
	String propertyKey;

	SpringFeaturesEnum(String propertyKey, String propertyName) {
		this.propertyKey = propertyKey;
		this.propertyName = propertyName;
	}

}
