package com.webintensive.springseed.config.springconfig.util;

import org.springframework.core.env.Environment;

/**
 * The utilities for spring configurations only. Please do not pollute this with
 * common utility methods. There is a reason this utility file is not a @Singleton
 * but a simple traditional singleton. At the time of configuration, @Singleton
 * doesn't have a consistent behavior.
 * 
 * @author gauravg
 */
public class SpringConfigUtil {

	private SpringConfigUtil() {
	}

	private static final SpringConfigUtil INSTANCE = new SpringConfigUtil();

	public boolean isFeatureEnabled(Environment environment, SpringFeaturesEnum feature) {
		boolean isFeatureEnabled = null != environment.getProperty(feature.propertyKey) ? environment.getProperty(
				feature.propertyKey).equalsIgnoreCase(Boolean.TRUE.toString()) : false;
		return isFeatureEnabled;
	}

	public static SpringConfigUtil getInstance() {
		return INSTANCE;
	}
}
