package com.webintensive.springseed.configurations;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.support.ControllerClassNameHandlerMapping;

import com.webintensive.springseed.config.springconfig.condition.InternationalizationCondition;

/**
 * @author gauravg
 */
@Configuration
@Conditional(InternationalizationCondition.class)
public class InternationalizationConfig {

	private @Value("${localization_base_path}")
	String localizationBasePath;

	private @Value("{localization_default_encoding}")
	String defaultEncoding;

	private static LocaleChangeInterceptor localeChangeInterceptor;

	{
		localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("locale");
	}

	/*-
	 * For resource bundles.
	 * 
	 * <bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource"
	 *     p:basenames="messages" />
	 */
	@Bean
	public ResourceBundleMessageSource resourceBundleMessageSource() {
		ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
		resourceBundleMessageSource.setBasename(localizationBasePath);
		resourceBundleMessageSource.setDefaultEncoding(defaultEncoding);
		resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
		resourceBundleMessageSource.setFallbackToSystemLocale(false);
		return resourceBundleMessageSource;
	}

	/*-
	 * Locale resolver bean. Configuration alternative for.
	 * 
	 * <bean id="localeResolver" class="org.springframework.web.servlet.i18n.SessionLocaleResolver" />
	 */
	@Bean
	public SessionLocaleResolver localeResolver() {
		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(Locale.ENGLISH);
		return sessionLocaleResolver;
	}

	/**
	 * This is important for the browsers. TODO: Get to know why exactly this is
	 * needed.
	 * 
	 * @return
	 */
	@Bean(name = "localeResolver")
	public CookieLocaleResolver cookieLocaleResolver() {
		CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
		cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
		return cookieLocaleResolver;
	}

	/**
	 * ClassNameaHandlerMapping for locale change interceptor.
	 * 
	 * @return
	 */
	@Bean
	public ControllerClassNameHandlerMapping controllerClassNameHandlerMapping() {
		ControllerClassNameHandlerMapping controllerClassNameHandlerMapping = new ControllerClassNameHandlerMapping();
		controllerClassNameHandlerMapping.setInterceptors(new LocaleChangeInterceptor[] { localeChangeInterceptor });
		return controllerClassNameHandlerMapping;
	}
}
