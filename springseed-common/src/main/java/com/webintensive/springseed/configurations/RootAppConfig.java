package com.webintensive.springseed.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * @author gauravg
 */
@Configuration
@ComponentScan(basePackages = "com.webintensive.springseed")
public class RootAppConfig {
	
}